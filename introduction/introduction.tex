\chapter{Introduction}

Given a polytope $P \subseteq R^d$,
the function $L_P(t)$ is defined (for integer $t > 0$)
as the number of integral points in $tP$;
that is,
\begin{equation*}
    L_P(t) = \#(tP \cap \mathbb Z^d).
\end{equation*}
Here,
$\#(A)$ is the number of elements in the set $A$
and $tP = \{tx \mid x \in P\}$ is the dilation of $P$ by $t$.

Ehrhart theory is the study of this function and its properties.
The two main results of this area
are the Ehrhart and Reciprocity Theorems.
If $P$ is a $d$-dimensional polytope whose vertices are integral points
(an \emph{integral} polytope),
the Ehrhart Theorem states that $L_P(t)$ will be a polynomial of degree $d$ in $t$.
In this case,
it makes sense to evaluate $L_P$ for negative integers;
then the Reciprocity Theorem states that $(-1)^d L_P(-t)$
is the number of integral points in the relative interior of $tP$.

A classical extension to this setting
is to allow $P$ to have rational coordinates.
Then $L_P(t)$ will not be a polynomial anymore,
but we can come close:
the Ehrhart Theorem for rational polytopes states that
we can still write $L_P(t)$ as
\begin{equation*}
    L_P(t) = c_d(t) t^d + \dots + c_1(t) t + c_0(t),
\end{equation*}
but we must allow the $c_i(t)$ to be periodic functions in $t$.
This kind of function is called a \emph{quasi-polynomial}.
Since each $c_i$ is periodic,
it still makes sense to evaluate $L_P$ in negative integers,
and again the Reciprocity Theorem holds.

This master thesis aims to investigate another extension:
allowing the dilation parameter to be an arbitrary real number.
There are some recent papers which started exploring this extension
\cite{Linke2011, BBKV2013, HenkLinke2015, BBLKV2016, Borda2016}.

We will sometimes use the classical theorems
to show results for real dilation parameters.
To minimize confusion,
we will denote real dilation parameters with the letter $s$,
so that $L_P(t)$ denotes the classical Ehrhart function
and $L_P(s)$ denotes the extension considered in this paper.
Thus,
$L_P(t)$ is just the restriction of $L_P(s)$ to integer arguments.
