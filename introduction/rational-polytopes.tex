\section{Rational polytopes}
\label{sec:rational-polytopes}

A polytope $P$ is called \emph{rational}
if it can be written as
\begin{equation}
    \label{eq:intersection-representation}
    P = \bigcap_{i = 1}^n \{ x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
\end{equation}
where the $a_i$ are integer vectors
and $b_i$ are integer numbers.

The \emph{reflexive} polytopes forms a special class of rational polytopes.
A polytope is reflexive if it is an integer polytope
(that is,
all of its vertices are integer vectors),
and in the representation~\eqref{eq:intersection-representation}
all the $b_i$ are $1$.

In Chapter~\ref{ch:semi-reflexive},
we will define \emph{semi-reflexive} polytopes
to be the rational polytopes such that $L_P(s) = L_P(\floor s)$
for all $s \geq 0$.
We have the following characterization of semi-reflexive polytopes
in terms of their hyperplane representation.

\begin{theorem}
    \label{thm:characterization}
    Let $P$ be a rational polytope
    written as in~\eqref{eq:intersection-representation}.
    Then $P$ is semi-reflexive
    if and only if all $a_i$ are integers
    and all $b_i$ are either $0$ or $1$.
\end{theorem}

The name ``semi-reflexive'' is justified as follows.
Given a polytope $P$,
define its dual $P^*$ by
\begin{equation*}
    P^* = \{x \in \mathbb R^d \mid
            \langle x, y \rangle \leq 1 \text{ for all $y \in P$}
        \}.
\end{equation*}

Then we have the following.

\begin{theorem}
    \label{thm:semi-reflexive-dual}
	$P$ is a reflexive polytope
    if and only if
    both $P$ and $P^*$ are semi-reflexive polytopes.
\end{theorem}

In Chapter~\ref{ch:real-ehrhart-theorem},
we will explore the following consequence of Theorem~\ref{thm:characterization}.
If $P$ is a semi-reflexive polytope,
then we may compute $L_P(s)$
using only information for integer dilation parameters;
that is,
we may deduce $L_P(s)$ from $L_P(t$).
Ehrhart theorem, in turn,
says that we may deduce $L_P(t)$ for all $t$
from a finite set of values of $t$;
this gives us a closed-form expression for $L_P(s)$.
We will extend this conclusion for all rational polytopes $P$;
that is,
we will show how to obtain a closed-form expression for $L_P(s)$
for all rational polytopes $P$.
This,
in turn,
gives a new proof of the real version of the Ehrhart theorem,
stated below.

\begin{theorem}
    \label{thm:real-ehrhart-theorem}
    Let $P \subseteq \mathbb R^d$ be a rational polytope.
    Then there exists piecewise-polynomial periodic functions $c_0(s), \dots, c_d(s)$
    such that
    \begin{equation*}
        L_P(s) = c_d(s) s^d + \dots + c_1(s) s + c_0(s).
    \end{equation*}
\end{theorem}

This,
in turn,
will give us a new proof of the following theorem,
by Linke~\cite[p.~1973]{Linke2011}.

\begin{theorem}[Linke, 2011]
    \label{thm:linkes-differential-equation}
    Let $P$ be a rational polytope, and write
    \begin{equation*}
        L_P(s) = c_d(s) s^d + \dots + c_1(s) s + c_0(s).
    \end{equation*}
    Then for every $s$ at which all the $c_i$ are left (resp. right) continuous,
    all the $c_i$ will be left (resp. right) differentiable,
    and
    \begin{equation*}
        c'_i(s) = -(i+1) c_{i+1}(s).
    \end{equation*}
\end{theorem}

In Section~\ref{sec:translation-variant},
we will explore another consequence of Theorem~\ref{thm:characterization}.
The origin satisfies every linear restriction $\langle a, x \rangle \leq b$
in which $b \geq 0$;
thus,
every semi-reflexive polytope must contain the origin.
This means that,
if $P$ is semi-reflexive
and some integer translation $P + w$ does not contain the origin,
then $P + w$ cannot be semi-reflexive
and thus we will have $L_{P + w}(s) \neq L_{P + w}(\floor s)$
for at least some $s$.
Since $L_{P + w}(t) = L_P(t)$ for all integer $w$ and $t$,
this shows that the real Ehrhart function
is not invariant under integer translations.

This assertion can be strenghtened as follows.

\begin{theorem}
    \label{thm:rational-translation-variant}
    Let $P$ be a rational polytope.
    Then there exists an integer vector $w$
    such that the functions $L_{P + kw}(s)$ are all distinct
    for $k \geq 0$.
\end{theorem}

This is in sharp contrast with the classical Ehrhart function,
where all the functions $L_{P + k w}(t)$ are the same.

In Chapter~\ref{ch:reconstruction},
we will make this distinction even sharper
with the following theorem.

\begin{theorem}
    \label{thm:rational-complete-invariant}
    Let $P$ and $Q$ be two rational polytopes such that,
    for every integer translation vector $w$,
    we have $L_{P + w}(s) = L_{Q + w}(s)$.
    Then $P = Q$.
\end{theorem}

In other words,
if $P$ is a rational polytope,
then the Ehrhart functions $L_{P + w}(s)$ (for integer $w$) indentifies $P$ uniquely,
so we may reconstruct $P$ from the Ehrhart function of its integer translates.

We may also see this theorem as a first step
towards a positive answer for the following question:
are any two polytopes with the same Ehrhart function
piecewise unimodular images of each other?
(This is a variation of Question~4.1 of Haase and McAllister~\cite{HaaseMcAllister}.)
Theorem~\ref{thm:rational-complete-invariant} has a stronger hypothesis
(that $P+w$ and $Q+w$ have the same Ehrhart function
for all integer $w$),
but has a stronger conclusion
(the polytopes are actually the same, not even up to translation).
This suggests that we might be able to drop some of that hypothesis
and still conclude that the polytopes are piecewise unimodular images of each other.
