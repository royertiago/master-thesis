\section{Notation and structure of the text}

Chapter~\ref{ch:preliminaries}
provides a quick introduction to the area.
Section~\ref{sec:classical-ehrhart-theory}
contains a quick recapitulation of the classical Ehrhart theory,
and Section~\ref{sec:real-dilations}
defines the real Ehrhart function $L_P(s)$.
(Since results of the classical setting are used in the discussion,
we will use the letter $s$ when working with real dilation parameters
to minimize confusion.)

We will usually represent polytopes
by their description as intersection of half-spaces.
That is,
we will write polytopes $P \subset \mathbb R^d$ as
\begin{equation*}
    P = \bigcap_{i = 1}^n \{ x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
    \tag{\ref{eq:intersection-representation} revisited}
\end{equation*}
where $a_i$ are vectors of $\mathbb R^d$ and $b_i$ are real numbers.
If $P$ is a full-dimensional polytope,
the number $n$ may be chosen to be the number of facets in $P$.
In this case,
each hyperplane $\{ x \mid \langle a_i, x \rangle = b_i \}$ intersects $P$ in a facet,
so that there is no redundant hyperplanes;
that is, such representation is minimal.
We will assume such representations are always minimal for full-dimensional polytopes.

In some sections,
we will deal with specific classes of polytopes,
and thus the vectors $a_i$ and the numbers $b_i$
will be appropriately restricted.
These restrictions will be stated explicitly.

In Chapter~\ref{ch:semi-reflexive}
we will define semi-reflexive polytopes
and prove Theorems~\ref{thm:characterization}
and~\ref{thm:real-characterization},
which characterizes them in terms of their hyperplane description.
Here,
the vectors $a_i$ will often be scaled
so that the right-hand sides $b_i$ are either $-1$, $0$ or $1$.

To show the characterization,
we will use the Iverson bracket,
which is defined as follows.
Given a proposition $p$,
we define the number $[p]$ to be $1$ if $p$ is true,
and $0$ if $p$ is false.
For example,
the number $\#(P \cap \mathbb Z^d)$ of integer points contained in $P$
may be expressed as
\begin{equation*}
    \#(P \cap \mathbb Z^d) = \sum_{x \in \mathbb Z^d} [x \in P].
\end{equation*}

The characterizations are shown in section~\ref{sec:characterization},
and then used in section~\ref{sec:semi-reflexive-examples}
to show some examples of semi-reflexive polytopes.
Theorem~\ref{thm:semi-reflexive-dual},
relating reflexive and semi-reflexive polytopes,
is shown in Section~\ref{sec:relation-with-reflexive}.
And,
in section~\ref{sec:same-ehrhart-function},
semi-reflexive polytopes are used
to construct an example of two distinct polytopes
which have the same Ehrhart function.

Section~\ref{sec:translation-variant}
contains the proof of Theorems
\ref{thm:translation-variant} and~\ref{thm:rational-translation-variant}.
Most of the time,
we will be working with arbitrary polytopes on this section,
so the vectors $a_i$ will be assumed to be normalized.

Chapter~\ref{ch:real-ehrhart-theorem},
presents a different approach for showing the real Ehrhart theorem,
based on a modification of the argument
used to show the ``if'' part of Theorem~\ref{thm:characterization}.
As a warm-up,
section~\ref{sec:polytopes-containing-the-origin}
proves this result for polytopes containing the origin.
When extending for all rational polytopes,
we use the ``front body/back shell decomposition'',
developed in section~\ref{sec:front-body-back-shell}.
The proof of the real Ehrhart theorem
is the subject of section~\ref{sec:real-ehrhart-theorem}.
For completeness,
section~\ref{sec:real-reciprocity} contains a proof of the real reciprocity theorem.

In the same chapter,
Section~\ref{sec:linkes-differential-equation}
provides a different proof of Theorem~\ref{thm:linkes-differential-equation}.

Chapter~\ref{ch:reconstruction},
in turn,
is dedicated to show Theorems~\ref{thm:rational-complete-invariant},
\ref{thm:codimension-zero-and-one} and~\ref{thm:convex-body-reconstruction}.

Sections \ref{sec:semirational-polytopes} and~\ref{sec:codimension-one}
deal with semi-rational polytopes,
so in this section the $a_i$ will be primitive integer vectors;
that is,
vectors $a_i$ such that $\frac 1 k a_i$ is not an integer
for any integer $k > 1$,
or, equivalently,
the greatest common divisor of all coordinates of $a_i$ is $1$.

In Section~\ref{sec:semirational-polytopes},
we will show Theorem~\ref{thm:codimension-zero-and-one}
just for full-dimensional semi-rational polytopes;
this is Corollary~\ref{thm:semirational-complete-invariant}.
It turns out that showing this theorem for codimension one polytopes
is actually harder than for full-dimensional ones;
in fact,
in Section~\ref{sec:piecing-together-avoiding-discontinuities},
we will first show Corollary~\ref{thm:semirational-dense-information-complete-invariant},
which is a strengthened version of Corollary~\ref{thm:semirational-complete-invariant}
that says that,
for full-dimensional semi-rational polytopes $P$ and $Q$,
we have $P = Q$ even if $L_{P + w}(s) = L_{Q + w}(s)$
for $s$ in a dense subset of $\mathbb R$.
Then we will reduce semi-rational polytopes with codimension one,
and rational polytopes with any dimension,
to this corollary.

In these proofs,
we will have to use limits with ``restricted domains''.
If $D \subseteq \mathbb R$ is an unbounded set,
then the expression
\begin{equation*}
    \lim_{\substack{
        s \to \infty \\
        s \in D
    }}
    f(s)
    = l
\end{equation*}
means that,
for every $\varepsilon > 0$,
there is a number $N$ such that,
for all $s \in D$,
if $s > N$ then $|f(s) - l| < \varepsilon$.
For example,
if $D = \{k \pi \mid k \in \mathbb Z\}$,
then
\begin{equation*}
    \lim_{\substack{
        \theta \to \infty \\
        \theta \in D
    }}
    \sin \theta
    = 0,
\end{equation*}
a limit which we will usually write as
\begin{equation*}
    \lim_{\substack{
        \theta \to \infty \\
        \frac\theta\pi \in \mathbb Z
    }}
    \sin \theta = 0.
\end{equation*}

Note that this limit is undefined if the domain is bounded.

The relative volume of a semi-rational polytope $P$,
denoted by $\rvol P$,
is defined in Section~\ref{sec:relative-volumes-of-facets}.

Finally,
section~\ref{sec:symmetric-reconstruction}
shows Theorem~\ref{thm:convex-body-reconstruction}.
