\section{Non-rational polytopes}
\label{sec:non-rational-polytopes}

Although Ehrhart theory is usually concerned with rational polytopes,
there has been some effort in working with non-rational polytopes as well.
For example,
Borda~\cite{Borda2016} deals with simplices and cross-polytopes
with algebraic coordinates;
and \cite{DeSarioRobins2011} and~\cite{DiazLeRobins2016}
deal with arbitrary real polytopes and real dilations
but for the solid-angle polynomial.

In the course of proving the theorems stated in Section~\ref{sec:rational-polytopes},
we will be able to extend some of those theorems
to real polytopes.
Interestingly,
we always need to impose some dimensionality condition
(like demanding the polytope to be full-dimensional)
in order to avoid simple counter-examples.

We will extend the theorems in Chapters \ref{ch:semi-reflexive}
and~\ref{ch:reconstruction}.
For Theorem~\ref{thm:characterization},
we have the following extension.

\begin{theorem}
    \label{thm:real-characterization}
    Let $P$ be a full-dimensional real polytope.
    Then $L_P(s) = L_P(\floor s)$ for all $s$
    if and only if
    the polytope $P$ can be written as in~\eqref{eq:intersection-representation}
    where each $b_i$ is either $0$ or $1$,
    and when $b_i = 1$ the vector $a_i$ must be integral.
\end{theorem}

For Theorem~\ref{thm:rational-translation-variant},
we may also handle polytopes which have codimension $1$.

\begin{theorem}
    \label{thm:translation-variant}
    Let $P \subseteq \mathbb R^d$ be a real polytope
    which is either full-dimensional
    or has codimension $1$.
    Then there is an integral vector $w \subseteq \mathbb R^d$ such that
    the functions $L_{P + k w}(s)$ are different
    for all integers $k \geq 0$.
\end{theorem}

The extension of Theorem~\ref{thm:rational-complete-invariant}
is more modest.
A polytope $P$ is \emph{semi-rational}
if it can be written as in~\eqref{eq:intersection-representation}
with the $a_i$ being integral vectors;
the $b_i$ may be arbitrary real numbers.
We have the following generalization.

\begin{theorem}
    \label{thm:codimension-zero-and-one}
    Let $P$ and $Q$ be two semi-rational polytopes in $\mathbb R^d$,
    both having codimension $0$ or $1$.
    Suppose moreover that $L_{P + w}(s) = L_{Q + w}(s)$
    for all integer $w$ and all real $s > 0$.
    Then $P = Q$.
\end{theorem}

And,
finally,
we may show another extension of Theorem~\ref{thm:rational-complete-invariant}
by stepping outside the realm of polytopes.
A convex body $K \subseteq \mathbb R^d$ is called \emph{symmetric}
if, for all $x \in \mathbb R^d$,
we have $x \in K$ if and only if $-x \in K$.

\begin{theorem}
    \label{thm:convex-body-reconstruction}
    Let $K$ and $H$ be symmetric convex bodies,
    and assume that $L_{K + w}(s) = L_{H + w}(s)$
    for all real $s > 0$ and all integer $w$.
    Then $K = H$.
\end{theorem}
