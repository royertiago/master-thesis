\subsection{Interiors of polytopes}
\label{sec:interiors-of-polytopes}

It is interesting to note there are results
similar to Theorems \ref{thm:characterization} and~\ref{thm:real-characterization},
but for interiors of polytopes.
More precisely:

\begin{theorem}
    Let $P$ be a full-dimensional polytope
    written as in~\eqref{eq:intersection-representation}.
    Then $L_{P^\circ}(s) = L_{P^\circ} (\ceil s)$ for all $s \geq 0$
    if and only if all $b_i$ is either $0$ or $1$,
    and $a_i$ is integral whenever $b_i = 1$.
\end{theorem}

In other words,
$L_P(s) = L_P(\floor s)$ if and only if $L_{P^\circ}(s) = L_{P^\circ}(\ceil s)$.

\begin{proof}
    The proof is very similar to the proofs of the theorems~\ref{thm:if-part},
    \ref{thm:weak-only-if-part}, and~\ref{thm:real-characterization},
    so only the needed changes will be stated.

    For Theorem~\ref{thm:if-part},
    the case $b_i = 0$ is left unchanged,
    and when $b_i = 1$ we need to use $[n < x] = [n < \ceil x]$ to conclude that,
    in all cases,
    $[\langle a_i, x \rangle < s b_i]
    = \big[\langle a_i, x \rangle < \ceil s b_i\big]$.

    For Proposition~\ref{thm:weak-only-if-part},
    we can use the same $u$, $s_0$ and $x_0$,
    but now we will show $L_{P^\circ}(s_0) < L_{P^\circ}(s_0 - \varepsilon)$.
    A point $x$ which is in the interior of $s_0 P^\circ$
    satisfy all linear restrictions of the form $\langle x, a_i \rangle < s_0 b_i$.
    For all sufficiently small $\varepsilon > 0$
    we have $\langle x, a_i \rangle < (s_0 - \varepsilon) b_i$,
    and so every integer point in $s_0 P^\circ$
    will also be present on $(s_0 - \varepsilon) P^\circ$.
    Now the linear restriction $\langle u, x \rangle > 1$
    ``shrinks'' to $\langle u, x \rangle > 1 - \varepsilon$,
    and so the point $x_0$
    (which satisfy $\langle u, x_0 \rangle = 1$)
    will be contained in $(s_0 - \varepsilon) P^\circ$,
    and thus $L_{P^\circ}(s_0 - \varepsilon) > L_{P^\circ}(s_0)$.

    And for Theorem~\ref{thm:real-characterization},
    we again can use the same $s_0$ and $x_0$ and also the face $F$,
    but now we will dilate $P^\circ$ instead of shrinking.
    As $sP \subseteq s'P$ for all $s \leq s'$,
    we also have $sP^\circ \subseteq s' P^\circ$ for all $s \leq s'$.
    Now $x_0 \in s_0 F$,
    so for all $\varepsilon > 0$
    we will have $x_0 \in (s_0 + \varepsilon)P^\circ$,
    which thus show $L_P(s_0) < L_P(s_0 + \varepsilon)$.
\end{proof}

Thus,
using essentially the same proof as of Theorem~\ref{thm:characterization},
we have the following characterization of semi-reflexive polytopes.

\begin{theorem}
    Let $P$ be a rational polytope.
    Then $P$ is semi-reflexive
    if and only if
    $L_{P^\circ}(s) = L_{P^\circ} (\ceil s)$ for all real $s \geq 0$.
\end{theorem}
