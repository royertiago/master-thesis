\subsection{Relation with reflexive polytopes}
\label{sec:relation-with-reflexive}

The hyperplane representation of Theorem~\ref{thm:characterization}
may be rewritten in matricial form as follows:
\begin{equation}
    P = \{x \in \mathbb R^d \mid A_1 x \leq \mathbf 1, A_2 x \leq 0 \},
    \label{eq:matricial-representation}
\end{equation}
where $\mathbf 1 = (1, \dots, 1)$ is the all-ones vector,
and $A_1$ and $A_2$ are integer matrices.
We will use this representation to relate semi-reflexive and reflexive polytopes.

There are several equivalent definitions of reflexive polytopes
(see e.g.~\cite[p.~185]{CottonwoodRoom}).
We mention two of them.

First, $P$ is a reflexive polytope if it is an integer polytope
which may be written as
\begin{equation*}
    P = \{x \in \mathbb R^d \mid A x \leq \mathbf 1 \},
\end{equation*}
where $\mathbf 1 = (1, \dots, 1)$ is the all-ones vector
and $A$ is an integer matrix.
This definition make it obvious that
every reflexive polytope is also semi-reflexive.

The second definition uses the dual $P^*$ of a polytope $P$,
defined by
\begin{equation*}
    P^* = \{x \in \mathbb R^d \mid
            \langle x, y \rangle \leq 1 \text{ for all $y \in P$}
        \}.
\end{equation*}
Then $P$ is reflexive if and only if both $P$ and $P^*$ are integer polytopes.

We have the following relation between reflexive and semi-reflexive polytopes.

\begin{restatetheorem}{thm:semi-reflexive-dual}
    $P$ is a reflexive polytope
    if and only if
    both $P$ and $P^*$ are semi-reflexive polytopes.
\end{restatetheorem}

\begin{proof}
    If $P$ is reflexive,
    then $P^*$ is also reflexive,
    and thus both are semi-reflexive.

    Now,
    suppose that $P$ and $P^*$ are semi-reflexive polytopes.
    This contains the implicit assumption that $P^*$ is bounded,
    and thus $P$ must contain the origin in its interior.
    Therefore,
    by Theorem~\ref{thm:characterization},
    we must have
    \begin{equation*}
        P = \{x \in \mathbb R^d \mid A x \leq \mathbf 1 \}
    \end{equation*}
    for some integer matrix $A$.
    (The fact that $0$ is in the interior of $P$
    allowed us to ignore $A_2$
    in the representation~\eqref{eq:matricial-representation}.)

    So, 
    we just need to show that $P$ has integer vertices.
    Since $(P^*)^* = P$,
    we may apply the same reasoning to $P^*$ to write
    \begin{equation*}
        P^* = \bigcap_{i = 1}^n \{x \in \mathbb R^d \langle a_i, x \rangle \leq 1 \}
    \end{equation*}
    for certain integers $a_1, \dots, a_n$.
    But these $a_i$ are precisely the vertices of $P$,
    being, thus, an integral polytope.
\end{proof}
