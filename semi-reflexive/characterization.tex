\section[Characterization of semi-reflexive polytopes]
{
    Characterizing semi-reflexive polytopes
    in terms of their hyperplane description
}
\label{sec:characterization}

The ``if'' part of both
Theorems \ref{thm:characterization} and~\ref{thm:real-characterization}
is easy.

\begin{theorem}
    \label{thm:if-part}
    Let $P$ be as in~\eqref{eq:intersection-representation}.
    If every $b_i$ is either $0$ or $1$,
    and $a_i$ is integral whenever $b_i = 1$,
    then $L_P(s) = L_P(\floor s)$.
\end{theorem}

\begin{proof}
    \begin{equation*}
        L_P(s)
            = \sum_{x \in \mathbb Z^d} [x \in sP]
            = \sum_{x \in \mathbb Z^d} \prod_{i = 1}^n
                \big[\langle a_i, x \rangle \leq s b_i\big].
    \end{equation*}

    If $b_i = 0$, the term $[\langle a_i, x \rangle \leq s b_i]$
    reduces to $[\langle a_i, x \rangle \leq 0]$,
    which is constant for all $s$;
    thus $[\langle a_i, x \rangle \leq s b_i]
    = \big[\langle a_i, x \rangle \leq \floor s b_i\big]$.

    If $b_i = 1$,
    as $x$ and $a_i$ are integral,
    the number $\langle a_i, x \rangle$ is an integer,
    thus $[\langle a_i, x \rangle \leq s b_i]
    = \big[\langle a_i, x \rangle \leq \floor s b_i\big]$ again.

    Therefore,
    \begin{align*}
        L_P(s)  &= \sum_{x \in \mathbb Z^d} \prod_{i = 1}^n
                    \big[\langle a_i, x \rangle \leq s b_i\big] \\
                &= \sum_{x \in \mathbb Z^d} \prod_{i = 1}^n
                    \big[\langle a_i, x \rangle \leq \floor s b_i\big] \\
                &= \sum_{x \in \mathbb Z^d} [x \in \floor s P] \\
                &= L_P(\floor s).
                \qedhere
    \end{align*}
\end{proof}

For the other direction we need a lemma.

Denote the open ball with radius $\delta$ centered at $x$ by $B_\delta(x)$;
that is,
if $x \in \mathbb R^d$,
we have
\begin{equation*}
    B_\delta(x) = \{y \in \mathbb R^d \mid \norm{y - x} < \delta\}.
\end{equation*}

\begin{lemma}
    \label{thm:integral-points-in-cone}
    Let $K$ be a full-dimensional cone with apex $0$,
    and let $\delta > 0$ be any value.
    Then there are infinitely many integer points $x \in K$
    such that $B_\delta(x) \subset K$.
\end{lemma}

That is,
there are many points which are ``very inside'' $K$.

\begin{proof}
    Choose $x$ to be any rational point in the interior of the cone.
    By definition,
    there is some $\varepsilon > 0$ with $B_\varepsilon(x) \subseteq K$.
    For any $\lambda > 0$,
    we have
    \begin{equation*}
        \lambda B_\varepsilon(x) = B_{\lambda \varepsilon}( \lambda x ) \subseteq K.
    \end{equation*}

    For all sufficiently large $\lambda$,
    we have $\lambda \varepsilon > \delta$,
    so we just need to take the infinitely many integer $\lambda$
    such that $\lambda x$ is an integer vector.
\end{proof}

We will need the fact that these integral points are distant from the boundary
only in the proof of Theorem~\ref{thm:real-characterization}.
For the next theorem,
existence of infinitely many such points would be enough.

\begin{proposition}
    \label{thm:weak-only-if-part}
    Let $P$ be a full-dimensional polytope.
    If $0 \notin P$,
    then $L_P(s)$ is not a nondecreasing function.
    In fact, $L_P(s)$ has infinitely many ``drops'';
    that is,
    there are infinitely many points $s_0$
    such that $L_P(s_0) > L_P(s_0 + \varepsilon)$
    for sufficiently small $\varepsilon > 0$.
\end{proposition}

\begin{proof}
    Writing $P$ as in~\eqref{eq:intersection-representation},
    we conclude at least one of the $b_i$ must be negative
    (because the vector $0$ satisfies every linear restriction where $b_i \geq 0$).
    Equivalently (dividing both sides by $b_i$),
    there is a half-space of the form $\{x \mid \langle u, x \rangle \geq 1\}$
    such that some facet $F$ of $P$ is contained in
    $\{x \mid \langle u, x \rangle = 1\}$.

    Now, consider the cone $\bigcup_{\lambda > 0} \lambda F$.
    The previous lemma says
    there is an integral point $x_0$ in this cone,
    and so by its definition we have $x_0 \in s_0 F$ for some $s_0 > 0$;
    thus, $x_0 \in s_0 P$.
    We will argue that, for all sufficiently small $\varepsilon > 0$,
    the polytopes $(s_0 + \varepsilon)P$ do not contain any integral point
    which is not present in $s_0 P$ (Figure~\ref{fig:polytope-not-containing-origin}).

    \begin{figure}[t]
        \centering
        \begin{tikzpicture}
            % The polytope
            \def\polytope{(1.5, -0.5) -- ++(-2, 2) -- ++(2, 2) -- ++(2, -2) -- cycle}
            \filldraw[blue, thick, fill = blue!40] \polytope;
            \node [blue!50!black] at (1.5, 1.5) {$s_0 P$};

            % Scaled polytope
            \draw[scale = 1.15, green!30!black] \polytope;
            \node[green!30!black] at (4, 0.5) {$(s_0+\varepsilon) P$};

            % Facet F
            \draw[very thick, red] (1.5, -.5) -- ++(-2, 2)
                node[below=0.2cm] {$F$};

            % Lattice
            \begin{pgfonlayer}{background}
                \draw[lightgray] (-1, 0) -- (4, 0)
                            (0, -1) -- (0, 4);
            \end{pgfonlayer}{background}
            \foreach \x in {-1, ..., 4}
                \foreach \y in {-1, ..., 4}
                    \fill (\x, \y) circle [radius = 1pt];
            \node [below left] at (0, 0) {$0$};

            % Integral point x_0
            \fill (1, 0) circle [radius=2pt]
                node [below left] {$x_0$};
        \end{tikzpicture}

        \caption[
            If the polytope is dilated it will lose the point $x_0$.
        ]{
            The polytope $P$,
            when dilated,
            loses the point $x_0$,
            but if the dilation is small enough
            then it does not gain any new integral point.
            Therefore, $L_P(s)$ will decrease from $s_0$ to $s_0 + \varepsilon$.
        }
        \label{fig:polytope-not-containing-origin}
    \end{figure}

    For small $\varepsilon$ (say, $\varepsilon < 1$),
    all these polytopes are ``uniformly bounded'';
    that is, there is some $N$ such that $(s_0 + \varepsilon)P \subseteq [-N, N]^d$
    for all $0 \leq \varepsilon < 1$.
    Each integral $x$ in $[-N, N]^d$ which is not in $s_0 P$
    must violate a linear restriction of the form $\langle a_i, x \rangle \leq s_0 b_i$;
    that is, $\langle a_i, x \rangle > s_0 b_i$ for some $i$.
    As we are dealing with real variables,
    we also have $\langle a_i, x \rangle > (s_0 + \varepsilon) b_i$
    for all sufficiently small $\varepsilon$,
    say, for all $\varepsilon < \delta_x$ for some $\delta_x > 0$.
    Now, as there is a finite number of such relevant integral $x$,
    we can take $\delta$ to be the smallest of all such $\delta_x$;
    then if $0 < \varepsilon < \delta$
    every integral point of $(s_0 + \varepsilon)P$ also appears in $s_0 P$.

    But the special restriction $\langle u, x \rangle \geq 1$,
    considered above,
    ``dilates'' to the linear restriction
    $\langle u, x \rangle \geq s_0 + \varepsilon$ in $(s_0 + \varepsilon)P$.
    Since $x_0$ satisfy this restriction with equality for $\varepsilon = 0$,
    for any $\varepsilon > 0$ we will have $x_0 \notin (s_0 + \varepsilon)P$.
    Therefore,
    not only the dilates $(s_0 + \varepsilon)P$ do not contain new integral points
    (for small enough $\varepsilon$),
    but actually these dilates lose the point $x_0$ if $\varepsilon > 0$.
    Thus,
    $L_P(s_0) > L_P(s_0 + \varepsilon)$ for all sufficiently small $\varepsilon > 0$.

    Finally,
    there are infinitely many integral $x_0$ in the cone
    $\bigcup_{\lambda > 0} \lambda F$;
    thus we have infinitely many different values of $\norm{x_0}$
    and hence of $s_0$,
    and the reasoning above shows $L_P(s)$ ``drops'' in every such $s_0$.
\end{proof}

A simple consequence of this proposition is that
if $s_0$ is a point where $L_P(s)$ ``drops'',
then in the interval $\big[\!\floor{s_0}, \floor{s_0} + 1\big)$
the function $L_P(s)$ will not be a constant function,
so we cannot have $L_P(s) = L_P(\floor s)$ for all $s$
if $P$ does not contain the origin.

A more interesting consequence comes from the observation that,
if $P$ is a full-dimensional polytope which contains $0$,
then $L_P(s)$ is a nondecreasing function.
If $v$ is any integer vector such that $0 \notin P + v$,
then the proposition says $L_{P + v}(s)$ is not a nondecreasing function,
and so $L_P(s)$ and $L_{P + v}(s)$ must be different functions,
even though $L_P(t)$ and $L_{P + v}(t)$ are the same.
We will explore this consequence in more details
in Section~\ref{sec:translation-variant}.

Before continuing,
we observe how this proposition may be extended to non-full-dimensional polytopes.
For any polytope $P \subseteq \mathbb R^d$,
if $s$ is sufficiently small
we have $sP \subseteq [-1, 1]^d$,
so if $P$ does not contain the origin
we have
\begin{equation*}
    \lim_{s \to 0^+} L_P(s) = 0.
\end{equation*}

Since we agreed that $L_P(0) = 1$,
the function $L_P(s)$ will not be nondecreasing
for any polytopes which does not contain the origin,
however we may not conclude anything about the infinitely many drops.
For example,
if $P = \conv\{(1, \pi), (2, 2\pi)\}$,
then $L_P(s) = 0$ whenever $s > 0$,
so $L_P$ has only one drop
--- at $s = 0$.

We are now able to show that,
for full-dimensional polytopes,
the converse of Theorem~\ref{thm:if-part} holds.

\begin{restatetheorem}{thm:real-characterization}
    Let $P$ be a full-dimensional real polytope.
    Then $L_P(s) = L_P(\floor s)$ for all $s$
    if and only if
    the polytope $P$ can be written as in~\eqref{eq:intersection-representation}
    where each $b_i$ is either $0$ or $1$,
    and when $b_i = 1$ the vector $a_i$ must be integral.
\end{restatetheorem}

\begin{proof}
    Since $0 \in P$, by the previous proposition,
    we know we can write $P$ as in~\eqref{eq:intersection-representation},
    with each $b_i$ being nonnegative.
    By dividing each inequality by the corresponding $b_i$ (if $b_i \neq 0$),
    we may assume each $b_i$ is either $0$ or $1$.
    Now we must show that when $b_i = 1$,
    the vector $a_i$ will be integral.

    Let $\langle u, x \rangle \leq 1$ be one of the linear restrictions where $b_i = 1$.
    Using an approach similar to the proof of the previous proposition,
    we will show that $u$ must be an integral vector.

    Let $F$ be the facet of $P$ which is contained in $\{\langle u, x \rangle = 1\}$,
    and again define $K = \bigcup_{\lambda \geq 0} \lambda F$.
    Suppose $u$ has a non-integer coordinate;
    first, we will find an integral point $x_0$ and a non-integer $s_0 > 0$
    such that $x_0 \in s_0 F$.

    By Lemma~\ref{thm:integral-points-in-cone}
    (using $\delta = \frac32$),
    there is some integral $y \in K$ such that $B_{\frac32}(y) \subset K$.
    If $\langle u, y \rangle$ is not an integer,
    we may let $x_0 = y$ and $s_0 = \langle u, y \rangle$;
    then $x_0$ is an integral point which is in the relative interior of $s_0 F$.
    If $\langle u, y \rangle$ is an integer,
    as $u$ is not an integral vector,
    some of its coordinates is not an integer,
    say the $j$th;
    then $\langle u, y + e_j \rangle$ will not be an integer,
    so (as $y + e_j \in B_{\frac32}(y) \subset K$)
    we may let $x_0 = y + e_j$ and $s_0 = \langle u, y + e_j \rangle$
    to obtain our desired integral point which is in a non-integral dilate of $F$.

    We have $sP \subset s_0 P$ for $s < s_0$,
    but $x_0 \notin sP$ for any $s < s_0$,
    so $L_P(s) < L_P(s_0)$ for all $s < s_0$.
    As $s_0$ is not an integer (by construction),
    this shows that $L_P(\floor{s_0}) < L_P(s_0)$,
    a contradiction.
\end{proof}

Finally,
using unimodular transforms,
we may show the characterization for rational polytopes.

\begin{restatetheorem}{thm:characterization}
    Let $P$ be a rational polytope
    written as in~\eqref{eq:intersection-representation}.
    Then $P$ is semi-reflexive
    if and only if all $a_i$ are integers
    and all $b_i$ are either $0$ or $1$.
\end{restatetheorem}

\begin{proof}
    The ``if'' part is Theorem~\ref{thm:if-part},
    so assume that $L_P(s) = L_P(\floor s)$.
    If $P$ is full-dimensional,
    we just need to apply Theorem~\ref{thm:real-characterization}:
    if any of the $a_i$ is non-integer,
    then it must be rational (because $P$ is rational)
    and the corresponding $b_i$ must be zero,
    so we may just multiply the inequality
    by the $\lcm$ of the denominators of the coordinates of $a_i$.
    Thus, assume that $P$ is not full-dimensional.

    By the discussion following Proposition~\ref{thm:weak-only-if-part},
    we must have $0 \in P$.
    Let $M = \aff P$,
    the affine hull of $P$.
    Since $P$ contains the origin,
    $M$ is a vector space;
    since $P$ is rational,
    $M$ is spanned by integer points.
    Let $\dim P$ be the dimension of $P$ (and of $M$)
    and let $d$ be the dimension of the ambient space
    (so that $P \subseteq \mathbb R^d$).
    Then there is a unimodular transform $A$
    which maps $M$ to $\mathbb R^{\dim P} \times \{0\}^{d - \dim P}$.

    If $P$ is contained in the half-space
    \begin{equation*}
        \{x \in \mathbb R^d \mid \langle a, x \rangle \leq b\},
    \end{equation*}
    then $AP$ is contained in the half-space
    \begin{equation*}
        \{x \in \mathbb R^d \mid \langle M^{-t}a, x \rangle \leq b\},
    \end{equation*}
    so applying unimodular transforms
    do not change neither the hypothesis nor the conclusions.
    Thus,
    let $Q$ be the projection of $AP$ to $\mathbb R^{\dim P}$;
    then $Q$ is a semi-reflexive polytope
    (because $L_Q(s) = L_P(s)$),
    so we may apply Theorem~\ref{thm:real-characterization} to conclude the proof.
\end{proof}

We finish this section by remarking that
the hypothesis of $P$ being either full-dimensional or rational
is indeed necessary.
For example,
if $H$ is the set of vectors which are orthogonal to
$(\ln 2, \ln 3, \ln 5, \ln 7, \dots, \ln p_d)$
(where $p_d$ is the $d$th prime number)
and $x = (x_1, \dots, x_d)$ is an integral vector,
$x \in H$ if and only if
\begin{equation*}
    x_1 \log 2 + \dots x_d \log p_d = 0,
\end{equation*}
which (by applying $e^x$ to both sides)
we may rewrite as
\begin{equation*}
    2^{x_1} 3^{x_2} \dots p_d^{x_d} = 1,
\end{equation*}
which is only possible if $x_1 = \dots = x_d = 0$.
That is,
the only integral point in $H$ is the origin.
So,
if $P \subseteq H$,
then $L_P(s)$ will be a constant function,
regardless of any other assumptions over $P$.
