\section[Two polytopes with the same Ehrhart function]
{Application: two polytopes having the same Ehrhart function}
\label{sec:same-ehrhart-function}

Let $P$ and $Q$ be the $3$-dimensional polytopes
\begin{align*}
    P &= \conv\Big\{
            (0, 0, 0),
            (0, \tfrac12, \tfrac12),
            (\tfrac12, 0, \tfrac12),
            (\tfrac12, \tfrac12, 0)
        \Big\} \\
    Q &= \conv\Big\{
            (0, 0, 0),
            (0, \tfrac12, 0),
            (\tfrac12, \tfrac12, 0),
            (\tfrac12, 0, 0),
            (\tfrac14, \tfrac14, \tfrac12)
        \Big\}
\end{align*}
(Figure~\ref{fig:tetrahedron-and-pyramid}).
First,
we will show these two polytopes are semi-reflexive.
Then,
interpolating polynomials,
we will show that $L_P(t) = L_Q(t)$.
This will allow us to conclude their real Ehrhart functions are also the same.%
\footnote{
    \label{fn:paper-cris}
    $P$ and $Q$ are the quasi-metric polytopes
    associated with the graphs
    \tikz[scale=0.25]{
        \fill circle [radius = 4pt] (0, 0);
        \filldraw (0, 0) -- (90:0.5cm) circle [radius = 4pt];
        \filldraw (0, 0) -- (210:0.5cm) circle [radius = 4pt];
        \filldraw (0, 0) -- (330:0.5cm) circle [radius = 4pt];
    }
    and
    \tikz[scale=0.25]{
        \fill (0, 0) circle [radius = 4pt];
        \fill (2cm, 0) circle [radius = 4pt];
        \draw (0, 0) -- (2cm, 0);
        \draw (0, 0) to [bend left] (2cm, 0);
        \draw (0, 0) to [bend right] (2cm, 0);
    },
    respectively.
    This specific example was taken from a paper by
    Fernandes, Pina, Ramírez Alfonsín, and Robins~\cite{Cris2017}.
    This paper shows that,
    if two $\{1, 3\}$-graphs are equivalent
    under nearest-neighbor interchange operations,
    then the associated polytopes will have the same (integer) Ehrhart quasipolynomial.
    Since all these polytopes are semi-rational,
    this paper provides a whole family of rational polytopes
    with the same real Ehrhart function.
}

\begin{figure}[p]
    \tikzset{every picture/.style = {
        scale = 1.5,
        y = {(-.5cm, -.2cm)},
        z = {(0cm, 1cm)}, % Reverse y and z graphics
    }}
    \newcommand\drawtetrahedron[1]{
        % Creates the coordinates A, B, C, D and draws the tetrahedron.
        % The argument is the dilation factor.
        \coordinate (A) at (0, 0, 0);
        \coordinate (B) at (0, #1*.5, #1*.5);
        \coordinate (C) at (#1*.5, 0, #1*.5);
        \coordinate (D) at (#1*.5, #1*.5, 0);

        \draw[blue, thick] (A) -- (B)
            (A) -- (C)
            (A) -- (D)
            (B) -- (C) -- (D) -- cycle;
    }
    \newcommand\drawpyramid[1]{
        \coordinate (A) at (0, 0, 0);
        \coordinate (B) at (0, #1*.5, 0);
        \coordinate (C) at (#1*.5, #1*.5, 0);
        \coordinate (D) at (#1*.5, 0, 0);
        \coordinate (E) at (#1*.25, #1*.25, #1*.5);

        \draw[blue, thick] (E) -- (A)
            (E) -- (B)
            (E) -- (C)
            (E) -- (D)
            (A) -- (B) -- (C) -- (D) -- cycle;
    }
    \newcommand\drawaxis{
        \draw[->] (0, 0, 0) -- (2, 0, 0) node[above] {$x$};
        \draw[->] (0, 0, 0) -- (0, 2, 0) node[left] {$y$};
        \draw[->] (0, 0, 0) -- (0, 0, 2) node[left] {$z$};
    }
    \newcommand\drawintegerpoints[1]{
        \foreach \point in {#1}
            \fill \point circle [radius = 1pt];
    }
    \centering

    \begin{tikzpicture}
        \drawaxis
        \drawtetrahedron{1}
        \drawintegerpoints{(0, 0, 0)}
    \end{tikzpicture}
    \qquad
    \begin{tikzpicture}
        \drawaxis
        \drawpyramid{1}
        \drawintegerpoints{(0, 0, 0)}
    \end{tikzpicture}

    \begin{tikzpicture}
        \drawaxis
        \drawtetrahedron{2}
        \drawintegerpoints{(0, 0, 0), (1, 1, 0), (1, 0, 1), (0, 1, 1)}
    \end{tikzpicture}
    \qquad
    \begin{tikzpicture}
        \drawaxis
        \drawpyramid{2}
        \drawintegerpoints{(0, 0, 0), (1, 0, 0), (1, 1, 0), (0, 1, 0)}
    \end{tikzpicture}

    \begin{tikzpicture}
        \drawaxis
        \drawtetrahedron{3}
        \drawintegerpoints{(0, 0, 0), (1, 1, 0), (1, 0, 1), (0, 1, 1), (1, 1, 1)}
        \draw [dotted]
            (1, 1, 1) -- (0, 1, 1)
            (1, 1, 1) -- (1, 0, 1)
            (1, 1, 1) -- (1, 1, 0);
    \end{tikzpicture}
    \qquad
    \begin{tikzpicture}
        \drawaxis
        \drawpyramid{3}
        \drawintegerpoints{(0, 0, 0), (1, 0, 0), (1, 1, 0), (0, 1, 0), (1, 1, 1)}
        \draw [dotted]
            (1, 1, 0) -- (0, 1, 0)
            (1, 1, 0) -- (1, 0, 0)
            (1, 1, 0) -- (1, 1, 1);
    \end{tikzpicture}

    \begin{tikzpicture}
        \drawaxis
        \drawtetrahedron{4}
        \drawintegerpoints{
            (0, 0, 0), (1, 1, 0), (1, 0, 1), (0, 1, 1),
                       (2, 2, 0), (2, 0, 2), (0, 2, 2),
            (1, 1, 2), (1, 2, 1), (2, 1, 1)}
        \draw [dotted]
            (2, 1, 1) -- (0, 1, 1)
            (1, 2, 1) -- (1, 0, 1)
            (1, 1, 2) -- (1, 1, 0);
        \fill[gray] (1, 1, 1) circle [radius = 1pt]; % Interior point
    \end{tikzpicture}
    \qquad
    \begin{tikzpicture}
        \drawaxis
        \drawpyramid{4}
        \drawintegerpoints{
            (0, 0, 0), (1, 0, 0), (1, 1, 0), (0, 1, 0),
                       (2, 0, 0), (2, 2, 0), (0, 2, 0),
            (1, 1, 2), (1, 2, 0), (2, 1, 0)}
        \draw [dotted]
            (2, 1, 0) -- (0, 1, 0)
            (1, 2, 0) -- (1, 0, 0)
            (1, 1, 0) -- (1, 1, 2);
        \fill[gray] (1, 1, 1) circle [radius = 1pt];
    \end{tikzpicture}

    \caption[
        Polytopes $P$ and $Q$ and their dilates.
    ]{
        Polytopes $P$ and $Q$ and their dilates.
        The black points are integer points in the boundary of each dilate;
        the gray points are in the interior.
    }
    \label{fig:tetrahedron-and-pyramid}
\end{figure}

$P$ is a tetrahedron,
so each $3$-element subset of its vertex set
span a supporting hyperplane of $P$.
Then we just need to look at the other point to determine the direction.
We get the inequalities
\begin{align*}
     x - y - z &\leq 0 \\
    -x + y - z &\leq 0 \\
    -x - y + z &\leq 0 \\
     x + y + z &\leq 1
\end{align*}

Theorem~\ref{thm:rational-ehrhart} says that
\begin{equation*}
    L_P(t) = (\vol P) t^3 + c_2(t)t^2 + c_1(t) t + c_0(t),
\end{equation*}
where each $c_i$ has period $2$.
A different interpretation of the periodicity of the $c_i$
is to look at each congruency class modulo $2$ of $t$;
since these functions will be constant in each congruency class,
we know there are two polynomials $p_0$ and $p_1$ such that
\begin{equation*}
    L_P(t) = \begin{cases}
        p_0(t), &\text{if $t \equiv 0 \pmod 2$;} \\
        p_1(t), &\text{if $t \equiv 1 \pmod 2$.} \\
    \end{cases}
\end{equation*}

To get the coefficients of $p_0$ and $p_1$,
we will use polynomial interpolation.
According to Theorem~\ref{thm:reciprocity},
we know $L_P(-t) = - L_{P^\circ}(t)$,
so we may interpolate the points $-3$, $-1$, $1$ and $3$
to get the formula for $p_1$.
Using the inequalities above to test all integral points in the cube $[0, 1]^3$
(or looking at Figure~\ref{fig:tetrahedron-and-pyramid}),
we get
\begin{align*}
    p_1(-1) = - L_{P^\circ}(-1) &= 0 & p_1(1) = L_{P}(1) &= 1 \\
    p_1(-3) = - L_{P^\circ}(-3) &= 0 & p_1(3) = L_{P}(3) &= 5
\end{align*}
so polynomial interpolation allows us to determine that
\begin{equation*}
    p_1(t) = \frac{1}{24} t^3 + \frac{1}{4} t^2 + \frac{11}{24} t + \frac{1}{4}.
\end{equation*}

% TODO: Note we get the volume here,
% and mention Ehrhart's theorem as the "moral successor" of Pick's theorem

To compute $p_0$,
we will do the same thing.
Interpolating, for example, at $-4$, $-2$, $2$ and $4$,
we have
\begin{align*}
    p_0(-2) = - L_{P^\circ}(-2) &= 0 & p_0(2) = L_{P}(2) &= 4 \\
    p_0(-4) = - L_{P^\circ}(-4) &= -1 & p_0(4) = L_{P}(4) &= 11
\end{align*}
so that
\begin{equation*}
    p_0(t) = \frac{1}{24} t^3 + \frac{1}{4} t^2 + \frac{5}{6} t + 1.
\end{equation*}

Now let us compute $L_Q(t)$.
The polytope $Q$ has five vertices,
so there are $10$ hyperplanes to analyze.
Two of them
(each spanneed by two opposite vertices of the base together with the top vertex)
yield ``invalid'' hyperplanes,
and each of the four $3$-set spanned by the vertices in the base
yield the same hyperplane.
We get the inequalities
\begin{align*}
        -z & \leq 0 \\
    2x + z & \leq 1 \\
    -2x + z & \leq 0 \\
    2y + z & \leq 1 \\
    -2y + z & \leq 0 \\
\end{align*}

Since the denominator of $Q$ is $4$,
now we have four different polynomials $q_0$, $q_1$, $q_2$ and $q_3$
for each congruence class of $t$ modulo $4$.
To compute,
for example,
$q_1$,
we need to evaluate (say)
$L_P(1) = q_1(1)$, $L_P(5) = q_1(5)$, $L_{P^\circ}(-3) = -q_1(-3)$
and $L_{P^\circ}(-7) = -q_1(-7)$,
and then interpolate these values.
(Now its better to use a computer to do this calculation.)
We get
\begin{align*}
    q_0(t) = q_2(t) &= \frac{1}{24} t^3 + \frac{1}{4} t^2 + \frac{5}{6} t + 1, \\
    q_1(t) = q_3(t) &= \frac{1}{24} t^3 + \frac{1}{4} t^2 + \frac{11}{24} t + \frac{1}{4}.
\end{align*}

This shows that $L_P(t) = L_Q(t)$ for all integers $t$.
Finally,
using Theorem~\ref{thm:characterization},
it is clear from the inequalities above
that both $P$ and $Q$ are semi-rational,
and thus we may conclude that $L_P(s) = L_Q(s)$ for all $s$.
