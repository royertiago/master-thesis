\section[$L_P(s)$ is highly non-translation invariant]
{A digression: $L_P(s)$ is highly non-translation invariant}
\label{sec:translation-variant}

It is easy to show that both the integer and the real Ehrhart functions
are invariant under unimodular transforms;
that is,
$L_P(s) = L_{AP}(s)$ for all unimodular transforms $A$.
It is also easy to show that the integer Ehrhart function
is invariant under integer translation;
that is,
$L_P(t) = L_{P + w}(t)$ for all integer vectors~$w$.

One of the consequences of Theorem~\ref{thm:weak-only-if-part},
mentioned after its proof,
is that for full-dimensional polytopes $P$ which contain the origin,
$L_P(s)$ and $L_{P + v}(s)$ will be different functions
for all vectors $v$ which ``take $P$ out of the origin'';
that is, all $v$ such that $0 \notin P + v$.
This section expands on this issue,
and shows how bad behaved the real Ehrhart function can be
with respect to translation.

We will first define an operation,
called ``pseudopyramid'',
that constructs a polytope $\ppyr(P)$ from a polytope $P$.
This operation will have the property that,
if $\ppyr(P)$ and $\ppyr(Q)$ have different volumes,
then $L_P(s)$ and $L_Q(s)$ differ in infinitely many points.
Then we will show that,
whenever $P$ is a polytope which does not contain the origin
and $v \in P$,
all pseudopyramids $\ppyr(P + \lambda v)$
(for $\lambda \geq 0$)
will have different volumes.

Let $P \subseteq \mathbb R^d$ be any polytope.
Define $\ppyr(P)$ to be the convex hull of $P \cup \{0\}$,
or,
equivalently,
\begin{equation*}
    \ppyr(P) = \bigcup_{0 \leq \lambda \leq 1} \lambda P
\end{equation*}
(Figure~\ref{fig:pseudopyramid}).
The pseudopyramid is so called
because it resembles the operation of creating a pyramid over a polytope.
Note however that the pseudopyramid lives in the same ambient space as the polytope,
whereas the pyramid over a polytope is a polytope in one higher dimension
(that is, $\ppyr(P) \subseteq \mathbb R^d$
while $\operatorname{pyr}(P) \subseteq \mathbb R^{d+1}$).

\begin{figure}[t]
    \centering
    \begin{tikzpicture}
        \draw (-1, 0) -- (3, 0);
        \draw (0, -1) -- (0, 3); % Axis
        \filldraw [thick, blue, fill = blue!40] (1, 1) rectangle (2.5, 2.5);
        \node at (1.75, 1.75) {$P$};

        \begin{scope}[xshift = 5cm]
            \draw (-1, 0) -- (3, 0);
            \draw (0, -1) -- (0, 3); % Axis
            \filldraw [thick, blue, fill = blue!40]
                (0, 0) -- (1, 2.5) -- (2.5, 2.5) -- (2.5, 1) -- cycle;
            \node at (1.75, 1.75) {$\ppyr(P)$};
        \end{scope}
    \end{tikzpicture}
    \caption{
        Pseudopyramid of a polytope.
    }
    \label{fig:pseudopyramid}
\end{figure}

The following lemma says that
we may compute the Ehrhart function of $P$
from the Ehrhart function of $\ppyr P$.

\begin{lemma}
    \label{thm:different-pseudopyramid-volumes}
    Let $P$ and $Q$ be real polytopes such that $L_P(s) = L_Q(s)$.
    Then $L_{\ppyr P}(s) = L_{\ppyr Q}(s)$.
\end{lemma}

Thus,
if,
for example,
$\ppyr P$ and $\ppyr Q$ have different volumes,
then these polytopes will have different Ehrhart functions
and we will be sure that $P$ and $Q$ also have different Ehrhart functions.

\begin{proof}
    First,
    we will define an operation,
    called ``lifting'',
    which we will use to reconstruct $L_{\ppyr P}(s)$ from $L_P(s)$.

    Let $f: \mathbb R \to \mathbb R$
    be any function which has a jump-discontinuity at a point $s_0$,
    and denote by $f(s_0^+)$ the limit of $f(s)$ as $s \to s_0$ with $s > s_0$.
    Define a function $g: \mathbb R \to \mathbb R$ by
    \begin{equation*}
        g(s) = \begin{cases}
            f(s), & \text{if $s \leq s_0$;} \\
            f(s) - f(s_0^+) + f(s_0), & \text{if $s > s_0$.}
        \end{cases}
    \end{equation*}
    The function $g$ is right-continuous at $s_0$ by construction.
    Call $g$ the \emph{result of lifting $f$ at $s_0$}.
    For example,
    if $f$ is the indicator function of $[0, 1]$,
    the result of lifting $f$ at $1$ is the indicator function of $[0, \infty)$.

    If the discontinuity points of $f$ are $s_0 < s_1 < s_2 < \dots$,
    we may successively lift the function at these points;
    that is,
    let $f_0 = f$
    and for $k \geq 0$ let $f_{k+1}$ be the result of lifting $f_k$ at $s_k$.
    If $s < s_n$ for some $n$,
    then for all $k > n$ we have $f_k(s) = f_n(s)$,
    so that the functions $f_k$ converge pointwise at every $s \in \mathbb R$.
    Let $g$ be this pointwise limit;
    we will call $g$ the \emph{lifting} of $f$.
    (Figure~\ref{fig:ehrhart-function-of-ppyr}
    shows the graph of the lifting of the function
    depicted in Figure~\ref{fig:ehrhart-function-of-square}.)

    \begin{figure}[t]
        \pgfplotsset{
            every axis/.style = {
                width = 5cm,
                axis lines = center,
                clip = false,
                xmin = 0, xmax = 1.25,
            },
            axis line style = {gray},
        }
        \begin{subfigure}{.5\linewidth}
            \centering
            \begin{tikzpicture}
                \draw [gray] (-1, 0) -- (3, 0);
                \draw [gray] (0, -1) -- (0, 3);

                \filldraw [blue, fill = blue!40, thick]
                    (1, 1) -- (2, 1) -- (2, 0) -- (1, 0) -- cycle;

                \draw [blue, thick] % The pseudopyramid
                    (0, 0) -- (1, 1) -- (2, 1) -- (2, 0) -- (1, 0) -- cycle;

                \foreach \x in {-1, ..., 3}
                    \foreach \y in {-1, ..., 3}
                        \fill (\x, \y) circle [radius = 1pt];

                \foreach \point in {(1, 1), (2, 1), (2, 0), (1, 0)}
                    \fill \point circle [radius = 2pt];
            \end{tikzpicture}
            \caption{}
            \label{fig:ppyr-of-square}
        \end{subfigure}
        %
        \begin{subfigure}{.5\linewidth}
            \centering
            \pgfplotsset{
                every axis/.style = {
                    xtick = \empty,
                    ytick = \empty,
                    width = 5cm,
                    height = 2.5cm,
                    axis lines = center,
                    clip = false,
                    xmin = 0, xmax = 1.25,
                    ymin = 0, ymax = 1,
                },
            }

            \begin{tikzpicture}
                \node at (-1, 0.5) {$\mathds 1_{(1, 0)}$};
                \begin{axis}
                    \draw [thick] (0, 0) -- (0.5, 0)
                        (0.5, 1) -- (1, 1)
                        (1, 0) -- (1.25, 0);
                    \draw [dashed] (0.5, 0) -- (0.5, 1)
                        (1, 0) -- (1, 1);
                    \addplot [closed dot] coordinates {(0.5, 1) (1, 1)};
                    \addplot [open dot] coordinates {(0.5, 0) (1, 0)};
                \end{axis}
            \end{tikzpicture}
            \\[1em]

            \begin{tikzpicture}
                \node at (-1, 0.5) {$\mathds 1_{(1, 1)}$};
                \begin{axis}
                    \draw [thick] (0, 0) -- (1.25, 0);
                    \draw [dashed] (1, 0) -- (1, 1);
                    \addplot [closed dot] coordinates {(1, 1)};
                    \addplot [open dot] coordinates {(1, 0)};
                \end{axis}
            \end{tikzpicture}
            \\[1em]

            \begin{tikzpicture}
                \node at (-1, 0.5) {$\mathds 1_{(2, 0)}$};
                \begin{axis}
                    \draw [thick] (0, 0) -- (1, 0)
                        (1, 1) -- (1.25, 1);
                    \draw [dashed] (1, 0) -- (1, 1);
                    \addplot [closed dot] coordinates {(1, 1)};
                    \addplot [open dot] coordinates {(1, 0)};
                \end{axis}
            \end{tikzpicture}
            \\[1em]

            \begin{tikzpicture}
                \node at (-1, 0.5) {$\mathds 1_{(2, 1)}$};
                \begin{axis}[xtick = {0.5, 1}]
                    \draw [thick] (0, 0) -- (1, 0)
                        (1, 1) -- (1.25, 1);
                    \draw [dashed] (1, 0) -- (1, 1);
                    \addplot [closed dot] coordinates {(1, 1)};
                    \addplot [open dot] coordinates {(1, 0)};
                \end{axis}
            \end{tikzpicture}

            \caption{}
            \label{fig:indicator-function-of-points}
        \end{subfigure}

        \begin{subfigure}{.5\linewidth} % L_P
            \centering
            \begin{tikzpicture}
                \begin{axis} [
                    ytick = {0, ..., 5},
                    ymin = 0, ymax = 5, % To synchronize with the graph below
                ]
                    \addplot [closed dot] coordinates
                        {(0, 1) (0.5, 1) (1, 4)};
                    \addplot [open dot] coordinates
                        {(0, 0) (0.5, 0) (1, 1) (1, 2)};
                    \draw [thick] (0, 0) -- (0.5, 0)
                        (0.5, 1) -- (1, 1)
                        (1, 2) -- (1.25, 2);
                    \draw [dashed] (0, 0) -- (0, 1)
                        (0.5, 0) -- (0.5, 1)
                        (1, 1) -- (1, 4);
                \end{axis}
            \end{tikzpicture}

            \caption{}
            \label{fig:ehrhart-function-of-square}
        \end{subfigure}
        %
        \begin{subfigure}{.5\linewidth} % L_ppyr P
            \centering
            \begin{tikzpicture}
                \begin{axis} [
                    ytick = {0, ..., 5},
                    ymin = 0, ymax = 5, % To synchronize with the graph above
                ]
                    \addplot [closed dot] coordinates
                        {(0, 1) (0.5, 2) (1, 5)};
                    \addplot [open dot] coordinates
                        {(0, 0) (0.5, 1) (1, 2)};
                    \draw [thick] (0, 1) -- (0.5, 1)
                        (0.5, 2) -- (1, 2)
                        (1, 5) -- (1.25, 5);
                    \draw [dashed] (0.5, 1) -- (0.5, 2)
                        (1, 2) -- (1, 5);
                \end{axis}
            \end{tikzpicture}

            \caption{}
            \label{fig:ehrhart-function-of-ppyr}
        \end{subfigure}
        \caption[
            Computing $L_{\ppyr P}(s)$ from $L_P(s)$.
        ] {
            Computing $L_{\ppyr P}(s)$ from $L_P(s)$.
            (\subref{fig:ppyr-of-square}): Polytope $P = [1, 2] \times [0, 1]$,
            and an outline of its pseudopyramid.
            (\subref{fig:indicator-function-of-points}): ``Indicator functions''
            $\mathds 1_x(s)$ of the points $(1, 0)$, $(1, 1)$, $(2, 0)$ and $(2, 1)$.
            (\subref{fig:ehrhart-function-of-square}): Function $L_P(s)$.
            (\subref{fig:ehrhart-function-of-ppyr}): Function $L_{\ppyr P}(s)$.
        }
        \label{fig:l-ppyr-from-l-p}
    \end{figure}

    Fix the polytope $P$;
    we will show that $L_{\ppyr P}(s)$ is the lifting of $L_P(s)$.

    Given a point $x$, define $\mathds 1_x(s) = [x \in sP]$
    (the ``indicator function'' of $x$);
    that is, $\mathds 1_x(s) = 1$ if $x \in sP$ and $0$ otherwise.
    Note we have $L_P = \sum_{x \in \mathbb Z^d} \mathds 1_x$.

    Observe that $\mathds 1_x$ is the indicator function of a closed interval.
    If this interval is $[a, b]$,
    denote by $\mathds 1'_x$ the result of lifting $\mathds 1_x$ at $b$.
    If the interval is $\emptyset$ or $[a, \infty)$,
    just let $\mathds 1'_x = \mathds 1_x$.
    Since we have
    \begin{equation*}
        s \ppyr P = \bigcup_{0 \leq \lambda \leq s} \lambda P,
    \end{equation*}
    we know that $x \in s \ppyr P$ whenever $s \geq a$,
    so we have $\mathds 1'_x(s) = [x \in s \ppyr P]$;
    therefore,
    \begin{equation*}
        L_{\ppyr P} = \sum_{x \in \mathbb Z^d} \mathds 1'_x.
    \end{equation*}

    It is a simple exercise showing the lifting of a sum of finitely many functions
    is the sum of their liftings.
    Let $N > 0$ be fixed.
    If we look only for $s < N$,
    only finitely many of the functions $\mathds 1_x$ will be nonzero,
    so we may apply this result.
    If $f$ is the lifting of $L_P(s)$,
    for $s < N$ we have
    \begin{equation*}
        f(s) = \sum_{x \in \mathbb Z^d} \mathds 1'_x = L_{\ppyr P}(s).
    \end{equation*}
    As $N$ was arbitrary, we conclude $L_{\ppyr P}$ is the lifting of $L_P$.

    Finally,
    if $L_P(s) = L_Q(s)$,
    then their liftings $L_{\ppyr P}(s)$ and $L_{\ppyr Q}(s)$ will be equal.
\end{proof}

In order to use this lemma,
we will decompose the pseudopyramid in several interior-disjoint pieces
and show that some of them get ``larger'' when the polytope is translated.
Since we have
\begin{equation*}
    \lim_{s \to \infty} \frac{ L_{\ppyr P}(s) }{s^d} = \vol \ppyr P,
\end{equation*}
once we show that $\ppyr P$ and $\ppyr (P + w)$ have different volumes,
Lemma~\ref{thm:different-pseudopyramid-volumes}
will guarantee that $L_P(s)$ and $L_{P + w}(s)$ are different.

If $P \subseteq \mathbb R^d$ is a full-dimensional polytope with $n$ facets,
write $P$ as
\begin{equation*}
    P = \bigcap_{i = 1}^n \{ x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
    \tag{\ref{eq:intersection-representation} revisited}
\end{equation*}
so that each of its facets $F_i$ are defined by
\begin{equation*}
    F_i = P \cap \{x \in \mathbb R^d \mid \langle a_i, x \rangle = b_i \}.
\end{equation*}

\begin{figure}[t]
    \tikzset{
        every path/.style = {line join = bevel}
    }
    \centering
    \begin{subfigure}{.4\linewidth}
        \centering
        \begin{tikzpicture}
            \draw (-1, 0) -- (3, 0);
            \draw (0, -1) -- (0, 3);
            \filldraw [blue, thick, fill = blue!40]
                (1, 1) -- (2, 2) -- (2.5, 2) -- (2.5, 0.5) -- (1.5, 0.5) -- cycle;
            \draw [red, very thick]
                (2.5, 0.5) -- (1.5, 0.5) -- (1, 1);
        \end{tikzpicture}
        \caption{} % No caption
        \label{fig:back-facets}
    \end{subfigure}
    \qquad
    \begin{subfigure}{.4\linewidth}
        \centering
        \begin{tikzpicture}
            \draw (-1, 0) -- (3, 0);
            \draw (0, -1) -- (0, 3);
            \filldraw [blue, thick, fill = blue!40]
                (1, 1) -- (2, 2) -- (2.5, 2) -- (2.5, 0.5) -- (1.5, 0.5) -- cycle;
            \filldraw [blue, thick, fill = blue!40]
                (0, 0) -- (1.5, 0.5) -- (1, 1) -- cycle;
            \filldraw [blue, thick, fill = blue!40]
                (0, 0) -- (2.5, 0.5) -- (1.5, 0.5) -- cycle;
        \end{tikzpicture}
        \caption{} % No caption
        \label{fig:pseudopyramid-decomposition}
    \end{subfigure}
    \caption{
        (\subref{fig:back-facets}): Back facets (in red) of a polytope.
        (\subref{fig:pseudopyramid-decomposition}):
        Decomposition of the pseudopyramid $\ppyr P$ of a polytope $P$
        in $P$ and in pseudopyramids of its back facets.
    }
\end{figure}

Call $F_i$ a \emph{back facet} of $P$ if $b_i < 0$
(Figure~\ref{fig:back-facets}).
The pseudopyramid $\ppyr F_i$ will intersect $P$,
but as $b_i < 0$ the interiors of these two full-dimensional polytopes are disjoint.
This idea leads to the following decomposition lemma.

\begin{lemma}
    \label{thm:pseudopyramid-decomposition}
    The pseudopyramid of a full-dimensional real polytope $P$
    is the interior-disjoint union of $P$ and the pseudopyramids $\ppyr F$
    of the back facets of $P$.
\end{lemma}

\begin{proof}
    Let $P$ be the polytope we are decomposing.
    If $x \in \ppyr(P)$,
    then $x = \lambda y$ for some $y \in P$.
    If $x \in P$, great;
    otherwise we may assume $y$ is in the boundary of $P$
    (otherwise we may replace $y$ by a shrunk version which is in the boundary).
    Then $\lambda' y \notin P$ for all $\lambda' < 1$,
    so $y$ is ``visible from the origin''.
    Therefore $y \in F$ for some facet $F$ of $P$ which is contained in the back shell,
    and thus $x \in \ppyr(F)$,
    which is a polytope of the pseudopyramid decomposition.

    This shows the pseudopyramid of $P$
    is the union of the pseudopyramid decomposition of $P$.
    Now we will show interior-disjointness.

    If $x$ is in the interior of a $\ppyr(F)$ for a back facet $F$ of $P$,
    then $x$ violates the linear restriction of $F$
    and thus is not contained in $P$.
    Besides,
    the ``projection of $x$ in the back shell of $P$''
    (that is,
    the shortest vector $\lambda x$ which is contained in $P$)
    is contained in the relative interior of the facet $F$,
    so it is not contained in any other facet of $P$,
    and thus $x$ is not contained in any other pseudopyramid of a facet of $P$.
\end{proof}

For the next lemma,
we will also need the fact that the volume of a pyramid
is proportional to its height and to the area of its base;
more specifically,
a pyramid in $\mathbb R^d$ with height $h$
and whose base has $(d-1)$-dimensional area $A$
has volume $\frac{Ah}{d}$.

\begin{lemma}
    \label{thm:full-dimensional-translation-variant}
    Let $P$ be a full-dimensional real polytope which does not contain the origin
    and $v$ any point of $P$.
    Then for any reals $\lambda > \mu \geq 0$,
    the functions $L_{P + \lambda v}(s)$ and $L_{P + \mu v}(s)$
    will differ at infinitely many points.
\end{lemma}

\begin{proof}
    Write $P$ as
    \begin{equation*}
        P = \bigcap_{i = 1}^n \{x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
    \end{equation*}
    where $n$ is the number of facets of $P$,
    so that each facet $F_i$ can be written as
    \begin{equation*}
        F_i = P \cap \{x \in \mathbb R^d \mid \langle a_i, x \rangle = b_i \}.
    \end{equation*}

    The facets of $P + \lambda v$ are of the form $F_i + \lambda v$.
    We will show that,
    for $\lambda > \mu \geq 0$,
    if $F_i + \mu v$ is a back facet of $P + \mu v$,
    then $F_i + \lambda v$ is also a back facet of $P + \lambda v$,
    and that the volume of $\ppyr(F_i + \mu v)$
    is strictly smaller than the volume of $\ppyr(F_i + \lambda v)$
    (Figure~\ref{fig:translation-of-pseudopyramid-decomposition}).
    The fact that $P$ does not contain the origin
    will guarantee the existence of at least one back facet.
    Since the volume of $P + \mu v$ and $P + \lambda v$ are the same,
    Lemma~\ref{thm:pseudopyramid-decomposition} will guarantee that
    the volume of $\ppyr(P + \mu v)$ is strictly smaller than
    the volume of $\ppyr(P + \lambda v)$,
    and thus by Lemma~\ref{thm:different-pseudopyramid-volumes}
    the functions $L_{P + \mu v}(s)$ and $L_{P + \lambda v}(s)$ are different.

    \begin{figure}[t]
        \tikzset{
            every path/.style = {line join = bevel}
        }
        \centering
        \begin{tikzpicture}
            \draw (-1, 0) -- (3, 0);
            \draw (0, -1) -- (0, 3);
            \filldraw [blue, thick, fill = blue!40]
                (.5, 1) -- (1, 2) -- (2, 2) -- (2, 0.5) -- (0.5, 0.5) -- cycle;
            \filldraw [blue, thick, fill = blue!40]
                (0, 0) -- (0.5, 0.5) -- (2, 0.5) -- cycle;
            \filldraw [blue, thick, fill = yellow!40]
                (0, 0) -- (.5, 1) -- (0.5, 0.5) -- cycle;
            \draw [red, thick] (0.5, 0.5) -- (0.5, 1) node [right] {$F_i + \mu v$};
        \end{tikzpicture}
        \qquad
        \begin{tikzpicture}
            \draw (-1, 0) -- (3, 0);
            \draw (0, -1) -- (0, 3);
            \filldraw [blue, thick, fill = blue!40]
                (1.5, 1.5) -- (2, 2.5) -- (3, 2.5) -- (3, 1) -- (1.5, 1) -- cycle;
            \filldraw [blue, thick, fill = blue!40]
                (0, 0) -- (1.5, 1) -- (3, 1) -- cycle;
            \filldraw [blue, thick, fill = blue!40]
                (0, 0) -- (1.5, 1.5) -- (2, 2.5) -- cycle;
            \filldraw [blue, thick, fill = yellow!40]
                (0, 0) -- (1.5, 1.5) -- (1.5, 1) -- cycle;
            \draw [red, thick] (1.5, 1) -- (1.5, 1.5) node [right] {$F_i + \lambda v$};
        \end{tikzpicture}
        \caption{
            For $\lambda > \mu$ and $v \in P$,
            if $F_i + \mu v$ is a back facet of $P + \mu v$,
            then $F_i + \lambda v$ is also a back facet of $P + \lambda v$.
        }
        \label{fig:translation-of-pseudopyramid-decomposition}
    \end{figure}

    For any $\mu$, we have
    \begin{equation*}
        P + \mu v =
            \bigcap_{i = 1}^n \{x \in \mathbb R^d \mid
                \langle a_i, x \rangle \leq b_i + \mu \langle a_i, v \rangle
            \},
    \end{equation*}
    so that
    \begin{equation*}
        F_i + \mu v = (P + \mu v) \cap
                \{x \in \mathbb R^d \mid
                    \langle a_i, x \rangle = b_i + \mu \langle a_i, v \rangle \}.
    \end{equation*}

    For all $i$,
    we know that
    \begin{equation*}
        \langle a_i, v \rangle \leq b_i,
    \end{equation*}
    because $v$ is contained in $P$,
    by assumption.
    If $F_i + \mu v$ is a back facet,
    we know that
    \begin{equation*}
        b_i + \mu \langle a_i, v \rangle < 0.
    \end{equation*}
    Adding $\mu \langle a_i, v \rangle$ to both sides of the first inequality
    and using the latter gives $\langle a_i, v \rangle < 0$.
    Therefore,
    for $\lambda > \mu \geq 0$,
    \begin{equation*}
        b_i + \lambda \langle a_i, v \rangle < b_i + \mu \langle a_i, v \rangle,
    \end{equation*}
    which shows that if $F_i + \lambda v$ is a back facet,
    then so is $F_i + \mu v$.

    As $P$ does not contain the origin,
    we know at least one of the $b_i$ is negative,
    and thus $P$ has at least one back facet $F_i$;
    therefore,
    applying the previous reasoning with $\mu = 0$ shows that
    all the polytopes $P + \lambda v$, for $\lambda \geq 0$,
    have $F_i + \lambda v$ as a back facet;
    that is,
    all these polytopes have back facets.

    Since $F_i$ is $(d-1)$-dimensional,
    the pseudopyramid $\ppyr(F_i + \mu v)$ is actually a pyramid.
    The height of this pyramid is the distance from the origin to the hyperplane
    \begin{equation*}
        \{ x \in \mathbb R^d \mid
            \langle a_i, x \rangle = b_i + \mu \langle a_i, v \rangle \}.
    \end{equation*}
    Without loss of generality we may assume $a_i$ is unitary,
    so that this distance is
    \begin{equation*}
        -\big( b_i + \mu \langle a_i, v \rangle \big).
    \end{equation*}
    As the bases of $\ppyr(F_i + \mu v)$ and $\ppyr(F_i + \mu v)$ have the same area,
    whenever $\lambda > \mu$
    the volume of $\ppyr(F_i + \mu v)$ will be strictly smaller than
    the volume of $\ppyr(F_i + \lambda v)$.

    As $P + \mu v$ has a back facet,
    by Lemma~\ref{thm:pseudopyramid-decomposition},
    the volume of $\ppyr(P + \lambda v)$
    is strictly larger than the volume of $\ppyr(P + \mu v)$.
    Observe that there might exist some facet $F_j + \lambda v$ of $P + \lambda v$
    such that $F_j + \mu v$ is not a back facet of $P + \mu v$;
    this is not a problem,
    because the back facets that do appear in $P + \mu v$
    suffice to make the volume of $\ppyr(P + \lambda v)$
    larger than $\ppyr(P + \mu v)$.

    Finally,
    since
    \begin{equation*}
        \lim_{s \to \infty} \frac{L_{\ppyr(P + \lambda v)}(s)} {s^d}
            = \vol \ppyr(P + \lambda v),
    \end{equation*}
    using Lemma~\ref{thm:different-pseudopyramid-volumes},
    we conclude that
    the functions $L_{P + \lambda v}(s)$ and $L_{P + \mu v}(s)$ must be different.
\end{proof}

\begin{restatetheorem}{thm:translation-variant}
    Let $P \subseteq \mathbb R^d$ be a real polytope
    which is either full-dimensional
    or has codimension $1$.
    Then there is an integral vector $w \in \mathbb R^d$ such that
    the functions $L_{P + k w}(s)$ are different
    for all integers $k \geq 0$.
\end{restatetheorem}

\begin{proof}
    If $P$ is full-dimensional and does not contain the origin,
    then it contains a nonzero rational vector $v$.
    Let $w$ be any nonzero multiple of $v$ which is an integer vector;
    then Proposition~\ref{thm:full-dimensional-translation-variant}
    shows directly that all the functions $L_{P + k w}(s)$,
    for $k \geq 0$,
    are different.

    If $P$ is full-dimensional, but contains the origin,
    again it will contain a nonzero rational vector $v$.
    Now $w$ not only needs to be a nonzero integer multiple of $v$,
    but also $w$ must be large enough so that $P + w$ does not contain the origin
    (such a $w$ always exist because $P$ is bounded).
    Now Proposition~\ref{thm:full-dimensional-translation-variant}
    only shows that all the functions $L_{P + k w}(s)$ will be different for $k \geq 1$.
    But since $L_{P + kw}(s)$ is nondecreasing only for $k = 0$,
    because that is the only value of $k$ for which $P + kw$ contains the origin,
    we must have $L_{P + wk}(s)$ distinct from $L_P(s)$
    whenever $k \neq 0$;
    this completes the proof in this case.

    And for the last case
    (if $P$ has codimension $1$),
    we will use Lemma~\ref{thm:different-pseudopyramid-volumes} directly.
    As $P$ is not full-dimensional,
    $P$ is contained in a hyperplane $H$ given by
    \begin{equation*}
        H = \{ x \in \mathbb R^d \mid \langle a, x \rangle = b \},
    \end{equation*}
    where $a$ is a unit vector and $b \geq 0$.

    Let $w$ be any integer vector such that $\langle a, w \rangle > 0$.
    We have
    \begin{equation*}
        P + k w \subseteq H + k w = \{
            x \in \mathbb R^d \mid
            \langle a, x \rangle = b + k \langle a, w \rangle
        \}.
    \end{equation*}

    As $P$ is not full-dimensional,
    the pseudopyramid $\ppyr(P + kw)$
    is actually a pyramid,
    whose base is $P + kw$.
    Let $A$ be the $(d-1)$-dimensional area of $P$.
    As $a$ is a unit vector,
    the height of this pyramid
    (which is the distance of $H + kw$ to the origin)
    is $b + \langle a, w \rangle$.
    Therefore,
    the volume of $\ppyr(P + kw)$ is
    \begin{equation*}
        \vol \ppyr(P + kw) = \frac1d A (b + \langle a, w \rangle).
    \end{equation*}
    Since $P$ has codimension $1$,
    its area $A$ is nonzero,
    so for $k \geq 0$ all these volumes are different.
    Thus,
    all functions $L_{P + kw}(s)$ are different in this case, too.
\end{proof}

If we assume the polytope is rational,
we may drop the dimensionality assumption.

\begin{restatetheorem}{thm:rational-translation-variant}
    Let $P \subseteq \mathbb R^d$ be a rational polytope with any dimension.
    Then there is an integral vector $w \subseteq \mathbb R^d$ such that
    the functions $L_{P + k w}(s)$ are distinct
    for all integers $k \geq 0$.
\end{restatetheorem}

\begin{proof}
    If $P$ has codimension $0$ or $1$,
    use Theorem~\ref{thm:translation-variant}.
    Otherwise,
    $P$ will be contained in a rational hyperplane passing through the origin,
    say, $H$.
    Then apply an affine transformation to $P$
    which maps $H$ to $\mathbb R^{d-1} \times \{0\}$
    and use this theorem for dimension $d-1$.
\end{proof}

We end this section by showing that,
if we do not have the rationality hypothesis,
then the dimension hypothesis is necessary.
Let $M \subseteq \mathbb R^d$ be the $(d-2)$-dimensional affine space
defined by
\begin{equation*}
    M = \{(\ln 2, \ln 3)\} \times \mathbb R^{d-2}.
\end{equation*}
That is, $M$ is the set of all points $(x_1, \dots, x_d)$ in $\mathbb R^d$
such that $x_1 = \ln 2$ and $x_2 = \ln 3$.

For any integer translation vector $w = (w_1, \dots, w_d)$
and any real $s > 0$,
we have
\begin{equation*}
    s(M + w) = \big\{\big( s(\ln 2 + w_1), s(\ln 3 + w_2) \big)\big\}
        \times \mathbb R^{d-2},
\end{equation*}
so if $s(M + w)$ contains an integer point $(x_1, \dots, x_d)$,
then $s(\ln 2 + w_1) = x_1$ and $s(\ln 3 + w_2) = x_2$.
Since $s$, $\ln 2 + w_1$ and $\ln 3 + w_2$ are nonzero,
we have $x_1, x_2 \neq 0$, too,
and thus their ratio is
\begin{equation*}
    \frac{x_1}{x_2} = \frac{\ln 2 + w_1}{\ln 3 + w_2},
\end{equation*}
which,
rearranging the terms,
gives
\begin{equation*}
    x_1 (\ln 3 + w_2) = x_2 (\ln 2 + w_1).
\end{equation*}

Raising $e$ to both sides of the equation then gives
\begin{align*}
    (e^{\ln 3 + w_2})^{x_1} &= (e^{\ln 2 + w_1})^{x_2} \\
        3^{x_1} e^{w_2 x_1} &= 2^{x_2} e^{w_1 x_2} \\
    \frac{3^{x_1}}{2^{x_2}} &= e^{w_1 x_2 - w_2 x_1}
\end{align*}

As $e$ is a transcendental number,
we must have $w_1 x_2 - w_2 x_1 = 0$,
which shows $\frac{3^{x_1}}{2^{x_2}} = 1$.
This is only possible if $x_1 = x_2 = 0$,
a contradiction.
Thus, $s(M + w)$ has no integer points.

Therefore, if $P$ is any polytope contained in $M$,
for any integer translation vector $w$ and any real $s > 0$
the polytope $s(P + w)$ will contain no integer points,
and thus $L_{P + w}(s) = 0$ for all $s > 0$ and all integer $w$.
So,
clearly these functions are all the same.
