\subsection{Real Reciprocity theorem}
\label{sec:real-reciprocity}

Since the ``components'' of $L_P(s)$ and of $L_{P^\circ}(s)$
are only $s$, $\{\pm (\num P)s\}$ and $\{\frac{s}{\den P}\}$
and these functions are well-defined for negative $s$,
we are ready to extend $L_P(s)$ and $L_{P^\circ}(s)$ to negative numbers.
We will perform just one more sanity check
before proceeding to prove a reciprocity theorem.

If $p_d(s) s^d + \dots + p_0(s)$ is a quasipolynomial
and $p_d$ is not identically zero,
we define the \emph{degree} of this quasipolynomial to be $d$.

\begin{lemma}
    \label{thm:quasipolynomial-equality}
    Let $p(s)$ and $q(s)$ be quasipolynomials in $s$ of degree $d$
    whose coefficients have period $k$.
    If $p(s) = q(s)$ for all $s$ in $[0, (d+1)k)$,
    then $p = q$.
\end{lemma}

\begin{proof}
    Let $p(s) = p_d(s) s^d + \dots + p_0(s)$
    and $q(s) = q_d(s) s^d + \dots + q_0(s)$,
    where each $p_i$ and $q_i$ is a periodic function of period $k$;
    that is,
    $p_i(s + k) = p_i(s)$ and $q_i(s + k) = q_i(s)$ for all $s$.

    Now,
    let $0 \leq s < k$ be fixed,
    and define for integer $t$
    the functions $\hat p(t) = p(s + kt)$ and $\hat q(t) = q(s + kt)$.
    By hypothesis,
    we have $\hat p(t) = \hat q(t)$ for all $0 \leq t \leq d$.
    But note that
    \begin{align*}
        \hat p(t) &= p(s + kt) \\
            &= p_d(s + kt) (s + kt)^d + \dots + p_0(s + kt) \\
            &= p_d(s) (s + kt)^d + p_{d-1}(s) (s + kt)^{d-1} + \dots + p_0(s),
    \end{align*}
    so $\hat p$ is a polynomial of degree $\leq d$;
    a similar reasoning shows
    $\hat q$ is also a polynomial of degree $\leq d$ with the same form.
    Since these two polynomials agree on $d + 1$ points,
    they must be equal and thus have the same coefficients,
    which shows $p_i(s) = q_i(s)$ for all $i$.

    Finally,
    since $s \in [0, k)$ was arbitrary,
    this means $p_i$ agrees with $q_i$ on $[0, k)$,
    and since these two functions have period $k$,
    this means $p_i = q_i$ for all $i$,
    from which the equality between $p$ and $q$ follows.
\end{proof}

\begin{theorem}
    Let $P$ be any rational polytope
    and $L_P(s)$ and $L_{P^\circ}(s)$ their lattice point enumerators.
    Then if we regard $L_P(s)$ and $L_{P^\circ}(s)$ as quasipolynomials,
    their evaluation at negative integers yield
    \begin{equation*}
        L_P(-s) = (-1)^{\dim P} L_{P^\circ}(s)
    \end{equation*}
    for all real $s$.
\end{theorem}

The proof essentially follows the one by Linke~\cite[p.~1971]{Linke2011}.

\begin{proof}
    We will use the Ehrhart-Macdonald reciprocity theorem for integer dilates.

    First, let $s_0 = \frac a b$ be a rational number,
    with $b > 0$.
    For all positive $s$,
    we have $L_P(s) = L_{\frac 1 b P}(bs)$
    and $L_{P^\circ}(s) = L_{(\frac 1 b P)^\circ}(bs)$,
    so by Lemma~\ref{thm:quasipolynomial-equality}
    these relations hold for all $s$.
    Then
    \begin{align*}
        L_P(-s_0) &= L_{\frac 1 b P}(-a) \\
            &= (-1)^{\dim P} L_{(\frac 1 b P)^\circ}(a) \\
            &= (-1)^{\dim P} L_{(P)^\circ}(s_0),
    \end{align*}
    where in the middle equality we used the Ehrhart-Macdonald reciprocity
    for the integer $a$.

    This shows that the reciprocity law is valid for all rational $s$.
    Now, by theorem~\ref{thm:real-ehrhart-theorem},
    $L_P(s)$ and $L_{P^\circ}(s)$ are quasipolynomials
    whose coefficients are step polynomials over a set of rational numbers;
    so,
    the coefficients (and thus the functions themselves)
    are continuous for every irrational number.
    Therefore,
    the validity of the reciprocity law for all real $s$ follows by continuity.
\end{proof}
