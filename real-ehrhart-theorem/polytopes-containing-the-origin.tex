\subsection{Polytopes containing the origin}
\label{sec:polytopes-containing-the-origin}

If $0 \in P$,
we may try to mimick the procedure we used
to obtain the formula~\eqref{eq:righttriangle-enumerator} for $\righttriangle$.
For example,
let $P$ be the polytope of Section~\ref{sec:same-ehrhart-function}.
We know that
\begin{equation*}
    L_P(t) = \begin{cases}
        \frac{1}{24} t^3 + \frac{1}{4} t^2 + \frac{5}{6} t + 1,
            &\text{$t$ is even;} \\
        \frac{1}{24} t^3 + \frac{1}{4} t^2 + \frac{11}{24} t + \frac{1}{4},
            &\text{$t$ is odd.}
    \end{cases}
\end{equation*}

Let us first find a closed-form expression for $L_P(t)$.
The simplest $2$-periodic step polynomial is $\{\frac t 2\}$;
we may create linear combinations of this step polynomial
with the constant polynomial $p(t) = 1$
to get a closed form expression for the coefficients $c_1$ and $c_0$.
In our case, we may write
\begin{equation*}
    c_1(t) = \frac 5 6 - \frac{9}{12} \left\{ \frac t 2 \right\}
    \qquad\text{and}\qquad
    c_0(t) = 1 - \frac 3 2 \left\{ \frac t 2 \right\}.
\end{equation*}

We may generalize this procedure using Lagrange's interpolation formula.

\begin{lemma}
    \label{thm:step-polynomial-lagrange}
    Any periodic function $f: \mathbb Z \to \mathbb R$
    of period $b$
    can be written as a step polynomial over $\frac 1 b$.
\end{lemma}

\begin{proof}
    If $t$ is an integer,
    all the possible values for $\{\frac t b\}$
    are of the form $\frac k b$
    for $0 \leq k < b$.
    Therefore,
    the function
    \begin{equation*}
        \left\{\frac t b\right\}
            \left( \left\{\frac t b\right\} - \frac 1 b \right) \dots
            \left( \left\{\frac t b\right\} - \frac{b-1}{b} \right)
    \end{equation*}
    is identically zero.
    If we leave out the factor $\{\frac t b\} - \frac a b$ in the expression above,
    we have a function $q_a(t)$
    which is nonzero for $t \equiv a \pmod b$ and $0$ otherwise.
    Each of these $q_a$ is clearly a step function over $\frac 1 b$,
    so the fact that
    every periodic function can be written as a linear combination of these $q_a$
    finishes the proof.
\end{proof}

Since the polytope $P$ satisfies the conditions of Theorem~\ref{thm:if-part},
we may apply it to the closed-form expression we got for $L_P(t)$.
We have
\begin{align*}
    L_P(s) &= L_P(\floor s) \\
        &= \frac{1}{24}\floor s^3 + \frac14 \floor s^2 +
            \left(
                \frac56 - \frac{9}{12} \left\{ \frac{\floor s}{2} \right\}
            \right) \floor s +
            1 - \frac32 \left\{ \frac{\floor s}{2} \right\}.
\end{align*}

The factors $\floor s$ may be expanded using the identity $\floor s = s - \{s\}$.
To deal with the factors $\big\{ \frac{\floor s}{2}\big\}$,
we will use the following lemma.

\begin{lemma}
    \label{thm:fractional-of-floor}
    If $b$ is a positive integer,
    then for all real $s$ we have
    \begin{equation*}
        \left\{ \frac{\floor s}{b} \right\} =
            \left\{ \frac s b \right\} - \frac{ \{s\} }{b}.
    \end{equation*}
\end{lemma}

\begin{proof}
    The identity $x = \floor x + \{x\}$ gives
    \begin{equation*}
        \left\{ \frac{\floor s}{b} \right\} =
            \frac{\floor s}{b} - \floor{\frac{\floor s}{b}}.
    \end{equation*}
    Now if $\floor{\floor s /b} = n$ then $\floor s / b < n + 1$,
    so $\floor s < b(n+1)$, so $s < b(n+1)$, so $s/b < n+1$, so $\floor{s/b} \leq n$.
    The inequality $\floor{s/b} \geq \floor{\floor s /b}$ then implies
    $\floor{s/b} = n$,
    so $\floor{\floor s /b} = \floor{s/b}$.
    Whence,
    \begin{align*}
        \left\{ \frac{\floor s}{b} \right\}
            &= \frac{\floor s}{b} - \floor{\frac s b} \\
            &= \frac{s - \{s\}}{b} - \frac s b + \left\{\frac s b\right\} \\
            &= \left\{\frac s b\right\} - \frac{\{s\}}{b}.
            \qedhere
    \end{align*}
\end{proof}

Now,
if we replace $\big\{ \frac{\floor s}{2} \big\}$ with $\{\frac s 2\} - \frac{\{s\}}{2}$
in the expression we got for $L_P(s)$,
we get
\begin{equation}
    \begin{aligned}
    L_P(s) &= \frac{1}{24}s^3 +
        \left( \frac14 - \frac{\{s\}}{8} \right) s^2 +
        \left( \frac{ \{s\}^2 - \{s\} }{8}
            - \frac34 \left\{\frac s 2\right\}
            + \frac56
        \right) s
        + {}
        \\
        & \quad{} - \frac{\{s\}^3}{24} - \frac{\{s\}^2}{8} - \frac{\{s\}}{12}
            - \frac32 \left\{\frac s 2\right\}
            + \frac34 \{s\} \left\{ \frac s 2 \right\}
            + 1.
    \end{aligned}
    \label{eq:ehrhart-function-cris-polytope}
\end{equation}

In this case,
we had a semi-reflexive polytope;
but,
for example,
when computing $L_\righttriangle(s)$,
we had first to shrink $\righttriangle$ to a smaller polytope,
which was semi-reflexive.
In the general case,
we will also shrink the polytope,
as follows.

If $P$ is the intersection of finitely many half-spaces of the form
\begin{equation*}
    H_i = \{ x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
\end{equation*}
where $a_i$ and $b_i$ have no common factors for any $i$,
define the \emph{numerator} of $P$ to be the least common multiple of the $b_i$.
If we divide $P$ by a multiple $kb_i$ of $b_i$,
this modified half-space will read as
\begin{equation*}
    \frac{1}{kb_i} H = \{x \in \mathbb R^d \mid \langle ka_i, x \rangle \leq 1\}.
\end{equation*}
Therefore,
if $P$ contains the origin and $m$ is its numerator,
then $\frac1m P$ is semi-reflexive.
This observation is the basis for the following theorem.

\begin{proposition}
    Let $P$ be a rational polytope which contains the origin,
    and $m$ and $k$ its numerator and denominator,
    respectively.
    Then $L_P(s)$ is a quasipolynomial in $s$
    whose coefficients are step polynomials over $m$ and $\frac 1 k$.
\end{proposition}

\begin{proof}
    As observed above,
    $\frac1m P$ is semi-refleixive,
    and thus by Theorem~\ref{thm:characterization}
    we have $L_{\frac1m P}(s) = L_{\frac1m P}(\floor s)$.

    The denominator of $\frac1m P$ divides $km$,
    so Ehrhart's Teorem for rational polytopes now says that
    \begin{equation*}
        L_{\frac1m P}(t) = c_d(t) t^d + \dots + c_0(t)
    \end{equation*}
    for periodic functions $c_i(t)$ with period $km$.
    Applying Lemma~\ref{thm:step-polynomial-lagrange}
    allows us to write each $c_i(t)$ as a step polynomial over $\frac{1}{km}$.

    Now, $\floor s$ is an integer,
    so we may plug $\floor s$ into these step polynomials
    and obtain an expression for $L_{\frac1m P}(\floor s)$
    composed of sums and products of constants and the terms
    $\floor s$ and $\big\{ \frac{\floor s}{km} \big\}$.
    Applying Lemma~\ref{thm:fractional-of-floor} now
    allows us to replace each $\big\{ \frac{\floor s}{km} \big\}$
    by terms of the form $\{ \frac{s}{km} \}$ and $\frac{ \{s\} }{km}$.
    Using the identity $\floor s = s - \{s\}$ now
    allows us to conclude that
    $L_{\frac1m P}(s)$ can be written as sums and products
    of constants and the terms $s$, $\{s\}$ and $\{ \frac{s}{km} \}$.

    Now,
    using the identity $L_P(s) = L_{\frac1m P}(ms)$,
    we get an expression for $L_P(s)$
    with the terms $s$, $\{ms\}$ and $\{\frac s k\}$.
    This is precisely a quasipolynomial
    whose coefficients are step polynomials over $m$ and $\frac 1 k$.
\end{proof}

To generalize this result for all rational polytopes $P$,
even in one dimension,
we will need to admit some more numbers
over which the coefficients of $L_P(s)$ are step polynomials.
For example,
if $P = [1, 2]$,
\begin{align*}
    L_P(s) &= \floor{2s} - \ceil{s} + 1 \\
        &= s - \{2s\} - \{-s\} + 1,
\end{align*}
whose constant coefficient is a step polynomial over $2$ and $-1$.
In general,
we will allow the coefficients of $L_P(s)$
to be step polynomials over $m$, $-m$ and $\frac 1 k$,
where $m$ and $k$ are the numerator and the denominator of the polytope,
respectively.
(We know we need to allow negative numbers because,
for example,
the function $L_P(s)$ above is right-discontinuous at $s = 1$,
and the functions $\{\alpha s\}$ cannot produce this kind of discontinuity
if $\alpha > 0$.)

We will use the ``front body/back shell decomposition'',
developed in the next section.
