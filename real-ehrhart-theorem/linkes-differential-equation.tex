\section{Linke's differential equation}
\label{sec:linkes-differential-equation}

As we saw,
for example,
with Proposition~\ref{thm:translation-variant},
looking at $L_P(s)$ for real $s$ reveals some new behavior of this function.
One of the most beautiful results of this nature
is Linke's differential equation,
which are satisfied by the coefficients of the quasipolynomial $L_P(s)$.

\begin{restatetheorem}[Linke, 2011\footnotemark]{thm:linkes-differential-equation}
    Let $P$ be a rational polytope, and write
    \begin{equation*}
        L_P(s) = p_d(s) s^d + \dots + p_1(s) s + p_0(s).
    \end{equation*}
    Then for every $s$ at which all the $p_i$ are left (resp. right) continuous,
    all the $p_i$ will be left (resp. right) differentiable,
    and
    \begin{equation*}
        p'_i(s) = -(i+1) p_{i+1}(s).
    \end{equation*}
\end{restatetheorem}
\footnotetext{
    \label{fn:linkes-proof-error}
    This theorem first appeared in Linke's paper~\cite[p.~1973]{Linke2011}.
    It claims that the differential equation is true
    whenever $p_i$ is differentiable,
    but the proof contained in that paper does not address the cases
    where some $p_i$ are continuous while the remaining are not.
    The statement is corrected in Linke's PhD thesis~\cite[p.~70]{LinkePhD}.
    (The theorem is stated here in a slightly different form;
    see discussion after the proof.)
}

\begin{proof}
    Let $m$ be the numerator of $P$.
    A consequence of Theorem~\ref{thm:real-ehrhart-theorem} is that
    if $s \in (\frac a m, \frac{a+1}{m})$
    for some integer $a$,
    then all the coefficients $p_i$ are continuous at $s$,
    and so there is a simple proof of the theorem for these $s$.
    The function $L_P(s)$ will be continuous in such an interval.
    As the image of $L_P(s)$ contain only integer numbers,
    for such a function to be continuous it must, in fact,
    be constant;
    that is,
    there is a number $c$ such that
    \begin{equation*}
        p_d(s) s^d + \dots + p_1(s) r + p_0(s) = c
    \end{equation*}
    for all $s \in (\frac a m, \frac{a+1}{m})$.
    Now each $p_i$ is a polynomial in this interval,
    so we may differentiate both sides of the above equation to get
    \begin{equation*}
        p'_d(s) s^d + \Big(d p_d(s) + p'_{d-1}(s)\Big) s^{d-1}
            + \dots + \Big(p_1(s) + p'_0(s)\Big) = 0.
    \end{equation*}

    Following the proof of Theorem~\ref{thm:quasipolynomial-equality},
    let $k = \den P$.
    Then each $p_i$ has period $k$,
    so if we apply the above equation to all $s$ of the form $s_0 + jk$
    for some fixed $s_0 \in (\frac a m, \frac{a+1}{m})$
    and all $j \in \mathbb Z$,
    the resulting polynomial in $j$ will have infinitely many zeros,
    and thus its coefficients must all be zero;
    therefore,
    \begin{equation*}
        p'_i(s_0) = -(i+1) p_{i+1}(s_0).
    \end{equation*}

    For all other cases,
    we will need the continuity hypothesis.
    Suppose all the $p_i$ are left-continuous at $s_0$
    (for right-continuity, the argument is the same);
    then the previous case guarantees that,
    for every sufficiently small $\varepsilon > 0$
    (and all $i$),
    we have
    \begin{equation*}
        p'_i(s_0 - \varepsilon) = -(i+1) p_{i+1}(s_0 - \varepsilon).
    \end{equation*}
    Since $p_i$ is left-continuous at $s$ (by hypothesis)
    and $\lim_{s \to s_0^-} p'_i(s)$ exists
    (by the above equation and the continuity of $p_{i+1}$),
    we know $p_i$ is left-differentiable (and left-continuous) at $s_0$,
    and thus (by continuity) the differential equation still holds.
\end{proof}

As noted in the beginning of the proof,
if $m$ is the numerator of the polytope $P$,
then (as a consequence of Theorem~\ref{thm:real-ehrhart-theorem})
the continuity condition is satisfied by every point
which is not of the form $\frac a m$ for some integer $a$;
therefore,
the continuity condition demanded by the theorem
might fail only for a countable, discrete set of points.

The theorem is slightly different from Linke's~\cite{LinkePhD};
specifically,
$L_P(s)$ is demanded to be (one-sided) continuous
in every point of the form $s_0 + jq$ for all integers $j \geq 0$,
where $q$ is the ``rational denominator of $P$'',
the smallest positive rational number such that $qP$ is an integer polytope.
Applying Theorem~\ref{thm:real-ehrhart-theorem} to $qP$
and using the fact that $L_{qP}(s) = L_P(qs)$
allows us to conclude that the functions $p_i$ have rational period $q$.
If the condition of Theorem~\ref{thm:linkes-differential-equation}
is satisfied by some number $s_0$,
by periodicity $L_P(s)$ will be continuous at all points of the form $s_0 + jq$.
That is,
the condition of Theorem~\ref{thm:linkes-differential-equation}
implies Linke's condition;
the opposite implication can be shown as follows.

Denote by $p_i(s_0^-)$ the left-sided limit $\lim_{s \to s_0^-} p_i(s)$.
We may rewrite the fact that $L_P(s)$ is left-continuous
for every point of the form $s_0 + jq$ as
\begin{equation*}
    p_d(s_0) (s_0 + jq)^d + \dots + p_0(s_0) =
        p_d(s_0^-) (s_0 + jq)^d + \dots + p_0(s_0^-).
\end{equation*}
(Periodicity allowed us to replace $p_i(s_0 + jq)$ with $p_i(s_0)$
in the above formula.)
Since this is a polynomial equality for integer $j \geq 0$,
their coefficients must be the same; that is, $p_i(s_0) = p_i(s_0^-)$,
which is precisely the continuity condition
of Theorem~\ref{thm:linkes-differential-equation}.

Finally,
we remark that merely assuming that $L_P(s)$ is continous at some $s$
is not enough,
because this does not guarantee the $p_i$ will be continuous;
for example,
for the $3$-dimensional polytope $P$ of Section~\ref{sec:same-ehrhart-function}
(whose Ehrhart function was computed in~\eqref{eq:ehrhart-function-cris-polytope}),
$L_P(s)$ is continuous at $1$
(in fact, $L_P(s) = 1$ for $s \in [0, 2)$),
but $p_2$, $p_1$ and $p_0$ are discontinuous at $1$.
