\subsection{Real Ehrhart theorem}
\label{sec:real-ehrhart-theorem}

Before tackling the theorem for all rational polytopes,
we will show the following results,
which will allow us to perform some useful reductions.

\begin{lemma}
    \label{thm:unimodular-image}
    Let $P$ be a rational polytope and $A$ a unimodular matrix.
    Then $P$ and $AP$ have the same numerator and denominator.
\end{lemma}

\begin{proof}
    Let $H = \{x \mid \langle u, x \rangle \leq \alpha\}$ be a half-space,
    where $u$ and $\alpha$ are integers without a common factor.
    Then
    \begin{align*}
        AH &= \{Ax \mid \langle u, x \rangle \leq \alpha\} \\
            &= \{x \mid \langle u, A^{-1} x \rangle \leq \alpha\} \\
            &= \{x \mid \langle A^{-t}u, x \rangle \leq \alpha\}.
    \end{align*}
    As $A$ is unimodular, $A^{-t}$ has only integer entries,
    so $A^{-t}u$ is an integral vector.
    If $A^{-t}u$ and $\alpha$ had a common factor $c$,
    this means $\frac 1 c A^{-t} u$ is an integral vector,
    and thus $\frac 1 c u = A^t (\frac 1 c A^{-t} u)$ is also an integral vector,
    so $u$ and $\alpha$ also shared this common factor.
    Therefore, $A^{-t}u$ and $\alpha$ have no common factors.
    This shows that
    the right-hand side of each inequality which bounds $P$
    is unchanged when we apply the linear operator $P$,
    even after dividing both sides by their common factors;
    so the least common multiple of all these $\alpha$
    (which is the numerator of the polytope)
    is left unchanged.

    For the denominator,
    let $v$ be a vertex of $P$
    and define $k$ and $k'$ to be
    the least common multiple of the denominators of the coordinates
    of the vectors $v$ and $Av$, respectively.
    Then $kv$ is an integer
    and $k$ divides every integer $m$ such that $mv$ is an integer,
    and same goes for $k'$ and $Av$.
    As $A$ has integer entries,
    $Akv = kAv$ must be an integer vector,
    and thus $k'$ divides $k$.
    Since $A^{-1}$ also has only integer entries,
    the vector $A^{-1} k'Av = k'v$ also is an integer vector,
    and thus $k$ divides $k'$.
    Thus, $k = k'$.
    This shows $A$ preserves least common multiples in the vertex level,
    and thus it must preserve the denominator of the whole polytope.
\end{proof}

We will denote the numerator and the denominator of a rational polytope $P$
by $\num P$ and $\den P$,
respectively.
For the denominator we have the following.

\begin{lemma}
    \label{thm:den-of-multiples}
    Let $P$ be a rational polytope and $b$ a positive integer.
    Then $\den \frac 1 b P$ divides $b \den P$.
\end{lemma}

\begin{proof}
    The denominator of $P$ is the least integer $\den P$
    such that $(\den P) P$ is an integral polytope,
    so $\den P$ divides any integer $k$ such that $k P$ is an integral polytope.
    As $(b \den P) \frac 1 b P$ is an integral polytope,
    $\den \frac 1 b P$ divides $b \den P$.
\end{proof}

\begin{restatetheorem}{thm:real-ehrhart-theorem}
    Let $P$ be a rational polytope
    and $m$ and $k$ multiples of its numerator and denominator,
    respectively.
    Then both $L_P(s)$ and $L_{P^\circ}(s)$ are quasipolynomials
    whose coefficients are step polynomials over $m$, $-m$ and $\frac 1 k$.
\end{restatetheorem}

\begin{proof}
    Let $P \subseteq \mathbb R^d$;
    we will show the theorem by induction on $d$.
    The interpretation for $d = 0$ is that a $0$-dimensional polytope
    is a single point,
    so $L_P(s) = L_{P^\circ}(s) = 1$,
    which satisfies our requirements.

    Assume now $d > 0$.
    First we will reduce to the case $m = 1$.

    Consider $P' = \frac 1 m P$.
    Since $m$ is a positive multiple of $\num P$,
    we have  $\num P' = 1$,
    and by Lemma~\ref{thm:den-of-multiples},
    $\den P'$ divides $m \den P$,
    so (as $\den P$ divides $k$) $\den P'$ divides $mk$.
    Therefore,
    if we let $m' = 1$ and $k' = km$,
    we may apply the theorem for $P'$ with $m'$ and $k'$
    to conclude both $L_{P'}(s)$ and $L_{(P')^\circ}(s)$ contain expressions of the form
    $\{\pm m's\}$ and $\{\frac{s}{k'}\}$.
    Since $L_P(s) = L_{P'}(ms)$
    (and similarly for $P^\circ$),
    the expressions $\{\pm m's\}$ become $\{\pm ms\}$
    and the expressions $\{\frac{s}{k'}\}$ become $\{\frac s k \}$.

    Therefore, we have solved the problem when $m \neq 1$,
    so henceforth assume $m = 1$.
    As $\num P$ must divide $m$,
    we must also have $\num P = 1$.
    We will now deal with the case when $P$ is not full-dimensional.

    In this case, $P \subseteq H$,
    where $H$ is the hyperplane $\{x \mid \langle a, x \rangle = b\}$
    for some integral $a$ and integer $b$.
    We know $b$ is either $0$ or $1$,
    as we are assuming $\num P = 1$
    (the case $b = -1$ may be reduced to the case $b = 1$
    by replacing $a$ with $-a$).

    If $b = 0$,
    $H$ is a vector subspace,
    and so there is a unimodular matrix $A$ such that
    $AH = \mathbb R^{d-1} \times \{0\}$.%
    \footnote{
        Proof: $H$, as a rational vector subspace,
        has dimension $d-1$,
        so the set $H \cap \mathbb Z^d$
        spans $H$.
        Now let $a_1, \dots, a_{d-1}$ be a lattice basis for $H \cap \mathbb Z^d$,
        and pick $a_d$ to be any vector of the canonical basis
        which is not in $H$;
        then the linear transform which maps the basis $\{a_1, \dots, a_n\}$
        to the canonical basis satisfies our requirements.
    }
    Then $AP$ may be regarded a polytope in $\mathbb R^{d-1}$
    with the same numerator and denominator as $P$
    which satisfies $L_{AP}(s) = L_P(s)$ and $L_{(AP)^\circ}(s) = L_{P^\circ}(s)$,
    by Lemma~\ref{thm:unimodular-image};
    so we may just apply induction for this case.

    If $b = 1$,
    $L_P(s)$ will be zero whenever $s$ is not an integer,
    because the hyperplane $sH$ will not contain any integer in this case.
    Let $p(s) = (1 - \{s\} - \{-s\})$;
    then $L_P(s) = p(s) L_P(\floor s)$
    and $L_{P^\circ}(s) = p(s) L_{P^\circ}(\floor s)$
    (because if $s$ is an integer $\floor s = s$
    and if $s$ is not an integer $p(s) = 0$).
    Now,
    by the Ehrhart Theorem for rational polytopes and integer $t$
    we know $L_P(t)$ and $L_{P^\circ}(t)$ are quasipolynomials with period $\den P$,
    so we can pretend the period is actually $k$
    (which is a multiple of $\den P$)
    and use Theorem~\ref{thm:step-polynomial-lagrange}
    to write their coefficients as step polynomials in $t$ over $\frac 1 k$.
    Then using Lemma~\ref{thm:fractional-of-floor}
    we conclude $L_P(\floor s)$ and $L_{P^\circ}(\floor s)$ are quasipolinomials
    whose coefficients are step polynomials over $m = 1$ and $\frac 1 k$,
    so as $L_P(s) = p(s) L_P(\floor s)$
    we have shown the theorem in this case.

    Therefore,
    we have solved the case when $P$ is not full-dimensional,
    so assume now $m = 1$ and $P$ is full-dimensional.

    Let $\mathcal F'$ and $\mathcal F''$ be the partition of the set of all faces of $P$
    given by Proposition~\ref{thm:face-set-partition}.
    We have the following:
    \begin{align*}
        L_{P^\rightsemicirc}(s) &= \sum_{F \in \mathcal F'} L_{F^\circ}(s) \\
        L_P(s) &= L_{P^\rightsemicirc}(s) + \sum_{F \in \mathcal F''} L_{F^\circ}(s) \\
        L_{P^\circ}(s) &=
            L_{P^\rightsemicirc}(s) -
            \sum_{\substack{F \in \mathcal F' \\ F \neq P}} L_{F^\circ}(s)
    \end{align*}

    For a given face $F$ of $P$,
    the Ehrhart's and Reciprocity Theorems for rational polytopes
    allows us to write,
    for integer $t$,
    the enumerator $L_{F^\circ}(t)$ as a quasipolynomial in $t$;
    and applying Theorem~\ref{thm:step-polynomial-lagrange} again
    the coefficients of $L_{F^\circ}(t)$ are step polynomials over $\frac 1 k$.
    Therefore $L_{P^\rightsemicirc}(t)$ is also one such quasipolynomial.
    Since $\num P = 1$,
    we satisfy the hypothesis of Proposition~\ref{thm:floor-on-front-body},
    and thus $L_{P^\rightsemicirc}(s) = L_{P^\rightsemicirc}(\floor s)$.
    Now using Lemma~\ref{thm:fractional-of-floor}
    we conclude $L_{P^\rightsemicirc}(s)$ is a quasipolynomial
    whose coefficients are step polynomials over $m = 1$ and $\frac 1 k$.

    Finally,
    all proper faces $F$ of $P$ are not full-dimensional,
    so by induction $L_{F^\circ}(s)$ also are quasipolynomials
    whose coefficients are step polynomials over $\pm m$ and $\frac 1 k$.
    As $L_P(s)$ and $L_{P^\circ}(s)$
    can both be obtained from $L_{P^\rightsemicirc}(s)$
    by adding or subtracting some appropriate $L_{F^\circ}(s)$,
    these two enumerators themselves are of this form.
\end{proof}

We note this provides some information on the rate of growth of $L_P(s)$.
For any convex set $P \subset \mathbb R^d$,
we know that
\begin{equation*}
    \lim_{s \to \infty} \frac{1}{s^d} L_P(s) = \vol P;
\end{equation*}
therefore, $L_P(s) = (\vol P) t^d + o(t^d)$.
If $P$ is a rational polytope,
the theorem allows us to improve our estimates to
\begin{equation*}
    L_P(s) = (\vol P) t^d + O(t^{d-1}).
\end{equation*}
This also provides a weak counterpart
to Proposition~\ref{thm:translation-variant};
as $\vol P = \vol(P + v)$,
if $P$ and $v$ are rational then $L_P(s) - L_{P + v}(s) = O(t^{d-1})$,
so that even though these two functions are different
they are not ``too different''.
