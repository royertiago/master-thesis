\subsection{Front body/back shell decomposition}
\label{sec:front-body-back-shell}

\begin{figure}[t]
    \begin{adjustbox}{center}
        \begin{tikzpicture}
            \tikzset{
                every scope/.style = {
                    execute at begin scope = {
                        % Draw axis in every scope.
                        \draw (0, 1) -- (4, 1);
                        \draw (1, 0) -- (1, 4);

                        % Redefine coordinates in every scope
                        % (TikZ does not apply transformations to named coordinates;
                        % this trick redefines the coordinates in every scope,
                        % so that they are in the correct, shifted positions.)
                        \coordinate (A) at (2, 2);
                        \coordinate (B) at (3, 3);
                        \coordinate (C) at (3.5, 3);
                        \coordinate (D) at (3.5, 1.5);
                        \coordinate (E) at (2.5, 1.5);
                        \coordinate (O) at (1, 1); % Origin

                        % Draw a dashed silhouette of the polytope
                        \draw [dashed] (A) -- (B) -- (C) -- (D) -- (E) -- cycle;
                    }
                }
            }

            % Each scope represents a 4cm x 4cm square
            % which contains a part of the polytope,
            % together with a dashed silhouette of the polytope and x and y axis.
            % From the point of view of the scope,
            % the "bounding square" runs from the point (-1, -1) to (3, 3).
            % By shifting every coordinate by (1, 1),
            % the "logical origin" is shifted to the "physical point" (-1, -1),
            % so that the point (0, 0) of each scope
            % is the lower left corner of the bounding square.

            % The polytope itself.
            \begin{scope}[xshift = -4.5cm]
                % The right edge of this square pass through (-0.5, 0).
                \filldraw [blue, thick, fill = blue!40]
                    (A) -- (B) -- (C) -- (D) -- (E) -- cycle;
            \end{scope}

            % Interior of the edges
            \foreach \beginvertex/\endvertex [count=\index from=0]
                in { (A)/(B), (B)/(C), (C)/(D), (D)/(E), (E)/(A)} {
                \begin{scope}[
                        yshift = 2.25cm,
                        xshift = \index * 2.5cm,
                        scale = 0.5
                    ]
                    \draw [blue, thick] \beginvertex -- \endvertex;
                    \draw [blue, thick, fill=white]
                        \beginvertex circle [radius = 4pt]
                        \endvertex circle [radius = 4pt];
                \end{scope}
            }

            % Interior of the polytope
            \begin{scope}[yshift = -0.25cm, scale = 0.5]
                % Draw the interior on the background layer,
                % behind the dashed silhouette of the polytope.
                \begin{pgfonlayer}{background}
                    \fill [blue!40]
                        (A) -- (B) -- (C) -- (D) -- (E) -- cycle;
                \end{pgfonlayer}
            \end{scope}

            % Vertices
            \foreach \vertex [count=\index] in {(B), (C), (D), (E), (A)} {
                \begin{scope}[
                        yshift = -0.25cm,
                        xshift = \index * 2.5cm, % Note \index starts from 1 here
                        scale = 0.5
                    ]
                    \fill \vertex circle [radius = 4pt];
                \end{scope}
            }

            \draw[thick, decoration={brace, mirror, amplitude=.2cm}, decorate]
                (0, -0.5) -- (7, -0.5);
            \draw[thick, decoration={brace, mirror, amplitude=.2cm}, decorate]
                (7.5, -0.5) -- (14.5, -0.5);

            % Front body. The x center is at 3.5, aligned with the brace.
            \begin{scope}[xshift = 1.5cm, yshift = -4.7cm]
                \begin{pgfonlayer}{background}
                    \fill [blue!40]
                        (A) -- (B) -- (C) -- (D) -- (E) -- cycle;
                \end{pgfonlayer}
                \draw [blue, thick]
                    (A) -- (B) -- (C) -- (D);
                \draw [blue, thick, fill=white]
                    (A) circle [radius = 2pt]
                    (D) circle [radius = 2pt];
            \end{scope}

            % Back shell. The x center is at 10.5, aligned with the second brace.
            \begin{scope}[xshift = 8cm, yshift = -4.7cm]
                \draw [blue, thick]
                    (D) -- (E) -- (A);
                \filldraw [blue, thick]
                    (D) circle [radius = 2pt]
                    (A) circle [radius = 2pt];
            \end{scope}

            % Mathematical symbols.
            \node at (-0.25, 2) {$=$}; % Aligned with the y-center of the polytope
            \node at (-0.25, -2.7) {$=$}; % Aligned with the y-center of the front body
            \node at (7, -2.7) {$\bigcup$};
            \foreach \x in {2.25, 4.75, ..., 13} {
                \node at (\x, 3.25) {$\cup$};
                \node at (\x, 0.75) {$\cup$};
            }

            % Hackery to get subcaptions in specific places of the image.
            \node at (-2.5, -0.25) { % Below polytope
                \begin{minipage}{2ex}
                    \subcaption{}
                    \label{fig:face-interiors-decomposition}
                \end{minipage}
            };
            \node at (3.5, -4.7) { % Below front body
                \begin{minipage}{2ex}
                    \subcaption{}
                    \label{fig:front-body}
                \end{minipage}
            };
            \node at (10.5, -4.7) { % Below back shell
                \begin{minipage}{2ex}
                    \subcaption{}
                    \label{fig:back-shell-as-face-unions}
                \end{minipage}
            };
        \end{tikzpicture}
    \end{adjustbox}

    \caption[
        Front body/back shell, and interiors of faces decomposition.
    ]{
        Decomposition of the polytope as the disjoint union of the interior of its faces
        (Lemma~\ref{thm:interiors-of-faces-partition}),
        and as the disjoint union of its front body and back shell
        (Proposition~\ref{thm:front-body-back-shell-decomposition}).

        (\subref{fig:face-interiors-decomposition}):
        Interiors of the faces of the polytope.

        (\subref{fig:front-body}):
        The front body of the polytope;
        in this case,
        it is the union of the interiors of first three edges,
        the interior of the polytope,
        and the first two vertices.

        (\subref{fig:back-shell-as-face-unions}):
        The back shell of the polytope;
        in this case,
        it is the union of the last two edges
        and the last three vertices.
    }
\end{figure}

Let $P$ be any polytope,
and write $P = \bigcap_{i = 1}^n H_i^\leq$,
where $H_i^\leq$ is the half-space
\begin{equation*}
    H_i^\leq \{x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i\}.
\end{equation*}
Define $H_i^<$ analogously by
\begin{equation*}
    H_i^< \{x \in \mathbb R^d \mid \langle a_i, x \rangle < b_i\}.
\end{equation*}

In the proof of Theorem~\ref{thm:if-part},
the fact that $[n \leq b_i \floor s] = [n \leq b_i s]$
when $b_i$ is $0$ or $1$
allowed us to show $L_P(s) = L_P(\floor s)$.
For general polytopes,
we may have to deal with negative $b_i$ as well,
and that relation is false in this case.
However,
we have an analogous relation if we replace the $\leq$ sign with $<$:
$[n < b_i \floor s] = [n < b_i s]$ if $b_i = -1$.
Therfore,
we will call by \emph{front body} of the polytope $P$,
denoted by $P^\rightsemicirc$,
the convex set
\begin{equation}
    P^\rightsemicirc =
        \bigcap_{\substack{1 \leq i \leq n \\ b_i \geq 0}} H_i^\leq
        \cap
        \bigcap_{\substack{1 \leq i \leq n \\ b_i < 0}} H_i^<.
        \label{eq:front-body-def}
\end{equation}
That is,
whenever $b_i < 0$,
we replace the inequality $\langle a_i, x \rangle \leq b_i$
with the strict inequality $\langle a_i, x \rangle < b_i$
in the half-space intersection representation of the polytope
(Figure~\ref{fig:front-body}).

Recall that the back shell of the polytope
is the set of nonzero points $x \in P$ which are ``visible from the origin''
(that is,
the set of all $x \in P$ such that
$\lambda x \notin P$ for all $0 \leq \lambda < 1$).
We call it the ``front body/back shell decomposition''
due to the followith theorem.

\begin{proposition}
    \label{thm:front-body-back-shell-decomposition}
    A polytope is the disjoint union of its front body and its back shell.
\end{proposition}

\begin{proof}
    Let $x \in P$ be in the back shell.
    We know $x$ satisfies every linear restriction of the form
    \begin{equation*}
        \langle a_i, x \rangle \leq b_i,
    \end{equation*}
    which bounds the polytope.
    In the case where $b_i \geq 0$,
    we know that for all $0 \leq \lambda \leq 1$
    the point $\lambda x$ will also satisfy these restrictions.
    But if $b_i \leq 0$,
    then for small enough $\lambda$ the point $\lambda x$ will violate
    at least one of these inequalities.
    If $x$ satisifes all these restrictions with strict inequality
    for all $b_i < 0$
    (that is, $\langle a_i, x \rangle < b_i$ whenever $b_i < 0$),
    then for some $\lambda < 1$ sufficiently close to $1$
    the point $\lambda x$ will also satisfy all these restrictions.
    That would contradict the fact that $x$ can be seen from the origin;
    therefore,
    for at least one $i$ with $b_i < 0$,
    we must have
    \begin{equation*}
        \langle a_i, x \rangle = b_i
    \end{equation*}
    (that is, $x$ satisfy the linear restriction with equality).
    In the front body,
    this linear restriction is replaced by strict inequality,
    and thus now $x$ violates it,
    so $x$ is not in the front body.

    Now,
    suppose $x \in P$ is not in the back shell;
    this means $x$ cannot be seen from the origin,
    and thus for some $\lambda < 1$ we have $\lambda x \in P$.
    This means $\lambda x$ satisfies
    \begin{equation*}
        \langle a_i, \lambda x \rangle \leq b_i
    \end{equation*}
    for all $i$,
    and so if $b_i < 0$ we have
    \begin{equation*}
        \langle a_i, x \rangle \leq \frac{b_i}{\lambda} < b_i,
    \end{equation*}
    which shows $x$ is in the front body.
\end{proof}

The lattice point enumerator $L_{P^\rightsemicirc}(s)$
of the front body of a polytope is defined in the same way as for other sets.
The next proposition
asserts the front body of a polytope has the computational property we aimed for.

\begin{proposition}
    \label{thm:floor-on-front-body}
    Let $P$ be a polytope and $P^\rightsemicirc$ be its front body,
    as in~\eqref{eq:front-body-def}.
    If all $b_i$ are either $0$, $1$ or $-1$
    and all $a_i$ are integral,
    then for all $s \geq 0$ we have
    \begin{equation*}
        L_{P^\rightsemicirc}(s) = L_{P^\rightsemicirc}(\floor s).
    \end{equation*}
\end{proposition}

\begin{proof}
    We saw in the proof of Theorem~\ref{thm:if-part}
    that,
    whenever $b_i = 0$ or $b_i = 1$,
    we have $\big[\langle a_i, x \rangle \leq b_i s \big]
     = \big[\langle a_i, x \rangle \leq \floor s b_i\big]$.
    If $b_i = -1$,
    for every integral point $x$,
    using $[n < s] = [n < \ceil s]$ we have
    \begin{align*}
        \big[ \langle a_i, x \rangle < b_i s \big] &=
                \big[ \langle a_i, x \rangle < -s \big] \\
             &= \big[ \langle a_i, x \rangle < \ceil{-s} \big] \\
             &= \big[ \langle a_i, x \rangle < - \floor s \big] \\
             &= \big[ \langle a_i, x \rangle < b_i \floor s \big].
    \end{align*}

    Therefore,
    \begin{align*}
        L_{P^\rightsemicirc}(s)
            &= \sum_{x \in \mathbb Z^d} [x \in sP^\rightsemicirc] \\
            &= \sum_{x \in \mathbb Z^d}
                \left(
                    \prod_{\substack{1 \leq i \leq n \\ b_i \geq 0}}
                        \big[\langle a_i, x \rangle \leq b_i s \big]
                \right)
                \left(
                    \prod_{\substack{1 \leq i \leq n \\ b_i < 0}}
                        \big[\langle a_i, x \rangle < b_i s \big]
                \right)
                \\
            &= \sum_{x \in \mathbb Z^d}
                \left(
                    \prod_{\substack{1 \leq i \leq n \\ b_i \geq 0}}
                        \big[\langle a_i, x \rangle \leq b_i \floor s \big]
                \right)
                \left(
                    \prod_{\substack{1 \leq i \leq n \\ b_i < 0}}
                        \big[\langle a_i, x \rangle < b_i \floor s \big]
                \right)
                \\
            &= \sum_{x \in \mathbb Z^d} [x \in \floor s P^\rightsemicirc] \\
            &= L_{P^\rightsemicirc}(\floor s).
            \qedhere
    \end{align*}
\end{proof}

We need one last property of the front body,
which is a refinement of the decomposition of
Proposition~\ref{thm:front-body-back-shell-decomposition}.
We will use the following lemma.

\begin{lemma}
    \label{thm:interiors-of-faces-partition}
    Let $\mathcal F$ be the collection of all faces of a polytope $P$
    (including $P$ itself).
    Then
    \begin{equation*}
        P = \bigcup_{F \in \mathcal F} F^\circ,
    \end{equation*}
    and this union is disjoint.
\end{lemma}

\begin{proof}
    Without loss of generality,
    we may assume $P$ to be full-dimensional.
    We will need to use the fact that the topological boundary of $P$
    is the union of all facets of $P$,
    and that any proper face of $P$
    is the intersection of some collection of facets of $P$
    (see \cite[p.~27]{GrunbaumBook} for a proof).

    We will use induction in the dimension of the polytope.
    The result is clearly true for $0$-dimensional polytopes
    (single points).

    If $x \in P$ is not in the interior of $P$,
    then it must be in the boundary of $P$,
    so $x \in F$ for some facet $F$ of $P$.
    By induction in the dimension of the polytope,
    $x \in G^\circ$ for some face $G$ of $F$,
    which is also a face of $P$.
    This shows that $P$ is the union of the interiors of its faces.

    Now we must show that the union is disjoint.
    If $x \in P$ is in the interior of $P$,
    then it cannot be in its topological boundary,
    so it will not be contained in any proper face of $P$
    --- thus $P^\circ$ is disjoint from $F^\circ$ for all faces $F \neq P$.

    So, suppose $x \in P$ is contained in the interiors of the faces $F$ and $G$;
    then $x$ is also in the interior of $F \cap G$.
    Since $F$ and $G$ are intersection of facets of $P$,
    so is $F \cap G$,
    and thus $F \cap G$ is a face of $F$.
    By induction,
    $F$ is the disjoint union of the interiors of its faces.
    As $x \in (F \cap G)^\circ$ and $x \in F^\circ$,
    we must have $F = F \cap G$.
    The same reasoning applied to $G$ shows
    $G = F \cap G = F$.
    Thus, the union is disjoint.
\end{proof}

If a point in the back shell is in the relative interior of a face of $P$,
then the whole face can be seen from the origin.
Since every point of $P$ is in the interior of some face,
this means the back shell of the polytope
is the union of interiors of certain faces of $P$.
As a consequence,
the remaining faces must form the front body of $P$.
In other words, we have the following theorem.

\begin{proposition}
    \label{thm:face-set-partition}
    For every polytope $P$ there is a partition $\mathcal F' \cup \mathcal F''$
    of the set of its faces such that
    $\bigcup_{F \in \mathcal F'} F^\circ$ is the front body $P^\rightsemicirc$ of $P$
    and $\bigcup_{F \in \mathcal F''} F^\circ$ is the back shell of $P$.
\end{proposition}
