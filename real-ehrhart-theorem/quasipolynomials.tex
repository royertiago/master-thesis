\section{Step polynomials and real quasipolynomials}
\label{sec:quasipolynomials}

Theorem~\ref{thm:rational-ehrhart} says that
whenever $P$ is a rational polytope,
the integer function $L_P(t)$ will be a quasipolynomial.
The expressions we got for $L_\righttriangle(s)$ and $L_{[0, 2] \times [0, 1]}(s)$
in Section~\ref{sec:real-dilations}
can be rewritten to exhibit quasipolynomial behavior,
using the identity $\floor x = x - \{x\}$:
\begin{align*}
    L_{[0, 2] \times [0, 1]}(s) &= (\floor{2s} + 1)(\floor s + 1) \\
        &= 2s^2 + \big(3 - 2\{s\} - \{2s\}\big)s + \{s\}\{2s\} - \{s\} - \{2s\} + 1. \\
    L_\righttriangle(s) &= \frac 1 2 \floor{2s}^2 + \frac 3 2 \floor{2s} + 1 \\
        &= 2s^2 + \big(3 - 2\{2s\}\big)s + \frac12 \{2s\}^2 - \frac32 \{2s\} + 1.
\end{align*}

The coefficients of $s$ and the ``constant term'' are periodic functions of $s$
(with period $1$ here).
The presence of terms of the form $\{\alpha s\}$ in our computations
(as a ``coefficient'')
is so ubiquitous it suggests that
the functions $L_P(s)$ can always be written as sums and products
of constants, $s$, and terms of the form $\{\alpha s\}$ for certain numbers $\alpha$.
In other words,
maybe $L_P(s)$ is always of the form
\begin{equation*}
    p_d(s) s^d + p_{d-1}(s) s^{d-1} + \dots + p_1(s)s + p_0(s),
\end{equation*}
where each $p_i(s)$ is a polynomial over certain $\{\alpha_j s\}$.
We will call a function $f$ a \emph{step polynomial}\footnotemark{}
over $\alpha_1, \dots, \alpha_n$
provided there is a polynomial function $p$ such that
\begin{equation*}
    f(t) = p(\{\alpha_1 t\}, \dots, \{\alpha_n t\}).
\end{equation*}
\footnotetext{
    This term was borrowed from Baldoni, Belini, Köppe and Vergne~\cite[p.~3]{BBKV2013}.
    They define a step polynomial to be a function of the form
    \begin{equation*}
        f(t) = p\big( (\zeta_1 t) \bmod q_1, \dots, (\zeta_k t) \bmod q_k \big),
    \end{equation*}
    where $\zeta_i, q_i \in \mathbb Z$
    and $p$ is a polynomial.
    In our definition,
    we demand that $q_1 = \dots = q_k = 1$.
}

For example,
if $P = [\alpha_1, \beta_1] \times \dots \times [\alpha_d, \beta_d]$,
as
\begin{equation*}
    \floor{s \beta_i} - \ceil{s \alpha_i} + 1 =
        (\beta_i - \alpha_i)s - \{\beta_i s\} - \{-\alpha_i s\} + 1,
\end{equation*}
$L_P(s)$ is a quasipolynomial in $s$
whose coefficients are step polynomials over
$-\alpha_1, \dots, -\alpha_d, \beta_1, \dots, \beta_d$.

The remainder of this section is dedicated to show that,
whenever $P$ is a rational polytope,
the function $L_P(s)$ is, indeed,
a quasi-polynomial whose coefficients are step polynomials.
