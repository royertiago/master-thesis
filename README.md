Master Thesis
=============

This repository contains the `.tex` source code
for the dissertation I wrote for my master's degree.

The `pdf` file can be found [here](main.pdf).
