\section{Classical Ehrhart theory}
\label{sec:classical-ehrhart-theory}

As mentioned in the introduction,
the \emph{Ehrhart lattice-point enumerator} $L_P(t)$
of a polytope $P \subseteq \mathbb R^d$
is defined by $L_P(t) = \#(tP \cap \mathbb Z^d)$.
We will agree that $L_P(0) = 1$.
Let us look at some examples.

The simplest polytopes are the real intervals $[\alpha, \beta] \subseteq \mathbb R$
(where $\alpha$ and $\beta$ are arbitrary real numbers).
If we fix $P = [\alpha, \beta]$,
we have
\begin{equation*}
    L_P(t) = \#([t\alpha, t\beta] \cap \mathbb Z) = \floor{t\beta} - \ceil{t\alpha} + 1.
\end{equation*}

We can handle rectangles in higher dimensions using the fact that
\begin{equation*}
    (P \times Q) \cap (A \times B) = (P \cap A) \times (Q \cap B),
\end{equation*}
so that if $P \in \mathbb Z^d$ and $Q \in \mathbb Z^f$ we have
\begin{align*}
    L_{P \times Q}(t) &= \#(tP \times tQ) \cap \mathbb Z^{d + f} \\
        &= \#((tP \cap \mathbb Z^d) \times (tQ \cap \mathbb Z^f)) \\
        &= \#(tP \cap \mathbb Z^d) \#(tQ \cap \mathbb Z^f) \\
        &= L_P(t) L_Q(t).
\end{align*}

If $P = [\alpha_1, \beta_1] \times \dots \times [\alpha_d, \beta_d]$,
we may apply this calculation repeatedly to conclude that
\begin{equation*}
    L_P(t) = (\floor{t\beta_1} - \ceil{t\alpha_1} + 1) \cdots
            (\floor{t\beta_n} - \ceil{t\alpha_n} + 1).
\end{equation*}

Consider now the right triangle $\righttriangle = \conv\{(0, 0), (0, 2), (2, 0)\}$
(Figure~\ref{fig:righttriangle}).
\begin{figure}[t]
    \centering
    \begin{tikzpicture}
        % Draw triangles
        \draw[fill = blue!20] (0, 0) -- (0, 6) -- (6, 0) -- cycle;
        \draw[fill = blue!40] (0, 0) -- (0, 4) -- (4, 0) -- cycle;
        \draw[fill = blue!60] (0, 0) -- (0, 2) -- (2, 0) -- cycle;

        % Add labels
        \path (0.5, 0.5) node {$P$};
        \path (2.5, 0.5) node {$2P$};
        \path (4.5, 0.5) node {$3P$};

        % Draw lattice
        \foreach \x in {-1, ..., 6}
            \foreach \y in {-1, ..., 6}
                \fill (\x, \y) circle[radius=1pt];

        % Draw axes
        \draw (-1, 0) -- (6, 0);
        \draw (0, -1) -- (0, 6);
    \end{tikzpicture}
    \caption{
        Polytope $P = \righttriangle = \conv\{(0, 0), (0, 2), (2, 0)\}$
        and its integer dilates.
    }
    \label{fig:righttriangle}
\end{figure}
The $t$th dilate of this triangle is defined by the intersection of the half-planes
defined by the equations $x \geq 0$, $y \geq 0$, and $x + y \leq 2t$,
so we could sum $2t - i$ for $i$ between $0$ and $2t$
($2t - i$ is the number of integer points inside $t\righttriangle$
which lies in the vertical line $x = i$);
or we could be smarter
and notice this triangle takes half of the space of the square $[0, 2t] \times [0, 2t]$.
Thus,
if we take out the $2t + 1$ integer points which are in the diagonal edge,
the remaining $(2t + 1)^2 - (2t + 1)$ points inside the square
are evenly split between the triangle's inside and outside.
But we will be lazy and use Pick's theorem below.

\begin{theorem}[Pick's theorem]
    If $A$ is the area of a polygon with integer vertices,
    $B$ is the number of integer points in the polygon's boundary,
    and $I$ is the number of integer points in the polygon's interior,
    then
    \begin{equation*}
        A = I + \frac B 2 - 1.
    \end{equation*}
\end{theorem}

Since the total number of integer points in the polygon is $I + B$,
rearranging terms in the equation gives $I + B = A + B/2 + 1$;
in our case, $A = 2t^2$ and $B = 6t$,
so $L_\righttriangle(t) = 2t^2 + 3t + 1$.

Incidentally,
for any integral polygon $P$ which has $B$ integer points in its boundary,
$tP$ will have $tB$ points in its boundary.
The area scales quadratically,
so if $A$ is $P$'s area then $At^2$ is $tP$'s area.
Using Pick's theorem again gives
\begin{equation}
    \label{eq:polygon-polynomial}
    L_P(t) = At^2 + \frac B 2 t + 1.
\end{equation}

Therefore,
for any integral polygon,
its Ehrhart function is actually a polynomial;
therefore,
the function $L_P$ is usually called the Ehrhart \emph{polynomial} of $P$.

In higher dimensions,
we do not get simple formulas for the Ehrhart function,
but amazingly the Ehrhart function is still a polynomial.

\begin{theorem}[Ehrhart's theorem]
    If $P \subseteq \mathbb R^d$ is an integral polytope,
    then $L_P(t)$ is a polynomial in $t$ of degree $d$.
\end{theorem}

Looking back at Equation~\eqref{eq:polygon-polynomial},
we see the leading coefficient is the area of the polygon.
This is a consequence of a different interpretation of $L_P(t)$;
instead of dilating the polytope,
we may shrink the lattice:
\begin{equation*}
    L_P(t) = \#\left(P \cap \frac 1 t \mathbb Z^d\right).
\end{equation*}
Now if we divide this value by $t^d$,
it becomes a Riemann sum of the indicator function of $P$
(the corresponding subrectangles are all hypercubes with side $1/t$);
as polytopes are Jordan-measurable,
we have
\begin{equation*}
    \lim_{t \to \infty} \frac{L_P(t)}{t^d} = \vol P,
\end{equation*}
for any polytope $P \subseteq \mathbb R^d$.
This allows us to conclude that
the leading coefficient of the Ehrhart polynomial of $P$
is always $\vol P$.

Analyzing again the $1$-dimensional polytopes $[\alpha, \beta]$,
if we use the identities $\floor x = x - \{x\}$
and $\ceil x = x + \{-x\}$,
we have
\begin{align*}
    L_{[\alpha, \beta]}(t) &= t\beta - \{t\beta\} - (t\alpha + \{-t\alpha\}) + 1 \\
        &= t(\beta - \alpha) + (1 - \{t\beta\} - \{-t\alpha\}).
\end{align*}
So, $L_{[\alpha, \beta]}(t)$ equals $(\vol P)t$ plus an error,
which is always in the interval $(-1, 1]$.
If $\alpha$ and $\beta$ are rational numbers,
this error term is a periodic function of $t$,
because $\{-t\alpha\}$ and $\{t\beta\}$ will be periodic.
This periodicity is not a coincidence:
it happens for every rational polytope.

We define the \emph{denominator} of a rational polytope $P$
to be the least common multiple
of the denominators of the coordinates of the vertices of $P$
(after reducing each fraction to lowest terms).
We have the following.

\begin{theorem}[Ehrhart's Theorem for Rational Polytopes]
    \label{thm:rational-ehrhart}
    Let $P \subseteq \mathbb R^d$ be a polytope with rational vertices.
    Then there are periodic functions $c_0(t), \dots, c_{d-1}(t)$,
    each with period dividing the denominator of $P$,
    such that
    \begin{equation*}
        L_P(t) = (\vol P)t^d + c_{d-1}(t) t^{d-1} + \dots + c_1(t) t + c_0(t).
    \end{equation*}
\end{theorem}

Such functions are called \emph{quasipolynomials}\footnote{
    A quasipolynomial may also have a periodic leading coefficient,
    but most quasipolynomials we will meet have constant leading coefficient.
}.

The Ehrhart lattice enumerator $L_P(t)$ was originally defined only for positive~$t$.
The expression above gives a natural extension of $L_P(t)$ to negative values:
$t^k$ is well-defined for negative $t$,
and each $c_k(t)$ may be computed on negative integers respecting their periodicity.
This raises the question to whether there is an interesting interpretation
of the values given by $L_P(-t)$;
it turns out that this expression enumerates the points in the interior of the polytope.

More formally, let $P^\circ$ be the relative interior\footnotemark{} of $P$.
Then we have the following.
\footnotetext{
    Denote by $\aff P$ the \emph{affine hull} of $P \subseteq \mathbb R^d$
    (the set of all affine combinations of points in $P$).
    The \emph{relative interior} $P^\circ$ of $P$
    is the set of all $x \in \mathbb R^d$
    such that,
    for some open set $U$ of $\mathbb R^d$ which contains $x$,
    we have $U \cap \aff P \subseteq P$.
}

\begin{theorem}[Ehrhart-Macdonald Reciprocity Theorem]
    \label{thm:reciprocity}
    Let $P$ be a rational polytope
    and $L_P(t)$ the associated Ehrhart quasipolynomial.
    Then
    \begin{equation*}
        L_P(-t) = (-1)^{\dim P} L_{P^\circ}(t),
    \end{equation*}
    where we use the quasipolynomial behavior to extend $L_P$ to negative numbers.
\end{theorem}
