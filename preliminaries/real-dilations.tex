\section{Real dilations}
\label{sec:real-dilations}

When allowing for $t$ to be an arbitrary real positive number,
the Ehrhart function has now more information about the polytope,
and so it becomes more ``sensitive'' to the shape of $P$.

(For simplicity,
we will reserve $t$ to be an integer,
and use the letter $s$ when talking about real dilates;
so, for example,
the function $L_P(t)$ is the restriction of the function $L_P(s)$
for integral values.)

A simple example of this extra sensitivity
is the rectangle $P = [0, 2] \times [0, 1]$
and the triangle $\righttriangle = \conv\{(0, 0), (0, 2), (2, 0)\}$.
Using Pick's theorem we can compute $L_P(t) = 2t^2 + 3t + 1$.
This is the same polynomial we computed for $\righttriangle$
in section~\ref{sec:classical-ehrhart-theory};
that is, $L_P(t) = L_\righttriangle(t)$.
However, for $s = 3/2$,
it is easy to see that $sP$ will contain 8 integer points
($sP = [0, 3] \times [0, 3/2]$ in this case),
whereas $s\righttriangle = \conv\{(0, 0), (0, 3), (3, 0)\}$ will contain 10.
(In our naming convention,
this means $L_P(t) = L_\righttriangle(t)$ but $L_P(s) \neq L_\righttriangle(s)$.)
This shows that $L_P(s)$ is a strictly stronger invariant than $L_P(t)$.

The easiest polytope to deal with is the rectangle
$P = [\alpha_1, \beta_1] \times \dots \times [\alpha_d, \beta_d]$.
Even when $\alpha_i$ and $\beta_i$ and $s$ are real numbers,
we have
\begin{equation*}
    L_P(s) = (\floor{s\beta_1} - \ceil{s\alpha_1} + 1) \cdots
            (\floor{s\beta_n} - \ceil{s\alpha_n} + 1).
\end{equation*}
%TODO: this equation is repeated from ehrhart-theory.tex.
Thus, in the specific case $P = [0, 2] \times [0, 1]$,
we have
\begin{equation*}
    L_P(s) = (\floor{2s} + 1)(\floor s + 1).
\end{equation*}

To calculate $L_\righttriangle(s)$,
we could use the same brute-force method
hinted in section~\ref{sec:classical-ehrhart-theory},
but again we will try to be smart.
First,
note that for any polytope $P \subseteq \mathbb R^d$ and positive real number $\alpha$,
we have
\begin{equation*}
    L_P(\alpha s) = \#(\alpha s P \cap \mathbb Z^d) = L_{\alpha P}(s);
\end{equation*}
therefore, we may define
$\righttriangle' = \frac 1 2 \righttriangle = \conv\{(0, 0), (1, 0), (0, 1)\}$
and compute $L_{\righttriangle'}$ instead.

The polytope $\righttriangle'$ has the interesting property that
it will only ``gain'' new integer points when $s$ is an integer;
in other words,
$L_{\righttriangle'}(s) = L_{\righttriangle'}(\floor s)$
(so that when increasing from an integer $k$ to the integer $k+1$
the value of $L_{\righttriangle'}$ will only change when $s$ becomes the integer $k+1$).
Therefore,
we may use Pick's theorem again to obtain
$L_{\righttriangle'}(t) = \frac 1 2 t^2 + \frac 3 2 t + 1$,
and thus
\begin{align}
    L_\righttriangle(s) &= L_{2\righttriangle}(s) \nonumber\\
        &= L_{\righttriangle'}(2s) \nonumber\\
        &= L_{\righttriangle'}(\floor{2s}) \nonumber\\
        &= \frac 1 2 \floor{2s}^2 + \frac 3 2 \floor{2s} + 1.
            \label{eq:righttriangle-enumerator}
\end{align}

Observe that $L_P(t) = L_\righttriangle(t) = 2t^2 + 3t + 1$,
so for integer dilates the Ehrhart polynomials of $P$ and $\righttriangle$ coincide,
but their real counterparts do not (Figure~\ref{fig:triangle-and-rectangle-graphs}).

\begin{figure}[t]
    \centering
    \begin{tikzpicture}
        \definecolor{rect color}{rgb}{0.0, 0.8, 0.25}
        \definecolor{triang color}{rgb}{0.0, 0.25, 0.8}

        \pgfmathdeclarefunction{L_triang}{1}
            {\pgfmathparse{1/2*floor(2*#1)^2 + 3/2*floor(2*#1) + 1}}
        \pgfmathdeclarefunction{L_rect}{1}
            {\pgfmathparse{(floor(2*#1) + 1) * (floor(#1) + 1)}}

        \begin{axis}[
                axis lines = center,
                minor y tick num = 4,
                ymin = 0,
                ymax = 25,
                xmin = 0,
                xmax = 3.01,
            ]

            \begin{scope}[line width = 1pt]
                % Draw L_{[0, 2] x [0, 1]} and L_\righttriangle where the graphs differ
                \foreach \xbegin/\xend in {0.5/1, 1.5/2, 2.5/3} {
                    \pgfmathsetmacro\height{L_rect(\xbegin)}
                    \addplot[rect color, domain=\xbegin:\xend]{\height};
                    \addplot[rect color, closed dot] coordinates {(\xbegin,\height)};
                    \addplot[rect color, open dot] coordinates {(\xend,\height)};

                    \pgfmathsetmacro\height{L_triang(\xbegin)}
                    \addplot[triang color, thick, domain=\xbegin:\xend]{\height};
                    \addplot[triang color, closed dot] coordinates {(\xbegin,\height)};
                    \addplot[triang color, open dot] coordinates {(\xend,\height)};
                }

                % Draw L_\righttriangle with an offset, where the graphs match
                \foreach \xbegin/\xend in {0/0.5, 1/1.5, 2/2.5} {
                    \pgfmathsetmacro\height{L_triang(\xbegin)}
                    \addplot[triang color, domain=\xbegin:\xend, yshift=.5pt]{\height};
                    \addplot[rect color, domain=\xbegin:\xend, yshift=-.5pt]{\height};

                    \addplot[rect color, closed dot] coordinates {(\xbegin,\height)};
                    \addplot[rect color, open dot] coordinates {(\xend,\height)};
                }
            \end{scope}

            % Draw vertical lines at discontinuities
            \foreach \x [remember=\x as \lastx (initially 0)]
                in {0.5, 1, 1.5, 2, 2.5}
            {
                % lim_{h -> x, h < 0} L_rect(x) = L_rect(last x).
                \pgfmathsetmacro\ylow{L_rect(\lastx)}
                \pgfmathsetmacro\ymid{L_rect(\x)}
                \pgfmathsetmacro\yhigh{L_triang(\x)}

                % We have L_rect(x) <= L_trian(x) for all x >= 0.
                % We'll draw a red line (for L_rect)
                % connecting the left-discontinuity of L_rect, at height ylow,
                % to the right-continuity of L_rect, at height ymid;
                % and then a blue line (for L_triang)
                % connecting the right-continuity of L_rect
                % to the right-continuity of L_triang, at height yhigh.
                % This way, % the blue function will appear to "pop out"
                % above the red function.

                \edef\uglyhack{
                    \noexpand\draw[densely dashed, rect color] (\x,\ylow) -- (\x,\ymid);
                    \noexpand\draw[densely dashed, triang color] (\x,\ymid) -- (\x,\yhigh);
                }\uglyhack
                % pgfplots postpones the expansion of many macros,
                % like, for example, \draw.
                % Therefore, if this code appeared "naked"
                % in the body of the \foreach,
                % the evaluation would happen only at \end{axis},
                % and so the (unexpanded) \x, \ylow etc.
                % would long be gone.
                %
                % The solution: define an "expanding macro" with the code,
                % so that when pgfplots sees the \draw
                % all other expansions had already taken place.
            }
        \end{axis}
    \end{tikzpicture}
    \caption[
        Graph of $L_\righttriangle(s)$ and $L_{[0, 2] \times [0, 1]}(s)$.
    ]{
        Graph of $L_\righttriangle(s)$ (blue)
        and $L_{[0, 2] \times [0, 1]}(s)$ (green).
    }
    \label{fig:triangle-and-rectangle-graphs}
\end{figure}

This property that $\righttriangle'$ has raises an interesting problem:
which polytopes also have this same property,
namely, that $L_P(s) = L_P(\floor s)$?
This question is treated in the next section.
