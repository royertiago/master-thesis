\subsection{Limit behavior of pseudopyramids}

Now we will show how to use the pseudopyramids to compute these projections.

Let $K$ be a pseudopyramid.
Define the \emph{outer radius} $R(K)$ of $K$
to be the smallest number such that
the ball of radius $R(K)$ around the origin contains $K$.
That is,
\begin{equation*}
    R(K) = \inf \{R \geq 0 \mid K \subseteq B_R(0) \}.
\end{equation*}

Define the \emph{front shell} of $K$
to be the set of points in the boundary of $K$
which are not contained in any facet passing through the origin;
that is,
the set of points $x$ in the boundary of $K$ such that
$\lambda x$ is contained in the interior of $K$ for all $0 < \lambda < 1$.
Define, then,
the \emph{inner radius} $r(K)$ of $K$
to be the largest number such that
the ball of radius $r(K)$ around the origin
contains no points of the front shell of $K$
(Figure~\ref{fig:inner-outer-radius}).
Note that this is equivalent to $r(K) S(K)$ to be contained in $K$;
that is,
\begin{equation*}
    r(K) = \sup \{r \geq 0 \mid r S(K) \subseteq K\}.
\end{equation*}

\begin{figure}[t]
    \centering
    \begin{tikzpicture}
        \draw (-1, 0) -- (3, 0);
        \draw (0, -1) -- (0, 3);
        \draw [thick, blue, fill = blue!40]
            (0, 0) -- (2, 1) -- (2, 2) -- (1, 2) -- cycle;

        \pgfmathsetmacro{\innerradius}{veclen(2, 1)}
        \pgfmathsetmacro{\outerradius}{veclen(2, 2)}
        \draw [dashed] (\innerradius, 0) arc
            [radius = \innerradius, start angle = 0, end angle = 90];
        \draw [dashed] (\outerradius, 0) arc
            [radius = \outerradius, start angle = 0, end angle = 90];

        \draw [xshift = -2pt, |<->|]
            (0, 0) -- node[left] {$r(K)$} (0, \innerradius);
        \draw [yshift = -2pt, |<->|]
            (0, 0) -- node[below] {$R(K)$} (\outerradius, 0);
    \end{tikzpicture}
    \qquad
    \begin{tikzpicture}
        \draw (-1, 0) -- (3, 0);
        \draw (0, -1) -- (0, 3);
        \draw [thick, blue, fill = blue!40]
            (0, 0) -- (2.5, 0.5) -- (0.5, 2.5) -- cycle;

        \pgfmathsetmacro{\innerradius}{veclen(1.5, 1.5)}
        \pgfmathsetmacro{\outerradius}{veclen(2.5, 0.5)}
        \draw [dashed] (\innerradius, 0) arc
            [radius = \innerradius, start angle = 0, end angle = 90];
        \draw [dashed] (\outerradius, 0) arc
            [radius = \outerradius, start angle = 0, end angle = 90];

        \draw [xshift = -2pt, |<->|]
            (0, 0) -- node[left] {$r(K)$} (0, \innerradius);
        \draw [yshift = -2pt, |<->|]
            (0, 0) -- node[below] {$R(K)$} (\outerradius, 0);
    \end{tikzpicture}
    \caption{
        Inner and outer radius for two pseudopyramids.
    }
    \label{fig:inner-outer-radius}
\end{figure}

We have the following relation between these radii
and the area of the spherical projection.

\begin{lemma}
    \label{thm:radii-vs-spherical-projection}
    Let $K \subseteq \mathbb R^d$ be a convex body
    which does not contain the origin.
    Then
    \begin{equation*}
        \frac{\vol \ppyr K}{R(\ppyr K)^d}
        \leq
        \frac{\area S(K)}{d}
        \leq
        \frac{\vol \ppyr K}{r(\ppyr K)^d}.
    \end{equation*}
\end{lemma}

\begin{proof}
    Denote by $S_\mu$ the set
    \begin{equation*}
        S_\mu = \bigcup_{0 \leq \lambda \leq \mu} \lambda S(P).
    \end{equation*}

    By the definition of $r(\ppyr P)$ and of $R(\ppyr P)$,
    we have
    \begin{equation*}
        S_{r(\ppyr P)} \subseteq \ppyr P \subseteq S_{R(\ppyr P)}.
    \end{equation*}

    Let $U \subseteq \mathbb R^{d-1}$ and $\varphi: U \to S(P)$
    be such that $\varphi$ is a parametrization of $S(P)$;
    we know such $U$ and $\varphi$ exist
    because we are assuming $P$ does not contain the origin.
    Define now the function $\psi: U \times (0, \mu] \to S_\mu$ by
    \begin{equation*}
        \psi(x, \lambda) = \lambda \varphi(x).
    \end{equation*}
    Then $\psi$ is a bijection between $U \times (0, \mu]$ and $S_\mu \setminus \{0\}$,
    so we may compute the volume of $S_\mu$
    using change of variables~\cite[p.~67]{SpivakManifolds}:
    \begin{align*}
        \vol S_\mu &= \int_{S_\mu} 1 \\
            &= \int_{\psi(U \times (0, \mu])} 1 \\
            &= \int_{U \times (0, \mu]} | \det \psi' |.
    \end{align*}

    Since
    \begin{equation*}
        \newcommand\vertline{\rule[-.5ex]{0.5pt}{2em}}
        \psi'(x, \lambda) = \begin{bmatrix}
            \vertline              &        & \vertline                  & \vertline \\
            \lambda D_1 \varphi(x) & \cdots & \lambda D_{d-1} \varphi(x) & \varphi(x) \\
            \vertline              &        & \vertline                  & \vertline
        \end{bmatrix},
    \end{equation*}
    we have, by the definition of the cross product,
    \begin{equation*}
        \det \psi'(x, \lambda) =
            \lambda^{d-1} \langle
                D_1 \varphi(x) \times \dots \times D_{d-1} \varphi(x),
                \varphi(x)
            \rangle.
    \end{equation*}

    Each of the vectors $D_i \varphi(x)$ is a tangent vector at the point $\varphi(x)$
    in the sphere.
    Since the $D_i\varphi(x)$ are linearly independent,
    their cross product
    is a multiple of the normal vector of the surface at that point.
    In this case,
    the vector $\varphi(x)$ is itself the unit normal,
    so the above inner product is $\pm$ the length of the cross product;
    that is,
    \begin{equation*}
        | \det \psi'(x, \lambda) | = \lambda^{d-1}
            \norm{ D_1 \varphi(x) \times \dots \times D_{d-1} \varphi(x) }.
    \end{equation*}

    Therefore,
    the volume of $S_\mu$ is
    \begin{align*}
        \vol S_\mu &= \int_{U \times (0, \mu]} \lambda^{d-1}
                \norm{ D_1 \varphi(x) \times \dots \times D_{d-1} \varphi(x) } \\
            &= \frac{\mu^d}{d}
                \int_U \norm{ D_1 \varphi(x) \times \dots \times D_{d-1} \varphi(x) } \\
            &= \frac{\mu^d}{d} \area S(P).
    \end{align*}

    Now combining this result with the inclusions between $S_\mu$ and $\ppyr P$
    gives the theorem.
\end{proof}

This lemma,
combined with Lemma~\ref{thm:spherical-approaches-orthogonal},
shows how to calculate the volume of the orthogonal projection
knowing only the volumes of the pseudopyramids.

\begin{lemma}
    \label{thm:volume-projection-from-volume-ppyr}
    Let $K \subseteq \mathbb R^d$ be a convex body,
    and $v$ any unit vector.
    Then
    \begin{equation*}
        \lim_{\mu \to \infty}
            \frac{\vol \ppyr(K + \mu v)}{\mu}
            = \frac{V_K(v)}{d}.
    \end{equation*}
\end{lemma}

\begin{proof}
    Let $N$ be large enough so that $K \subseteq B_N(0)$.
    For any $\mu$,
    as $v$ is a unit vector,
    we have $K + \mu v \subseteq B_{N + \mu}(0)$,
    so
    \begin{equation*}
        R(\ppyr(K + \mu v)) \leq \mu + N.
    \end{equation*}

    Since all the points in the front shell of $\ppyr(K + \mu v)$
    are points of $K$,
    all of them must have norm greater or equal to $\mu - N$.
    Therefore,
    no origin-centered ball with radius smaller than that can contain these points.
    Thus,
    \begin{equation*}
        r(\ppyr(K + \mu v)) \geq \mu - N.
    \end{equation*}

    Using these two inequalities
    and Proposition~\ref{thm:radii-vs-spherical-projection}
    gives
    \begin{equation*}
        \frac{\vol \ppyr(K + \mu v)}{(\mu + N)^d}
        \leq
        \frac{\area S(K + \mu v)}{d}
        \leq
        \frac{\vol \ppyr(K + \mu v)}{(\mu - N)^d},
    \end{equation*}
    which may be rewritten as
    \begin{equation*}
        \begin{adjustbox}{center} % Looooong equation
            % For some reason, adjustbox doesn't like \begin{equation*} or $$.
            $\displaystyle
                \frac{(\mu - N)^d}{\mu^d} \frac{\mu^{d-1} \area S(K + \mu v)}{d}
                \leq
                \frac{\vol \ppyr(K + \mu v)}{\mu}
                \leq
                \frac{(\mu + N)^d}{\mu^d} \frac{\mu^{d-1} \area S(K + \mu v)}{d}.
            $
        \end{adjustbox}
        % Double nesting to get the vertical spacing right.
    \end{equation*}

    Now Theorem~\ref{thm:spherical-approaches-orthogonal}
    and the squeeze theorem finish the proof.
\end{proof}

For example,
for $K = [0, 1]^2$ and $v = (1, 0)$,
we have $\vol \ppyr(K + \mu v) = 1 + \frac{\mu}{2}$,
so $\lim_{\mu \to \infty} \frac{\vol \ppyr(K + \mu v)}{\mu} = \frac12$,
which is precisely one-half of the area of $\{0\} \times [0, 1]$,
the projection $K'$ of $K$ on the $y$-axis.
This highlights that,
for large $\mu$,
the pseudopyramid $\ppyr(K + \mu v)$ ``behaves like'' an actual pyramid,
with height $\mu$ and base $K'$.
