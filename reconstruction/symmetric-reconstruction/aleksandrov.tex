\subsection[Aleksandrov and Hausdorff]
{Aleksandrov's projection theorem and Hausdorff distances}
\label{sec:aleksandrov}

The punchline is Aleksandrov's projection theorem.
Let $K \subseteq \mathbb R^d$ be a convex body
(that is,
a covex, compact set with nonempty interior).
For any unit vector $v$,
we will denote by $V_K(v)$
the $(d-1)$-dimensional area of the orthogonal projection of $K$ in $\{v\}^\perp$.

For example,
let $K = [0, 1] \times [0, 1] \subseteq \mathbb R^2$,
$v = (0, 1)$ and $v' = (\frac{\sqrt 2}{2}, \frac{\sqrt 2}{2})$.
Then $V_K(v) = 1$ and $V_K(v') = \sqrt 2$.

A convex body $K$ is said to be \emph{symmetric}
if $x \in K$ if and only if $-x \in K$.
An importan reconstruction theorem for symmetric convex bodies
is Aleksandrov's projecion theorem
(see e.g.~\cite[p.~115]{GeometricTomography}).

\begin{theorem}[Aleksandrov's projection theorem]
    Let $K$ and $H$ be two symmetric convex bodies in $\mathbb R^d$
    such that $V_K(v) = V_H(v)$ for all unit vectors $v$.
    Then $K = H$.
\end{theorem}

So,
the goal is to compute the function $V_K$ using the Ehrhart functions $L_{K + w}$.
The two main tools are the Hausdorff distance and pseudopyramids.

For $\lambda \geq 0$ and $x \in \mathbb R^d$,
denote by $B_\lambda(x)$ the ball with radius $\lambda$ centered at $x$.
If $K$ is a convex body and $\lambda \geq 0$,
define $K_\lambda$ by
\begin{equation*}
    K_\lambda = \bigcup_{x \in K} B_\lambda(x).
\end{equation*}

The Hausdorff distance $\rho(K, H)$ between two convex bodies $K$ and $H$
is defined to be (Figure~\ref{fig:hausdorff-distance})
\begin{equation*}
    \rho(K, H) = \inf \{ \lambda \geq 0 \mid
        K \subseteq H_\lambda \text{ and } H \subseteq K_\lambda
    \}.
\end{equation*}

\begin{figure}[t]
    \centering
    \begin{tikzpicture}
        \draw[line width = 2pt, blue] (0, 0) -- (0, 2) -- (2, 0) -- cycle;
        \draw[line width = 2pt, green!30!black]
            (0.5, 0.5) -- (2, 0.5) -- (2, 2) -- (0.5, 2) -- cycle;

        \pgfmathsetmacro{\rho}{sqrt(2)}
        \draw[thin, blue]
            (-\rho, 0) arc [radius = \rho, start angle = 180, end angle = 270]
            -- ++(2, 0) arc [radius = \rho, start angle = -90, end angle = 45]
            -- ++(-2, 2) arc [radius = \rho, start angle = 45, end angle = 180]
            -- cycle;

        \draw[thin, green!30!black] (0.5, 0.5)
            ++(-\rho, 0) arc [radius = \rho, start angle = 180, end angle = 270]
            -- ++(1.5, 0) arc [radius = \rho, start angle = -90, end angle = 0]
            -- ++(0, 1.5) arc [radius = \rho, start angle = 0, end angle = 90]
            -- ++(-1.5, 0) arc [radius = \rho, start angle = 90, end angle = 180]
            -- cycle;
    \end{tikzpicture}
    \caption[
        Hausdorff distance between two convex sets.
    ]{
        Hausdorff distance between two convex sets.
        The thick lines are the boundaries of the sets $K$ and $H$;
        the thin lines are the boundaries of the sets
        $K_\lambda$ and $H_\lambda$.
    }
    \label{fig:hausdorff-distance}
\end{figure}

It can be shown that
the set of convex sets in $\mathbb R^d$
is a metric space under the Hausdorff distance
and that the Euclidean volume is continuous in this space
(see e.g.~\cite[p.~9]{GeometricTomography}),
but we just need the following special case of this theory.

\begin{lemma}
    \label{thm:hausdorff-distance}
    Let $K$ and $A_1, A_2, \dots$ be convex bodies.
    If $\lim_{i \to \infty} \rho(K, A_i) = 0$,
    then $\lim_{i \to \infty} \vol A_i = \vol K$.
\end{lemma}
