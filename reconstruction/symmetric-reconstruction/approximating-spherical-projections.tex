\subsection{Approximating spherical projections}

Given a convex body $K$,
define its spherical projection $S(K)$ by
(Figure~\ref{fig:spherical-projection})
\begin{equation*}
    S(K) = \left\{ \frac{x}{\norm x}
        \,\middle|\, x \in K \text{ and } x \neq 0 \right
    \}.
\end{equation*}

\begin{figure}[t]
    \centering
    \begin{tikzpicture}
        \draw[->] (-1, 0) -- (6, 0);
        \draw[->] (0, -1) -- (0, 4);

        \draw (1, 0) arc [start angle = 0, end angle = 90, radius = 1];

        \filldraw [thick, blue, fill = blue!40]
            (5, 3) -- (6, 3) -- (6, 4) -- (5, 4) -- cycle;
        \node[blue] at (6.5, 3.5) {$K + v$};

        \draw [gray, thin] (5, 4) -- (0, 0) -- (6, 3);

        \pgfmathsetmacro{\startangle}{atan(3/6)}
        \pgfmathsetmacro{\endangle}{atan(4/5)}
        \pgfmathsetmacro{\radius}{veclen(5, 3)}

        \draw [red, very thick] (\startangle:1)
            arc [start angle = \startangle, end angle = \endangle, radius = 1];
        \draw [red] (\startangle:1)
            +(1, 0) node {$S(K + v)$};

        \draw [dotted] (\startangle - 10:\radius)
            arc [start angle = \startangle - 10,
                end angle = \endangle + 10, radius = \radius];
        \draw [red, thick] (\startangle : \radius)
            arc [start angle = \startangle, end angle = \endangle, radius = \radius]
            ++(-1.5, 0) node {$\norm v S(K + v)$};
    \end{tikzpicture}
    \caption{
        Spherical projection of $K + v$.
    }
    \label{fig:spherical-projection}
\end{figure}

The connection between pseudopyramid volumes
and areas of projections
can be seen in Figure~\ref{fig:spherical-projection}.
The set $\norm v S(K + v)$ is a dilation of the projection $S(K + v)$ of $K$.
Note that the shape of $\norm v S(K + v)$
``looks like'' the orthogonal projection of $K$ in $\{v\}^\perp$;
that is,
the area of $\norm v S(K + v)$
approximates $V_K(v)$.

If the pseudopyramid were an actual pyramid
(with base $\norm v S(K + v)$),
then using the formula $v = \frac{Ah}{d}$ for the volume of a pyramid
would allow us to discover what is the area of the projection,
which would give an approximation to $V_K(v)$.
We will show that this formula is true ``in the infinity'';
that is,
using limits,
we can recover the area of the projection
using taller and taller pseudopyramids.

For convex bodies $K$,
the set $S(K)$ is a manifold\footnote{
    Technically,
    $S(K)$ will be a manifold-with-corners
    (see~\cite[p.~137]{SpivakManifolds}).
    However,
    their interiors relative to the $(d-1)$-dimensional sphere $S^{d-1}$
    are manifolds,
    and since we are dealing with areas
    there will be no harm in ignoring these boundaries.
}.
If $K$ does not contain the origin in its interior,
then $S(K)$ may be parameterized with a single coordinate system;
that is,
there is a set $U \subseteq \mathbb R^{d-1}$
and a continuously differentiable function $\varphi: U \to S(K)$
which is a bijection between $U$ and $S(K)$.
Since we want to move $P$ towards infinity,
this shall always be the case
if the translation vector is long enough.
In this case,
we define its area to be~\cite[p.~126]{SpivakManifolds}
\begin{align*}
    \area S(K) &= \int_U \norm{D_1 \varphi \times \dots \times D_{d-1} \varphi} \\
        &= \int_U \norm{
            \frac{\partial \varphi}{\partial x_1}
            \times \dots \times
            \frac{\partial \varphi}{\partial x_{d-1}} dx_1 \dots dx_{d-1}
        }.
\end{align*}

The following theroem states that
the spherical projection approximates,
in a sense,
the orthogonal projection,
for large enough translation vectors.

\begin{theorem}
    \label{thm:spherical-approaches-orthogonal}
    Let $v$ be a unit vector
    and $K \subseteq \mathbb R^d$ a convex body.
    Then
    \begin{equation*}
        \lim_{\mu \to \infty} \mu^{d-1} \area S(P + \mu v) = V_K(v).
    \end{equation*}
\end{theorem}

\begin{proof}
    By rotating all objects involved if needed,
    we may assume that $v = (0, \dots, 0, 1)$.
    Let $N$ be large enough that $K \subseteq B_N(0)$;
    we will assume that $\mu > N$,
    so that $K + \mu v$ lies strictly above the hyperplane $x_d = 0$.

    Let $K'$ be the orthogonal projection of $K$ into $\{v\}^\perp$.
    We will think of $K'$ as being a subset of $\mathbb R^{d-1}$.
    Denote by $K_\mu$ the projection of the set $\mu S(K + \mu v)$ on $\mathbb R^{d-1}$
    (Figure~\ref{fig:spherical-orthogonal-projection});
    that is,
    first project $K + \mu v$ to the sphere with radius $\mu$,
    then discard the last coordinate.
    Note this is similar to project it to the hyperplane $x_d = 0$.
    We will show that,
    as $\mu$ goes to infinity,
    both the Hausdorff distance between $K_\mu$ and $K'$
    and the difference between the volume of $K_\mu$ and the area of $\mu S(K + \mu v)$
    tend to zero.

    \begin{figure}[t]
        \centering
        \begin{tikzpicture}
            \pgfmathsetmacro{\Mu}{4}
            \pgfmathsetmacro{\N}{2}
            \draw[gray, thin] (-\Mu - 1, 0) -- (\Mu + 1, 0);

            % Sphere ||x|| = mu
            \draw[dashed] (\Mu, 0) arc [radius = \Mu, start angle = 0, end angle = 180];
            \draw[|<->|] (-\Mu, 0) -- node [below] {$\mu$} (0, 0);

            % Sphere of radius N around the polytope
            \draw[dashed] (0, \Mu) circle [radius = \N];
            \draw[|<->|] (-\N, \Mu) -- node [above] {$N$} (0, \Mu);

            % K + mu v
            \coordinate (A) at (0.6, \Mu + 1.0);
            \coordinate (B) at (1.6, \Mu + 0.8);
            \coordinate (C) at (1.8, \Mu + 0.4);
            \coordinate (D) at (0.8, \Mu + 0.4);
            \draw [thick, blue, fill = blue!40] (A) -- (B) -- (C) -- (D) -- cycle;
            \node [blue] at (\N + 0.5, \Mu + 0.5) {$K + \mu v$};

            % K'
            \draw [blue, line width = 2pt, yshift = 1pt]
                (A |- 0, 0) -- node [above] {$K'$} (C |- 0, 0);

            % mu S(K + mu v)
            \pgfmathanglebetweenpoints{\pgfpointorigin}{\pgfpointanchor{C}{center}}
            \edef\startangle{\pgfmathresult}
            \pgfmathanglebetweenpoints{\pgfpointorigin}{\pgfpointanchor{A}{center}}
            \edef\endangle{\pgfmathresult}
            \draw [very thick, red] (\startangle:\Mu)
                coordinate (S-right)
                arc [radius = \Mu, start angle = \startangle, end angle = \endangle]
                coordinate (S-left);
            \node [red, right = 0.5] at (S-right) {$\mu S(K + \mu v)$};

            % K_mu
            \draw [green!30!black, line width = 2pt, yshift = -1pt]
                (S-left |- 0, 0) -- node [below] {$K_\mu$} (S-right |- 0, 0);

            \begin{scope}[very thin]
                % Lines from the origin to K
                \draw [red] (0, 0) -- (A);
                \draw [red] (0, 0) -- (C);

                % Vertical lines corresponding to the projections
                \draw [blue] (A) -- (A |- 0, 0);
                \draw [blue] (C) -- (C |- 0, 0);
                \draw [green!30!black] (S-left) -- (S-left |- 0, 0);
                \draw [green!30!black] (S-right) -- (S-right |- 0, 0);
            \end{scope}
        \end{tikzpicture}
        \caption[
            Spherical and orthogonal projections.
        ]{
            The spherical projection $\mu S(K + \mu v)$,
            when projected orthogonally to the plane $x_d = 0$
            (the set $K_\mu$),
            approaches the volume of the projection $K'$.
        }
        \label{fig:spherical-orthogonal-projection}
    \end{figure}

    First, let us bound the Hausdorff distance between $K'$ and $K_\mu$.
    If $x \in K + \mu v$,
    then $x$ gets projected to a point $x_0 \in K'$
    by just discarding the last coordinate;
    however, to be projected to a point $x_1 \in K_\mu$,
    first we replace $x$ by $x' = \frac{\mu}{\norm x} x$
    to get a point $x' \in \mu S(K + \mu v)$,
    and then the last coordinate of $x'$ is discarded.
    Note that $x_1 = \frac{\mu}{\norm x} x_0$;
    therefore,
    the distance between these two points is
    \begin{equation*}
        \norm{x_0 - x_1}
            = \left| 1 - \frac{\mu}{\norm x} \right| \norm{x_0}
            = \frac{|\norm x - \mu|}{\norm x} \norm{x_0}
    \end{equation*}

    We have $x \in K + \mu v \subseteq B_N(\mu v)$,
    so $\mu - N \leq \norm x \leq \mu + N$.
    As $v = (0, \dots, 0, 1)$ and $x_0$ is $x$ without its last coordinate,
    we have $\norm{x_0} \leq N$
    (because, in $\mathbb R^{d-1}$,
    we have $x_0 \in B_N(0)$).
    So,
    the distance between $x_0$ and $x_1$ is at most $\frac{N^2}{\mu - N}$.

    Every point in $K'$ and in $K_\mu$ can be obtained through these projections.
    This means that,
    given any point $x_0$ in one of the sets,
    we may find another point $x_1$ in the other set
    which is at a distance of at most $\frac{N^2}{\mu + N}$ from the former,
    because we can just pick a point $x$ whose projection is $x_0$;
    then its other projection $x_1$ will be close to $x_0$.
    Thus
    \begin{equation*}
        \rho(K', K_\mu) \leq \frac{N^2}{\mu - N},
    \end{equation*}
    so by Theorem~\ref{thm:hausdorff-distance}
    the volumes of $K_\mu$ converges to $\vol K'$.

    Now,
    let us relate $\vol K_\mu$ with $\mu^{d-1} \area S(K + \mu v)$.
    If $y = (y_1, \dots, y_d)$ is a point in $\mu S(K + \mu v)$,
    we know that $\norm y = \mu$
    and that $y_d > 0$
    (because we are assuming $\mu > N$).
    Therefore,
    if we define $\varphi: K_\mu \to \mu S(K + \mu v)$ by
    \begin{equation*}
        \varphi(y_1, \dots, y_{d-1}) = \left(
            y_1, \dots, y_{d-1}, \sqrt{\mu^2 - y_1^2 - \dots - y_{d-1}^2}
        \right),
    \end{equation*}
    then $\varphi$ will be a differentiable bijection
    between $K_\mu$ and $\mu S(K + \mu v)$,
    so that $\varphi$ is a parametrization for $\mu S(K + \mu v)$.

    For the partial derivatives,
    we have $\frac{\partial \varphi_i}{\partial y_j} = [i = j]$ if $i < d$;
    that is,
    the partial derivatives behave like the identity.
    For $i = d$,
    we have
    \begin{equation*}
        \frac{\partial \varphi_d}{\partial y_j} =
            \frac{y_j}{\sqrt{\mu^2 - y_1^2 - \dots - y_{d-1}^2}}.
    \end{equation*}

    Now, by definition of $N$, we have
    \begin{equation*}
        \left| \frac{\partial \varphi_d}{\partial y_j} \right|
            \leq \frac{N}{\sqrt{\mu^2 - N^2}},
    \end{equation*}
    so the vectors $D_i \varphi$ converge uniformly to $e_i$ for large $\mu$.
    Since the generalized cross product is linear in each entry,
    the vector $D_1 \varphi \times \dots \times D_{d-1} \varphi$
    converges uniformly to $e_d$,
    and thus the number
    \begin{align*}
        \left| \vol K_\mu - \area \mu S(K + \mu v) \right|
        &=
            \left| \int_{K_\mu} 1 -
                \int_{K_\mu} \norm{D_1 \varphi \times \dots \times D_{d-1} \varphi}
            \right| \\
        &\leq
            \int_{K_\mu} \Big|
                1 - \norm{D_1 \varphi \times \dots \times D_{d-1} \varphi}
            \Big|
    \end{align*}
    converges to zero.

    Now, since $\area (\mu S(K + \mu v)) = \mu^{d-1} \area S(K + \mu v)$,
    combining these two convergence results gives the theorem.
\end{proof}
