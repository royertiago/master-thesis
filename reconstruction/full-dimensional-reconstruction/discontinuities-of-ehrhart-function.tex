\subsection{Discontinuities of the Ehrhart function}
\label{sec:discontinuities-of-ehrhart-function}

We will extract information about the polytope $P$
by analyzing the discontinuities of the various functions $L_{P + w}(s)$.
This section discusses the meaning of these discontinuities.

Consider the polytope $P = [\frac23, 1] \times [0, \frac 13]$
(Figure~\ref{fig:ehrhart-function-small-square}),
whose Ehrhart function is
\begin{equation*}
    L_P(s) = \left(\floor s - \ceil{\frac 23 s} + 1\right)
            \left(\floor{\frac 13 s} + 1\right).
\end{equation*}
We will analyze what happens
for the dilation parameters $s = 1$, $s = \frac 32$ and $s = 3$.

\begin{figure}[t]
    \centering
    \newcommand{\drawscaledsquare}[2]{
        % #1: scale factor
        % #2: label printed inside
        \filldraw [blue, line width = 1pt, fill = blue!40]
            (#1 * 2/3, 0) -- (#1, 0) -- (#1, #1 * 1/3) -- (#1 * 2/3, #1 * 1/3) -- cycle;

        \node at (#1 * 5/6, #1 * 1/6) {#2};
    }
    \begin{tikzpicture}[scale = 1.5]
        \draw (-1, 0) -- (4, 0);
        \draw (0, -0.5) -- (0, 2);

        \foreach \x in {-1, ..., 4}
            \foreach \y in {0, 1, 2}
                \fill[gray] (\x, \y) circle [radius = 1pt];

        \drawscaledsquare{1}{$P$};
        \drawscaledsquare{1.5}{$\frac32 P$};
        \drawscaledsquare{3}{$3P$};
    \end{tikzpicture}

    \centering
    \begin{tikzpicture}
        \begin{axis}[
            axis lines = center,
            xmin = 0,
            xmax = 3.5,
            ymin = 0,
            ymax = 4.5,
        ]
            \draw [thick]
                (0, 0) -- (1, 0)
                (1, 1) -- (1.5, 1)
                (1.5, 0) -- (2, 0)
                (2, 1) -- (3, 1)
                (3, 2) -- (3.5, 2)
                ;

            \draw [dashed]
                (1, 0) -- (1, 1)
                (1.5, 1) -- (1.5, 0)
                (2, 0) -- (2, 1)
                (3, 1) -- (3, 4)
                ;

            \addplot [closed dot] coordinates {
                (0, 1) (1, 1) (1.5, 1) (2, 1) (3, 4)
            };
            \addplot [open dot] coordinates {
                (0, 0) (1, 0) (1.5, 0) (2, 0) (3, 1) (3, 2)
            };
        \end{axis}
    \end{tikzpicture}
    \caption{
        Polytope $P = [\frac23, 1] \times [0, \frac 13]$
        and its Ehrhart function.
    }
    \label{fig:ehrhart-function-small-square}
\end{figure}

At $s = 1$,
the polytope $P$ is ``gaining'' a new integer point,
namely,
$(1, 0)$.
This gain is marked in the Ehrhart function of $P$ by a discontinuity:
$L_P(s)$ is left-discontinuous at $s = 1$.
It is a jump-discontinuity,
and the magnitude of the jump is
\begin{equation*}
    L_P(1) - \lim_{s \to 1^-} L_P(s) = 1 - 0 = 1,
\end{equation*}
which is the number of points which $P$ gains when reaching $s = 1$.

At $s = \frac32$,
the polytope ``loses'' the integer point $(1, 0)$.
Again this is marked in $L_P(s)$ by a discontinuity;
we have a right-discontinuity at $s = \frac32$,
which is again a jump-discontinuity,
and the magnitude of the jump is
\begin{equation*}
    L_P(\tfrac 32) - \lim_{s \to {\frac32}^+} L_P(s) = 1 - 0 = 1,
\end{equation*}
again the number of points lost by $P$ at $s = \frac 32$.

At $s = 3$,
these two situations happen simultaneously.
The polytope $P$ gains the points $(2, 1)$, $(3, 1)$ and $(3, 0)$,
and then immediately loses the points $(2, 1)$ and $(2, 0)$.
The gain is marked by a left-discontinuity,
with a jump of magnitude $3$,
and the loss is marked by a right-discontinuity,
with a jump of magnitude $2$.

Observe that these discontinuities are very regular:
when gaining points,
there is a left-discontinuity
and the function $L_P(s)$ ``jumps upwards'',
and when losing points,
there is a right-discontinuity
and the function $L_P(s)$ ``jumps downwards''.
The magnitude of the jump is exactly the number of points gained or lost
at that dilation parameter.
We will now formalize
how this behavior gives information about the facets of $P$.

Write $P$ as
\begin{equation*}
    P = \bigcap_{i = 1}^n \{ x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
    \tag{\ref{eq:intersection-representation} revisited}
\end{equation*}
where each $a_i$ is a primitive integer vector,
and let $F_i$ be the $i$th facet of $P$;
that is,
\begin{equation*}
    F_i = P \cap \{x \mid \langle a_i, x \rangle = b_i \}.
\end{equation*}

The facets $F_i$ for which $b_i < 0$
were called \emph{back facets} in Section~\ref{sec:translation-variant}.
By analogy,
we will call the facets $F_i$ for which $b_i > 0$ by \emph{front facets}.
The relation between the magnitude of the discontinuities
and the number of points in back and front facets
is summarized by the following lemma.

\begin{lemma}
    \label{thm:jump-magnitudes}
    Let $P$ be a full-dimensional polytope
    and $s_0$ a discontinuity point of $L_P(s)$.
    If $s_0$ is a left-discontinuity,
    then the magnitude
    \begin{equation*}
        L_P(s_0) - \lim_{s \to s_0^-} L_P(s)
    \end{equation*}
    of the jump
    is the number of integral points contained in the front facets of $s_0 P$.
    If $s_0$ is a right-discontinuity,
    then the magnitude
    \begin{equation*}
        L_P(s_0) - \lim_{s \to s_0^+} L_P(s)
    \end{equation*}
    of the jump
    is the number of integral points contained in the back facets of $s_0 P$.
\end{lemma}

\begin{proof}
    In the hyperplane representation of $P$,
    if a point $x_0$ is not contained in $s_0 P$,
    then it must violate at least one inequality;
    that is,
    \begin{equation*}
        \langle a_i, x_0 \rangle > s_0 b_i
    \end{equation*}
    for some $i$.
    Since this is a strict inequality,
    for any $s$ sufficiently close to $s_0$ we also have
    \begin{equation*}
        \langle a_i, x_0 \rangle > s b_i,
    \end{equation*}
    and thus if $x \notin s_0 P$ then $x \notin s P$
    for all $s$ sufficiently close to $s_0$.

    This means that the difference between $L_P(s_0)$
    and any of the limits
    \begin{equation*}
        L_P(s_0^+) = \lim_{s \to s_0^+} L_P(s)
        \quad \text{and} \quad
        L_P(s_0^-) = \lim_{s \to s_0^-} L_P(s)
    \end{equation*}
    must be due to points $x_0 \in s_0 P$.

    Let $x_0$ be a point in $s_0 P$.
    When considering the inequalities of $s P$ for $s < s_0$,
    if $b_i \leq 0$ we have
    \begin{equation*}
        \langle a_i, x_0 \rangle \leq s_0 b_i \leq s b_i,
    \end{equation*}
    and thus all these inequalities are satisfied.
    Thus the only inequalities that might be violated
    are the ones when $b_i > 0$,
    which correspond to front facets.

    Suppose then that $x_0$ is contained in the front facet of $s_0 P$
    which is determined by the inequality $\langle a_i, x \rangle \leq s_0 b_i$.
    The point $x_0$ satisfies this inequality with equality;
    that is,
    \begin{equation*}
        \langle a_i, x_0 \rangle = s_0 b_i.
    \end{equation*}

    If $s < s_0$, as $b_i > 0$, we have
    \begin{equation*}
        \langle a_i, x_0 \rangle = s_0 b_i > s b_i,
    \end{equation*}
    which shows that $x_0 \notin sP$ for all $s < s_0$.

    Conversely,
    if $x_0$ is not contained in any front facet of $s_0 P$,
    it will satisfy
    \begin{equation*}
        \langle a_i, x_0 \rangle < s_0 b_i
    \end{equation*}
    for all $i$ with $b_i > 0$,
    and thus for all $s < s_0$ sufficiently close to $s_0$
    the point $x_0$ will still satisfy the corresponding inequality for $s P$.
    Thus, $x_0 \in s P$ for all $s$ close enough to $s_0$.

    This means that the difference between $L_P(s_0)$ and $L_P(s_0^-)$
    must be due to integer points in front facets of $s_0 P$,
    and thus $L_P(s_0) - L_P(s_0^-)$
    is the number of integer points in front facets of $s_0 P$.

    The analysis for $s > s_0$ is analogous.
\end{proof}

For example,
the polytope $P = [\frac 23, 1] \times [0, \frac13]$
(Figure~\ref{fig:ehrhart-function-small-square})
can be written as
\begin{align*}
    P = & \phantom{{}\cap{}}
                \{(x, y) \in \mathbb R^2 \mid x \leq 1 \} \\
        & {}\cap \{(x, y) \in \mathbb R^2 \mid -x \leq \tfrac23 \} \\
        & {}\cap \{(x, y) \in \mathbb R^2 \mid y \leq \tfrac13 \} \\
        & {}\cap \{(x, y) \in \mathbb R^2 \mid -y \leq 0 \}.
\end{align*}

This polytope has two front facets,
namely $F_1 = \{1\} \times [0, \frac 13]$
(the right edge)
and $F_3 = [\frac 23, 1] \times \{\frac 13\}$
(the upper edge),
and one back facet,
namely $F_2 = \{\frac23\} \times [0, \frac 13]$
(the left edge).
The bottom edge,
$F_4 = [\frac 23, 1] \times \{0\}$,
is contained in the $x$-axis,
which is determined by the equation $y = 0$,
and thus is neither a front facet nor a back facet.

At $s = 1$,
the Ehrhart function $L_P(s)$ has a right-discontinuity,
and the magnitude of the jump there is $1$;
this corresponds to $s F_1$ containing one integer point,
namely, $(1, 0)$.

At $s = \frac 32$,
the Ehrhart function $L_P(s)$ has a left-discontinuity,
and the magnitude of the jump is also $1$;
this corresponds to $s F_2$ containing one integer point,
again $(1, 0)$.

At $s = 3$,
we have both a left and a right-discontinuity at $L_P(s)$.
The left discontinuity has magnitude $3$,
which corresponds to the three points contained in $s(F_1 \cup F_3)$
(namely, $(2, 1)$, $(3, 1)$ and $(3, 0)$),
and the right discontinuity has magnitude $2$,
which corresponds to the two points contained in $s F_2$
(namely, $(2, 1)$ and $(2, 0)$).
Note that $(3, 1)$ is not counted twice,
despite appearing in both $F_1$ and $F_3$,
whereas $(2, 1)$ is counted both as an ``entering point''
(because it is contained in the front facet $F_3$)
and as a ``leaving point''
(because it is contained in the back facet $F_2$).
