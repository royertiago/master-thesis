\subsection{Pseudo-Diophantine equations}
\label{sec:pseudodiophantine-equations}

We will now show how to recover $b_1$.
From the discussion preceding the proof of Lemma~\ref{thm:isolating-largest-vector},
we have a sequence of values $V_k$
from which we extract $\rvol F_1$.
The $V_k$ represent the values of some discontinuities of $L_{P + w_k}(s)$
which we know are due to points passing through $F_1$.
We are interested
in these discontinuities.

For simplicity,
we will assume that there are infinitely many good $V_k$.
For all large enough $k$ for which $V_k$ is good,
there is precisely one real number $s_k$ in the interval
$(\alpha_k, \alpha_k + \epsilon_k)$
which corresponds to a discontinuity of $L_{P + w_k}(s)$,
and this discontinuity was caused by $F_1$.
That is,
we have infinitely many equations of the following form:
there is some integer $m_k$ such that
\begin{equation*}
    s_k (b_1 + k \langle a_1, w_0 \rangle) = m_k.
\end{equation*}

The following lemma asserts that
there is exactly one solution for $b_1$
of this infinite set of ``semi-Diophantine equations''.

\begin{lemma}
    \label{thm:pseudodiophantine-equations}
    Let $s_l \in \mathbb R$, $k_l \in \mathbb Z$ be given for each integer $l > 0$,
    where the $s_l$ are non-integer numbers
    such that $\{s_l\}$ approaches zero for $l \to \infty$.
    Then the infinite system of equations
    \begin{equation*}
        s_l (b + k_l) = m_l,
        \qquad
        \text{$l > 0$}
    \end{equation*}
    for $b \in \mathbb R$ and $m_l \in \mathbb Z$,
    has at most one solution.
\end{lemma}

\begin{proof}
    Without loss of generality, we may assume all $s_l$ and all $k_l$ are different.
    Let $(b, m_1, m_2, m_3, \dots)$ be a solution for this system of equations.
    If we did not have the integrality constraint on $m_l$,
    then all the solutions of this infinite system of equations
    would have the form
    \begin{equation*}
        (b + \lambda, m_1 + s_1 \lambda, m_2 + s_2 \lambda, \dots),
        \qquad \lambda \in \mathbb R.
    \end{equation*}

    The integrality constraint dictates that $m_l + s_l \lambda$ is an integer,
    say $m'_l$;
    thus for some integer $\nu_l = m'_l - m_l$ we have $s_l \lambda = \nu_l$.

    Assume $b$ is irrational;
    then all $s_l$ are also irrational.
    Since
    \begin{equation*}
        \lambda = \frac{\nu_1}{s_1} = \frac{\nu_2}{s_2},
    \end{equation*}
    so we know that $\frac{s_1}{s_2} = \frac{\nu_1}{\nu_2}$ is a rational number.
    Since $b = \frac{m_1}{s_1} - k_1 = \frac{m_2}{s_2} - k_2$,
    we have
    \begin{align*}
        k_2 - k_1 &= \frac{m_1}{s_1} - \frac{m_2}{s_2} \\
        s_1( k_2 - k_1 ) &= m_1 - \frac{s_1}{s_2} m_2.
    \end{align*}

    Since $k_1 \neq k_2$,
    the above equation shows that $s_1$ is rational;
    this is a contradiction,
    unless $\nu_2 = 0$,
    which amounts to $\lambda = 0$.
    Therefore,
    if $b$ is irrational,
    we indeed have at most one solution.

    Assume now that $b$ is rational.
    Therefore,
    all $s_l$ are rational,
    so we may write $s_l = \frac{p_l}{q_l}$
    for relatively prime $p_l, q_l$.

    For $m_l + s_l \lambda$ to be an integer,
    $\lambda$ must be a rational number,
    say, $\lambda = \frac p q$.
    The number $s_l \lambda$ must also be an integer,
    say $r$;
    that is,
    \begin{equation*}
        p_l p = q_l q r.
    \end{equation*}
    Reducing both sides modulo $q_l$ gives
    \begin{equation*}
        p_l p \equiv 0 \mod{q_l};
    \end{equation*}
    as $p_l$ and $q_l$ are relatively prime,
    this shows that $q_l$ divides $p$.

    Finally,
    since we assumed that no $s_l$ is an integer,
    but the distance of $s_l$ to the nearest integer approaches zero as $l \to \infty$,
    we must have $\lim_{l \to \infty} q_l = \infty$.
    So $p$ is a multiple of arbitrarily large numbers,
    thus the only possibility is $p = 0$,
    which implies $\lambda = 0$,
    from which the lemma follows.
\end{proof}

The fact that each $s_k$ is contained in $(\alpha_k, \alpha_k + \epsilon_k)$,
together with properties~\ref{item:alpha_k-larger-than-k},
\ref{item:alpha_k-tends-to-infinity} and~\ref{item:epsilon_k-tends-to-zero}
from Lemma~\ref{thm:isolating-largest-vector},
guarantees that the fractional part $\{s_k\}$ of $s_k$,
although never zero,
approaches zero as $k \to \infty$.
Therefore,
we are in position to apply the lemma,
which thus determines $b_1$ uniquely.
