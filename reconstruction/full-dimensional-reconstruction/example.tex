\subsection{An example: reconstructing a rectangle}
\label{sec:reconstruction-example}

\begin{figure}[t]
    \centering
    \begin{tikzpicture}
        \draw[->] (-1, 0) -- (3, 0);
        \draw[->] (0, -1) -- (0, 2);
        \draw[-|] (0, 0) -- (2, 0) node [below] {$2$};
        \draw[-|] (0, 0) -- (0, 1) node [left] {$1$};
        \filldraw [fill = green, draw = green!40!black, thick]
            (0, 0) -- (2, 0) -- (2, 1) -- (0, 1) -- cycle;

        \node at (1, 0.5) {$Q$};
    \end{tikzpicture}

    \caption{Rectangle $[0, 2] \times [0, 1]$.}
    \label{fig:reconstruction-example}
\end{figure}

Let $P$ be the polytope $[0, 2] \times [0, 1]$
(Figure~\ref{fig:reconstruction-example}).
The facets of this polytope have normal vectors
$a_1 = (1, 0), a_2 = (0, 1), a_3 = (-1, 0), a_4 = (0, -1)$.
For a given translation vector $(m, l)$,
its Ehrhart function is
\begin{equation*}
    L_{P + (m, l)}(s) =
        (\floor{s(m+2)} - \ceil{sm} + 1)
        (\floor{s(l+1)} - \ceil{sl} + 1).
\end{equation*}

In this section,
we will show how to reconstruct the polytope $P$
knowing only $a_1, \dots, a_4$
and $L_{P + (m, l)}(s)$ for all integer $m, l$ and all real $s > 0$.

Let $F_1$, $F_2$, $F_3$ and $F_4$ be the facets of $P$
associated with $a_1$, $a_2$, $a_3$ and $a_4$,
respectively
(so that,
for example,
$F_1 = P \cap \{(x, y) \mid x = 2\}$).
First,
we will gather information about the facet $F_1$.

Let us define a list of vectors $w_k$ by $w_k = k a_1 = (k, 0)$.
We will extract data about $F_1$
by analyzing the discontinuities of the functions $L_{P + w_k}(s)$.

We know that there exist some real numbers $b_1, b_2, b_3, b_4$
such that the points $(x, y)$ in the polytope
are bounded by the linear inequalities
\begin{align*}
    \langle a_1, (x, y) \rangle &\leq b_1 \\
    \langle a_2, (x, y) \rangle &\leq b_2 \\
    \langle a_3, (x, y) \rangle &\leq b_3 \\
    \langle a_4, (x, y) \rangle &\leq b_4,
\end{align*}
which translates to
\begin{align*}
    x &\leq b_1 \\
    y &\leq b_2 \\
    -x &\leq b_3 \\
    -y &\leq b_4.
\end{align*}

In the polytope $s(P + w_k)$,
these inequalities become
\begin{align*}
    x &\leq s(b_1 + k) \\
    y &\leq sb_2 \\
    -x &\leq s(b_3 - k) \\
    -y &\leq sb_4.
\end{align*}

We know from theorem~\ref{thm:jump-magnitudes}
that all discontinuities of $L_{P + w_k}(s)$
are caused by points passing through the facets of $P + w_k$.
Moreover,
the left-discontinuities
are caused exclusively by points passing through front facets.
We know that,
for $k$ large enough
(larger than $b_3$),
the right-hand side of the inequality defining $F_3 + w_k$
(which is $b_3 - k$)
will be negative,
and thus $F_3 + w_k$ will be a back facet of $P + w_k$.
Therefore,
if we analyze only the left-discontinuities of $L_{P + w_k}(s)$,
we will eventually get data regarding only $F_1$, $F_2$ and $F_4$;
that is,
we eliminated the interference of $F_3$ in our analysis.

Observe that we do not know whether $F_2$ and $F_4$ are front facets or not,
so the data we get may or may not include information about these facets.
But since we are trying to get data about $F_1$,
and for large enough $k$ the facet $F_1 + w_k$ will be a front facet,
this will not be a problem.

The function $L_{P + w_k}(s)$ is
\begin{equation*}
    L_{P + w_k}(s) = (\floor{s(k+2)} - \ceil{sk} + 1)(\floor{s} + 1).
\end{equation*}
It has a left-discontinuity whenever $s$ is of the form $\frac{n}{k+2}$
for some integer $n$.
If $s$ itself is an integer,
the jump in the discontinuity has size $3s + 1$
(that is,
$L_{P + w_k}(s) - L_{P + w_k}(s^-) = 3s + 1$).
Otherwise,
if $s$ is not an integer,
the jump has size $\floor s + 1$.

Back to the linear inequalities.
Consider the function $L_{F_1 + w_k}(s)$.
According to Lemma~\ref{thm:limit-is-relative-volume},
we have
\begin{equation*}
    \lim_{\substack{
        s \to \infty \\
        s(b_1 + k) \in \mathbb Z
    }}
    \frac{L_{F_1 + w_k}(s)}{s}
    = \rvol F_1.
\end{equation*}
A direct consequence of this is that,
if $\rvol F_1 > 0$
(that is, $F_1$ is not a lower-dimensional face),
the function $L_{F_1 + w_k}(s)$ will eventually be positive
for all large enough $s$ with the form $\frac{n}{b_1 + k}$.
This means that the distance between
any two consecutive discontinuities caused by $F_1$ in $P + w_k$
(which is $\frac{1}{b_1 + k}$)
should get smaller as $k$ increases;
that is,
the discontinuities caused by $F_1$ get closer as $k$ increases.

If we repeat the analysis for $F_2$ and $F_4$,
we learn that
these facets cause discontinuities for $s$ of the form $\frac{n}{b_2}$
and $\frac{n}{b_4}$.
Thus,
these discontinuities do not get closer for large $k$.

Analyzing $L_{P + w_k}(s)$,
we have found two types of left-discontinuities:
whenever $s$ is an integer,
the jump has size $3s + 1$,
and whenever $s$ is of the form $\frac{n}{k + 2}$,
but is not an integer,
the jump has size $\floor s + 1$.
Observe that only this second class of inequalities get closer together,
and thus cannot be caused by the stationary $F_2$ and $F_4$;
therefore,
these discontinuities are caused by $F_1$.

(More precisely:
the left-discontinuities caused by $F_2$ and $F_4$
must happen in integer multiples of $\frac{1}{b_2}$ and $\frac{1}{b_4}$.
Simply by taking $k$ large enough,
we will find some multiples of $\frac{1}{k + 2}$
which are not multiples of either $\frac{1}{b_2}$ nor $\frac{1}{b_4}$,
and thus these discontinuities must be caused by $F_1$.)

Finally,
since $F_1 + w_k$ causes discontinuities of the form $\frac{n}{b_1 + k}$
and the discontinuities we observe in $L_{P + w_k}(s)$ are of the form $\frac{n}{2 + k}$,
we conclude that $b_1 = 2$.
Therefore,
we know precisely where the supporting hyperplane of $F_1$ lies.

Observe we can also recover the relative volumee of $F_1$:
since the discontinuities caused solely by $F_1$ have size $\floor s + 1$,
Lemma~\ref{thm:limit-is-relative-volume} says that
$\rvol F_1 = 1$.

Repeating this analysis for $F_2$, $F_3$ and $F_4$
gives us the values of $b_2$, $b_3$ and $b_4$,
thus reconstructing the whole polytope.


\subsubsection{Dealing with extraneous information}

In the example,
we assumed that all facets $F_1, \dots, F_4$ were actual facets
and not degenerate faces.
What if were told that the facet normals included the vector $a_5 = (-1, -1)$?

When gathering information for $F_1$ and $F_2$,
the ``facet'' $F_5$ would also eventually become a back facet,
so the same argument as before is valid.
However,
this extra facet messes up the analysis for $F_3$ and $F_4$.
For example,
if we define $w_k = (-k, 0)$
(when trying to extract information about $F_3$),
the supporting hyperplane of $F_5$ is
\begin{equation*}
    \{ (x, y) \in \mathbb R^2 \mid -x - y = -k + b_5 \},
\end{equation*}
so the discontinuities caused by $F_5$ come closer together
at the same rate as the discontinuities caused by $F_3$,
and thus we cannot properly separate them.

But we can make progress if we analyze $F_5$ first.
As before,
let $w_k = k a_5 = (-k, -k)$.
Here,
the inequality corresponding to $F_5 + w_k$ is
\begin{equation*}
    -x -y \leq 2k + b_5,
\end{equation*}
so we get discontinuities for $s$ of the form $\frac{n}{2k + b_5}$.
However,
the function $L_{P + w_k}(s)$ is
\begin{equation*}
    L_{P + w_k}(s) = (\floor{sk} - \ceil{s(k-2)} + 1)(\floor{sk} - \ceil{s(k-1)} + 1),
\end{equation*}
and this function only has left-discontinuities for $s$ of the form $\frac n k$.
That is,
we expect that the spacing between consecutive discontinuities
approaches $\frac{1}{2k}$ for large $k$,
but we only get a spacing of $\frac{1}{k}$.
This contradicts Lemma~\ref{thm:limit-is-relative-volume},
unless $\rvol F_5 = 0$.
But this means that $F_5$ is a degenerate facet;
this allows us to conclude that $a_5$ is not the direction of any facet of $P$.

So,
the plan for showing Theorem~\ref{thm:rational-complete-invariant}
goes like this:
given two polytopes $P$ and $Q$
and the functions $L_{P + w}(s)$ and $L_{Q + w}(s)$
for all integer $w$,
we will first write $P$ and $Q$ over the same set of facet normals $a_1, \dots, a_n$,
even if some of the represented facets are degenerate;
then we will go through the procedure outlined above
in order to extract the right-hand sides $b_1, \dots, b_n$ of the linear inequalities.
(Here,
the ability of pruning out extraneous vectors,
like $a_5$ in the example,
allows us to not worry about the degenerat facets.)
This information uniquely determines $P$ and $Q$,
so if $L_{P + w}(s) = L_{Q + w}(s)$,
we will have $P = Q$.

The hard part of extracting the $b_1, \dots, b_n$
will be isolating the interference of other facets
when analyzing a specific facet.
In Section~\ref{sec:isolating-largest-vector}
we will prove a technical lemma
which will allow us to do this isolation,
and in Section~\ref{sec:pseudodiophantine-equations}
we will prove another technical lemma
which will give us the right-hand sides $b_i$
using the (properly isolated) information given by the previous step.
