\subsection{Isolating the facet with the largest vector}
\label{sec:isolating-largest-vector}

Again,
write $P$ as
\begin{equation*}
    P = \bigcap_{i = 1}^n \{ x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
    \tag{\ref{eq:intersection-representation} revisited}
\end{equation*}
where each $a_i$ is a primitive integer vector.

A consequence of Lemma~\ref{thm:jump-magnitudes}
is that all discontinuities of $L_P(s)$
are caused by integer points passing through facets of $P$.
We will focus now on the left-discontinuities,
which are caused by integer points passing through front facets of $P$.

Let $F_i$ be the $i$th facet;
that is,
\begin{equation*}
    F_i = P \cap \{x \in \mathbb R^d \mid \langle a_i, x \rangle = b_i\}.
\end{equation*}
If $b_i > 0$,
so that $F_i$ is a front facet,
then $F_i$ is ``eligible'' for causing left-discontinuities on $P$.
Such discontinuities will only be caused by $F_i$ if $s b_i$ is an integer;
moreover,
for small values of $s$,
it might happen that $s F_i$ is too small to contain integer points.
However,
as long as $F_i$ is not a ``degenerate facet''
(that is, $F_i$ has codimension $1$),
its relative volume will be positive;
thus Lemma~\ref{thm:limit-is-relative-volume} guarantees that,
for all large enough $s$,
the polytope $s F_i$ will contain integer points
whenever $s b_i$ is an integer.

Therefore,
the discontinuities of the function $L_P(s)$ give some clues about $b_i$;
that is,
$s$ may only be a left-discontinuity point
if $s b_i$ is an integer for some $b_i > 0$,
and eventually all such $s$ are left-discontinuity points.
The problem is that these discontinuities give clues for all $b_i$ at once,
so we need a way of isolating such clues for each $b_i$.

We will look at the discontinuities of $L_{P + w}(s)$,
for a certain infinite collection of integer $w$.
If we choose $w_k = k a_1$,
for example,
then the left-discontinuities of $L_{P + w_k}(s)$
occur only when $s(b_i + k\langle a_i, a_1 \rangle)$ is an integer.
For larger $k$,
the spacing between discontinuity points of $L_{P + w_k}(s)$ decreases.
For now,
assume that $a_1$ has the largest norm among all $a_i$;
then the factor $\langle a_1, a_i \rangle$ will be largest for $i = 1$,
and thus
(for all arbitrarily large $k$)
the discontinuities coming from the facet $F_1 + w_k$
will be the closest among all facets of $P + w_k$.

However,
the fact these discontinuities are interleaved
(or even overlapping)
is what makes things difficult.
We will use the following technical lemma;
it essentially provides us with a ``window'' $(\alpha_k, \alpha_k + \epsilon_k)$
where we can,
at least infinitely often,
be sure only the discontinuities stemming from $a_1$ appear.

\begin{lemma}
    \label{thm:isolating-largest-vector}
    Let $a_1, \dots, a_n$ be primitive integer vectors in $\mathbb R^d$,
    with $\norm{a_1} \geq \norm{a_i}$ for all $i$.
    Then there is an integer vector $w_0$
    and a sequence $(\alpha_k, \alpha_k + \epsilon_k)$ of intervals
    such that,
    for all possible choice of real numbers $b_1, \dots, b_n$,
    the following properties are true:
    \begin{enumerate}[series = technical-lemma]
        \item \label{item:w_0-not-orthogonal}
            $ \langle a_i, w_0 \rangle \neq 0$ for all $i$;

        \item \label{item:w_0-same-direction-as-a_1}
            $ \langle a_1, w_0 \rangle > 0 $;

        \item \label{item:alpha_k-larger-than-k}
            $\alpha_k > k$ for all $k$;

        \item \label{item:alpha_k-tends-to-infinity}
            $\displaystyle
                \lim_{k \to \infty} |\alpha_k - k| = 0
            $;

        \item \label{item:epsilon_k-tends-to-zero}
            $\displaystyle
                \lim_{k \to \infty} \epsilon_k = 0
            $;

        \item \label{item:a_1-provokes-discontinuities}
            For all sufficiently large $k$,
            there is either one or two distinct values of $s$
            in $(\alpha_k, \alpha_k + \epsilon_k)$
            such that
            \begin{equation*}
                s(b_1 + k \langle a_1, w_0 \rangle)
            \end{equation*}
            is an integer;
            and

        \item \label{item:no-other-discontinuities}
            There exists infinitely many $k$ such that,
            for all $i \geq 2$ such that $\langle a_i, w_0 \rangle > 0$,
            there is no $s \in (\alpha_k, \alpha_k + \epsilon_k)$ such that
            \begin{equation*}
                s(b_i + k \langle a_i, w_0 \rangle)
            \end{equation*}
            is an integer.
    \end{enumerate}
\end{lemma}

Before proving the lemma,
let us see what these properties mean,
intuitively.

Properties \ref{item:alpha_k-larger-than-k},
\ref{item:alpha_k-tends-to-infinity} and~\ref{item:epsilon_k-tends-to-zero}
says that the numbers $s \in (\alpha_k, \alpha_k + \epsilon_k)$
are of the form $k + \delta$,
where $\delta$ is a positive value
which approaches zero for large $k$.
This will guarantee the hypothesis of Lemma~\ref{thm:pseudodiophantine-equations}.

Define $w_k = k w_0$,
so that the interval $(\alpha_k, \alpha_k + \epsilon_k)$
is an ``interesting interval'' of discontinuities in $L_{P + w_k}(s)$.
We are assuming we know the vectors $a_i$,
but not the right-hand sides $b_i$.
Property~\ref{item:w_0-not-orthogonal}
guarantees that,
for all sufficiently large $k$,
the right-hand sides $b_i + \langle a_i, w_k \rangle$
will have the sign of $\langle a_i, w_0 \rangle$,
so we know that all left-discontinuities of $L_{P + w_k}(s)$
are caused by the $a_i$ for which $\langle a_i, w_0 \rangle > 0$.
Property~\ref{item:w_0-same-direction-as-a_1} says that
$a_1$ is one of these vectors which causes the left-discontinuity.

Define $V_k$ to be the sum of the magnitudes
of all left-discontinuities of $L_{P + w_k}(s)$
which happen in the interval $(\alpha_k, \alpha_k + \epsilon_k)$.
Property~\ref{item:a_1-provokes-discontinuities}
says that,
for all large enough $k$,
at least one of these discontinuities is caused by $F_1$;
Lemma~\ref{thm:limit-is-relative-volume}
and the fact that the values in $(\alpha_k, \alpha_k + \epsilon_k)$ are approximately $k$
says that this discontinuity has magnitude of approximately $k^{d-1} \rvol F_1$.
(Here we use the uniformity in $w$ claimed by that lemma.)

Intuitively,
each $V_k$ equals $k^{d-1} \rvol F_1$
plus some positive garbage.
Properties \ref{item:a_1-provokes-discontinuities}
and~\ref{item:no-other-discontinuities}
allows us to handle this garbage.

We may categorize the $V_k$ in three cases:
the \emph{good} case,
where $L_{P + w_k}(s)$ has just a single discontinuity
in $(\alpha_k, \alpha_k + \epsilon_k)$;
the \emph{not-so-good} case,
where there is two discontinuities of about the same magnitude,
and the \emph{bad} case,
which is the remaining cases.
Property~\ref{item:a_1-provokes-discontinuities}
says that,
in the good and the not-so-good case,
$V_k$ is approximately $k^{d-1} \rvol F_1$ and $2 k^{d-1} \rvol F_1$,
respectively,
and Property~\ref{item:no-other-discontinuities}
says that either the good or the not-so-good case happen infinitely often.

Therefore,
at least one of the following limits is true
(that is, exists and equals the expression in the right-hand side):
\begin{equation*}
    \lim_{\substack{
        k \to \infty \\
        \text{$V_k$ good}
    }}
    \frac{V_k}{k^{d-1}}
    = \rvol F_1
    \qquad
    \text{or}
    \qquad
    \lim_{\substack{
        k \to \infty \\
        \text{$V_k$ not-so-good}
    }}
    \frac{V_k}{k^{d-1}}
    = 2 \rvol F_1.
\end{equation*}

Therefore,
if we know $a_1, \dots, a_n$ and $L_{P + w}(s)$ for all integer $w$,
then we can determine $\rvol F_1$.
Later,
we will see how to do some sort of induction to get the values of $\rvol F_i$
for the remaining $i$, too;
this is why Lemma~\ref{thm:isolating-largest-vector}
was stated directly in terms of the vectors $a_i$,
without referring to the polytope.

\begin{proof}[Proof of Lemma~\ref{thm:isolating-largest-vector}]
    First,
    choose an integral vector $w' \in \{a_1\}^\perp$
    which is not orthogonal to any of the vectors $a_i$,
    for $a_i \neq \pm a_1$
    (such a $w'$ exist because the intersections $\{a_1\}^\perp \cap \{a_i\}^\perp$,
    where the ``forbidden'' $w'$ falls,
    have codimension $2$,
    and thus their union are a proper subset of $\{a_1\}^\perp$).

    Since
    \begin{equation*}
        \langle a_1, a_i \rangle < \norm{a_1} \norm{a_i} \leq \norm{a_1}^2,
    \end{equation*}
    for all large enough $\tau > 0$ we have
    \begin{equation*}
        \tau \norm{a_1}^2 > \langle a_i, w' + \tau a_1 \rangle.
    \end{equation*}

    So, choose $\tau > 0$ to be an integer so large that
    the above equation is satisfied,
    and also that
    \begin{equation*}
        \langle a_i, w' + \tau a_1 \rangle \neq 0
    \end{equation*}
    for all $i$.
    (If $\langle a_i, a_1 \rangle = 0$,
    then any $\tau$ will do,
    due to the choice of $w'$;
    otherwise,
    we may just make $\tau$ large,
    because the other terms in the expression are constant.)

    Define $w_0 = \tau a_1 + w'$.
    This definition of $w_0$ satisfies conditions
    \ref{item:w_0-not-orthogonal} and~\ref{item:w_0-same-direction-as-a_1}.

    Define $\epsilon_k = \frac1k \epsilon_0$,
    where $\epsilon_0$ is a value
    (to be determined later)
    which satisfies
    \begin{equation}
        \frac{1}{\langle a_1, w_0 \rangle}
        < \epsilon_0 <
        \frac{2}{\langle a_1, w_0 \rangle}.
        \label{eq:bounds-on-epsilon_0}
    \end{equation}

    This suffice to get property~\ref{item:a_1-provokes-discontinuities}:
    note that $s(b_1 + k\langle a_1, w_0 \rangle)$ is an integer
    if and only if $s = \frac{m}{b_1 + k\langle a_1, w_0 \rangle}$
    for some integer $m$.
    Therefore,
    any open interval in $\mathbb R$
    whose length is larger than $\frac{1}{b_1 + k \langle a_1, w_0 \rangle}$
    is guaranteed to contain at least one of these.
    The first inequality guarantees that
    \begin{equation*}
        \frac{1}{b_1 + k \langle a_1, w_0 \rangle} < \frac{\epsilon_0}{k} = \epsilon_k
    \end{equation*}
    for all large enough $k$.
    Since $(\alpha_k, \alpha_k + \epsilon_k)$ has lenght $\epsilon_k$,
    this guarantees that,
    for all large enough $k$,
    at least one $s$ in this interval satisfies
    $s(b_1 + k \langle a_1, w_0 \rangle) \in \mathbb Z$.

    Now,
    any open interval which contains three distinct numbers $s$ of the form
    $\frac{n}{b_1 + k\langle a_1, w_0 \rangle}$,
    for integer $n$,
    must have length larger than $\frac{2}{b_1 + k \langle a_1, w_0 \rangle}$.
    Here,
    the second inequality in~\eqref{eq:bounds-on-epsilon_0}
    guarantees that
    \begin{equation*}
        \epsilon_k = \frac{\epsilon_0}{k} < \frac{2}{b_1 + k \langle a_1, w_0 \rangle}
    \end{equation*}
    for all large enough $k$.
    This shows that,
    once we define $\epsilon_0$ properly
    (satisfying inequality~\eqref{eq:bounds-on-epsilon_0}),
    we satisfy both properties \ref{item:a_1-provokes-discontinuities}
    and~\ref{item:epsilon_k-tends-to-zero}.

    Property~\ref{item:no-other-discontinuities} will be the hardest.
    We will define $\alpha_k$ to be a value which is a bit greater that $k$,
    so that the values of the interval $(\alpha_k, \alpha_k + \epsilon_k)$
    are of the form $k + \delta$,
    where $\delta$ is ``small, but not too small''.
    The value $\delta$ will lie in $(\alpha_k - k, \alpha_k - k + \epsilon_k)$.
    We will make $\alpha_k - k + \epsilon_k$ close to $0$,
    so that $\delta$ will be small,
    but we will make sure $\alpha_k - k$ stays some ``safe distance'' from $0$,
    so that $\delta$ is not ``too small''.

    We want to control when $s(b_i + k \langle a_i, w_0 \rangle)$ is an integer.
    If $s = k + \delta$,
    this number is
    \begin{equation*}
        (k + \delta)(b_i + k \langle a_i, w_0 \rangle) =
            k^2 \langle a_i, w_0 \rangle +
            \delta b_i +
            k b_i +
            \delta k \langle a_i, w_0 \rangle.
    \end{equation*}
    If we can guarantee that this value is distant from an integer
    for all $i > 2$,
    we get property~\ref{item:no-other-discontinuities}.

    The first term of the above expression,
    $k^2 \langle a_i, w_0 \rangle$,
    will always bee an integer,
    so we have nothing to do.

    We will choose $\alpha_k$ and $\epsilon_k$
    so that $\alpha_k - k$ is proportional to $\frac 1k$.
    As $\epsilon_k = \frac{\epsilon_0}{k}$ is also proportional to $\frac 1k$,
    and $\delta$ is ``sandwiched'' between these two values,
    the value of $\delta$ will be proportional to $\frac 1 k$, too.
    This means that the second term,
    $\delta b_i$,
    will tend to zero for large $k$.

    The third term,
    $k b_i$,
    will be dealt with in an indirect way.
    By the ``simultaneous Dirichlet's approximation theorem'',
    for each $N$ there is a $k \in \{1, \dots, N^n\}$ such that
    all numbers $k b_i$ are within $\frac 1 N$ of an integer.
    By choosing larger and larger $N$,
    we guarantee the existence of arbitrarily large $k$
    where all the distance of $k b_i$ to the nearest integer
    is made arbitrarily small.
    (This indirect attack to $k b_i$ is the responsible for the phrase
    ``there exist infinitely many $k$''
    in the condition~\ref{item:no-other-discontinuities},
    as opposed to something like
    ``for all large enough $k$''.)

    So,
    the difficulties rests upon the fourth term,
    $\delta k \langle a_i, w_0 \rangle$.
    We want to place it in the interval $(\varepsilon, 1 - \varepsilon)$
    for some $\varepsilon > 0$,
    so that this term is always at least within $\varepsilon$ of the nearest integer.
    Since for the other three terms the distance to the nearest integer
    gets arbitrarily small,
    but this fourth term stays distant,
    we can guarantee that
    $(k + \delta)(\langle a_i, w_0 \rangle k + b_i)$ is not an integer.

    We will determine a suitable $\varepsilon > 0$ later.
    Let $\varepsilon' > 0$ be such that,
    for $i \geq 2$,
    we have $\langle a_i, w_0 \rangle > \varepsilon'$
    whenever $\langle a_i, w_0 \rangle > 0$,
    and $\langle a_i, w_0 \rangle < \tau \norm{a_1}^2 - \varepsilon'$
    for all $i \geq 2$.

    Define
    \begin{equation*}
        \alpha_k = k + \frac{1}{k} \frac{\varepsilon}{\varepsilon'}.
    \end{equation*}

    Observe that this definition of $\alpha_k$
    clearly satisfies conditions \ref{item:alpha_k-larger-than-k}
    and~\ref{item:alpha_k-tends-to-infinity}.
    We have $\delta > \frac{1}{k} \frac{\varepsilon}{\varepsilon'}$,
    and thus
    \begin{equation*}
        \delta k \langle a_i, w_0 \rangle > \delta k \varepsilon' > \varepsilon.
    \end{equation*}

    Define
    \begin{equation*}
        \epsilon_0 = \frac{1 - \varepsilon}{t \norm{a_1}^2 - \varepsilon'}
            - \frac{\varepsilon}{\varepsilon'}.
    \end{equation*}
    Then since $\epsilon_k = \frac{\epsilon_0}{k}$,
    we get
    \begin{equation*}
        \delta < \alpha_k - k + \epsilon_k
        = \frac{1}{k} \frac{1 - \varepsilon}{t \norm{a_1}^2 - \varepsilon'},
    \end{equation*}
    and thus
    \begin{equation*}
        \delta k \langle a_i, w_0 \rangle < \delta k (t \norm{a_1}^2 - \varepsilon')
        < 1 - \varepsilon.
    \end{equation*}

    This show that,
    regardless of the value of $\varepsilon > 0$,
    the definitions of $\alpha_k$ and $\epsilon_k$ guarantee that,
    if $s = k + \delta \in (\alpha_k, \alpha_k + \epsilon_k)$,
    then $\delta k \langle a_i, w_0 \rangle \in (\varepsilon, 1 - \varepsilon)$.
    The other terms in $s (b_i + k \langle a_i, w_0 \rangle)$
    will approximate integer values,
    thus showing property~\ref{item:no-other-discontinuities}.

    Now we will choose $\varepsilon > 0$
    so that the definition of $\epsilon_0$
    satisfy inequality~\eqref{eq:bounds-on-epsilon_0}.

    The inequality $\frac{1}{\langle a_1, w_0 \rangle} < \epsilon_0$
    can be rewritten as follows:
    \begin{align*}
        \epsilon_0 &> \frac{1}{\langle a_1, w_0 \rangle} \\
        \frac{1 - \varepsilon}{\tau \norm{a_1}^2 - \varepsilon'}
            - \frac{\varepsilon}{\varepsilon'}
            &> \frac{1}{\tau \norm{a_1}^2}
            \\
        \frac{1}{\tau \norm{a_1}^2 - \varepsilon'} &>
            \frac{1}{\tau \norm{a_1}^2} +
            \varepsilon \left(
                \frac{1}{\varepsilon'} +
                \frac{1}{\tau \norm{a_1}^2 - \varepsilon'}
            \right).
        \intertext{
            Analogously,
            the inequality $\epsilon_0 < \frac{2}{\langle a_1, w_0 \rangle}$
            can be rewritten as
        }
        \frac{1}{\tau \norm{a_1}^2 - \varepsilon'} &<
            \frac{2}{\tau \norm{a_1}^2} +
            \varepsilon \left(
                \frac{1}{\varepsilon'} +
                \frac{1}{\tau \norm{a_1}^2 - \varepsilon'}
            \right).
    \end{align*}

    As $\varepsilon' > 0$ is a small value,
    it is clear that
    \begin{equation*}
        \frac{1}{\tau \norm{a_1}^2}
        <
        \frac{1}{\tau \norm{a_1}^2 - \varepsilon'}
        <
        \frac{2}{\tau \norm{a_1}^2},
    \end{equation*}
    so an $\varepsilon > 0$ satisfying both inequalities above does exist.
    This guarantees condition~\ref{item:a_1-provokes-discontinuities},
    finishing the proof of the lemma.
\end{proof}
