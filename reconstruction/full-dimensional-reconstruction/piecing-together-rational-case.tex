\subsection{Piecing together the semi-rational case}

Throughout the last sections,
we argued how we could obtain $b_1$ and $\rvol F_1$
if we knew $a_1, \dots, a_n$ and $L_{P + w}(s)$
for all integer $w$ and all real $s > 0$.
During this discussion,
we collected some lemmas,
which we now will use to show how to obtain the remaining $b_i$.

\begin{theorem}
    \label{thm:recovering-right-hand-sides}
    Let $a_1, \dots, a_n \in \mathbb R^d$ be primitive integer vectors
    and for each integer $w$ let $f_w(s)$ be a function on $\mathbb R$.
    Then there is at most one set of numbers $b_1, \dots, b_n$ such that
    \begin{equation*}
        P = \bigcap_{i=1}^n \{x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i\}
    \end{equation*}
    is a full-dimensional semi-rational polytope,
    that $f_w(s) = L_{P + w}(s)$ for all $s > 0$ and all integer $w$,
    and that the polytopes $F_i$ defined by
    \begin{equation*}
        F_i = P \cap \{x \in \mathbb R^d \mid \langle a_i, x \rangle = b_i\}
    \end{equation*}
    are faces of $P$.
\end{theorem}

Observe that here we are not assuming that $n$ is the number of facets of $P$;
in particular,
every facet of $P$ will be an $F_i$ for some $i$,
but some of the $F_i$ might be lower-dimensional faces of $P$.
For example,
the facet $F_5$ of the example in Section~\ref{sec:reconstruction-example}
is a lower-dimensional face of the polytope $P$.
We will have to take care
to detect when a face $F_i$ is a codimension $1$ facet or not;
this will be important
in the proof of Corollary~\ref{thm:semirational-complete-invariant}.

\begin{proof}
    Order the vectors $a_i$ by lenght,
    in nonincreasing order;
    that is,
    \begin{equation*}
        \norm{a_1} \geq \norm{a_2} \geq \dots \geq \norm{a_n}.
    \end{equation*}

    For each $i$,
    we will determine whether $F_i$ is a facet of $P$ or not
    (that is, whether $F_i$ has codimension $1$),
    and,
    in the case it is a facet,
    we will determine $\rvol F_i$ and $b_i$.
    Then,
    given $b_i$ for the facets of $P$,
    the polytope is completely determined;
    since then $b_i$ is the smallest real number such that
    \begin{equation*}
        P \subseteq \{ x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
    \end{equation*}
    this determines the remaining $b_i$
    (which are associated with lower dimensional faces of $P$).

    We will proceed by induction,
    so assume that,
    for some $j \geq 1$,
    we have determined everything for the faces $F_i$ with $i < j$.

    By Lemma~\ref{thm:jump-magnitudes},
    all discontinuities of the function $L_{P + w}(s)$
    are due to integer points passing through the facets of $P$.
    If $s_0$ is a point of left-discontinuity of $L_{P + w}(s)$,
    let $F_{i_1} + w, \dots, F_{i_l} + w$ be the front facets of $P + w$
    such that $s_0 (b_i + \langle a_i, w \rangle)$ is an integer;
    that is, $F_{i_1}, \dots, F_{i_l}$
    are the facets which caused the discontinuity at $s_0$.
    Then Lemma~\ref{thm:limit-is-relative-volume} says that
    if $U$ is the magnitude of this discontinuity,
    then $\frac{U}{s_0^{d-1}}$
    is approximately $\rvol F_{i_1} + \dots + \rvol F_{i_l}$.

    Here,
    the fact that Lemma~\ref{thm:limit-is-relative-volume}
    asserts that the limit is uniform in the translation vector $w$
    guarantees that
    the number $\frac{U}{s^{d-1}}$
    will approximate $\rvol F_{i_1} + \dots + \rvol F_{i_l}$
    just by making $s$ large,
    regardless of $w$.

    Let $S(x) = 0^{d-1} + 1^{d-1} + \dots + \floor{x}^{d-1}$ for $x \geq 0$
    and $S(x) = -S(-x)$ for $x \leq 0$,
    and define
    \begin{equation*}
        g_w(s) = f_w(s) - \sum_{i=1}^{j-1} S(s(b_i + \langle a_i, w \rangle)) \gamma_i,
    \end{equation*}
    where $\gamma_i$ is $\rvol F_i$,
    if $F_i$ is a facet of $P$,
    and zero otherwise.

    If $x = s(b_i + \langle a_i, w \rangle)$ is a positive integer,
    then the term $S(x) \rvol F_i$
    will cause a left-discontinuity of magnitude $x^{d-1} \rvol F_i$;
    if $x$ is a negative integer,
    then the term $S(x) \rvol F_i$
    will cause a right-discontinuity of magnitude $x^{d-1} \rvol F_i$.
    Subtracting these values from $f_w$ ``cleans'' the function
    from the influence of the discontinuities caused by $F_i$,
    for $i < j$.

    In terms of $\frac{U}{s^{d-1}}$,
    this says that,
    if $V$ is the magnitude of the left discontinuity at $s_0$ of $g_w$,
    then $\frac{V}{s_0^{d-1}}$ approximates the sum of the $\rvol F_{i_m}$
    for which $i_m \geq j$.

    Since we know whether $F_i$ is a facet or not,
    and we know $\rvol F_i$ and $b_i$ in the case it is,
    we may construct the function $g_w$,
    so we may work with the ``cleaner'' $V$ instead of with $U$.
    (Note that removing the terms $S(x) \rvol F_i$
    does not perfectly eliminate the influence of the facets $F_i$,
    because the magnitudes of the jumps only approximate $s^{d-1} \rvol F_i$.
    Therefore,
    there might be some ``residual'' discontinuities of order $O(s^{d-2})$ in $g_w$.
    Since we will always divide the magnitude of the discontinuities by $s^{d-1}$,
    we may safely ignore these residual discontinuities.)

    Use Lemma~\ref{thm:isolating-largest-vector}
    with the vectors $a_j, \dots, a_n$
    to get appropriate $w_0$, $\alpha_k$ and $\epsilon_k$.

    Define $V_k$ to be sum of the magnitudes of the left-discontinuities
    of the function $g_k$ in the interval $(\alpha_k, \alpha_k + \epsilon_k)$.
    We will first determine whether $F_j$ is a facet or not.

    Property~\ref{item:a_1-provokes-discontinuities} says that,
    if $F_j$ is a facet
    (so that its $(d-1)$-dimensional volume is nonzero),
    the value of $\frac{V_k}{k^{d-1}}$
    must be at least $\rvol F_j$.
    Property~\ref{item:no-other-discontinuities} says that,
    for arbitrarily many $k$,
    the value of $\frac{V_k}{k^{d-1}}$
    will be either $\rvol F_j$ or $2 \rvol F_j$.
    Conversely,
    if $F_j$ is not a facet,
    then this number will approach zero.
    Therefore,
    $F_j$ is a facet if and only if
    \begin{equation*}
        \liminf_{k \to \infty} \frac{V_k}{k^{d-1}} > 0.
    \end{equation*}

    If $F_j$ is not a facet,
    there is nothing more to do,
    so assume that $F_j$ is a facet of $P$.

    Let us say that a value $V_k$ is \emph{good}
    if the interval $(\alpha_k, \alpha_k + \epsilon_k)$
    contains exactly one discontinuity;
    \emph{not-so-good},
    if the interval contains exactly two discontinuities,
    and the magnitude of the jump in both cases is approximately the same
    (that is, their ratio is close to $1$);
    and \emph{bad} otherwise.

    If $V_k$ is good,
    then we know from Lemma~\ref{thm:limit-is-relative-volume}
    that $V_k$ is approximately $k^{d-1} \rvol F_j$.
    If $V_k$ is not-so-good,
    we know at least one of the two discontinuities
    must have a magnitude of approximately $k^{d-1} \rvol F_j$,
    so the other discontinuity must also have that magnitude,
    and thus $V_k$ is approximately $2 k^{d-1} \rvol F_j$.

    Property~\ref{item:no-other-discontinuities}
    of Lemma~\ref{thm:isolating-largest-vector} says that
    there are infinitely many $V_k$ which are either good or not-so-good.
    This means that at least one of the limits
    \begin{equation*}
        \liminf_{\substack{
                k \to \infty \\
                \text{$V_k$ good}
            }}
            \frac{V_k}{k^{d-1}}
        \qquad
        \text{and}
        \qquad
        \frac12
        \liminf_{\substack{
                k \to \infty \\
                \text{$V_k$ not-so-good}
            }}
            \frac{V_k}{k^{d-1}}
    \end{equation*}
    exists and equals $\rvol F_j$.

    If either one of the limits do not exist
    (which happens only if there is only finitely many good $V_k$
    or finitely many not-so-good $V_k$, respectively),
    or both limits agree,
    then we know for sure the value of $\rvol F_j$.
    It might happen that both limit exists,
    but their values are different;
    this happens if there are infinitely many good and not-so-good $V_k$,
    but either of these have some associated ``garbage''.
    For good $V_k$,
    we know that there is a single discontinuity in $(\alpha_k, \alpha_k + \epsilon_k)$,
    but it might happen that this discontinuity
    is due to $F_j$ and some other facets $F_i$ for $i > j$.
    A similar problem happens with the not-so-good $V_k$.
    In this case,
    we will have ``dirty'' $V_k$,
    which will make the limits larger than $\rvol F_j$.

    However,
    property~\ref{item:no-other-discontinuities}
    does guarantee that
    there will be infinitely many ``clean'' $V_k$,
    so in the event that both limits exist and differ,
    the smallest value is the correct one
    (because that's where the infinitely many ``clean'' $V_k$ appeared).

    To recover $b_j$,
    we will handle these cases separately.

    Assume first that the ``infinitely many good $V_k$'' gave the correct value.
    Looking in the intervals $(\alpha_k, \alpha_k + \epsilon_k)$
    for which the corresponding $V_k$ approximate $k^{d-1} \rvol F_j$,
    we get an infinite number of $s_k \in (\alpha_k, \alpha_k + \epsilon_k)$
    which we know are discontinuities provoked by $F_j$.
    That is,
    there is a sequence of integers $m_k$ such that
    \begin{equation*}
        s_k (b_j + k \langle a_j, w_0 \rangle) = m_k
    \end{equation*}
    for all these $k$.
    By properties \ref{item:alpha_k-larger-than-k},
    \ref{item:alpha_k-tends-to-infinity} and~\ref{item:epsilon_k-tends-to-zero}
    of Lemma~\ref{thm:isolating-largest-vector},
    we have that $\{s_k\}$ approaches zero for these $k$,
    and thus we may apply Lemma~\ref{thm:pseudodiophantine-equations}
    to determine $b_j$ uniquely.

    Now assume that the correct limit is the ``infinitely many not-so-good $V_k$''.
    Let $s_k$ and $s'_k$ (with $s_k < s'_k$)
    be the two discontinuities which happen in $(\alpha_k, \alpha_k, \epsilon_k)$,
    when $V_k$ is not-so-good and approximates $2 \rvol F_j$.
    The main difficulty of this case is that,
    for each $k$,
    we know at least one of $s_k$ and $s'_k$
    is a point of discontinuity caused by $F_j$,
    but it might not be both.

    Call the case where both are discontinuities caused by $F_j$
    the \emph{nice} case.
    Since these two are consecutive discontinuities,
    we have
    \begin{equation*}
        s'_k - s_k = \frac{1}{b_j + k \langle a_j, w_0 \rangle};
    \end{equation*}
    rearranging this equation gives
    \begin{equation*}
        b_j = \frac{1}{s'_k - s_k} - k \langle a_j, w_0 \rangle.
    \end{equation*}

    In a non-nice case,
    the distance between $s'_k$ and $s_k$
    is smaller than in the nice case
    (because in any open interval
    with length larger than $\frac{1}{b_j + k \langle a_j, w_0 \rangle}$
    lies a point of discontinuity of $F_j$);
    that is,
    \begin{equation*}
        s'_k - s_k < \frac{1}{b_j + k \langle a_j, w_0 \rangle}.
    \end{equation*}
    If $k$ is large enough,
    this translates to
    \begin{equation*}
        b_j < \frac{1}{s'_k - s_k} - k \langle a_j, w_0 \rangle.
    \end{equation*}

    Due to properties \ref{item:a_1-provokes-discontinuities}
    and~\ref{item:no-other-discontinuities},
    together with the current assumption that
    there are only finitely many good $V_k$,
    we know that the nice case happens infinitely often;
    therefore,
    \begin{equation*}
        b_j = \limsup_{\substack{
            k \to \infty \\
            \text{$V_k$ not-so-good}
        }}
        \frac{1}{s'_k - s_k} - k \langle a_j, w_0 \rangle.
    \end{equation*}

    Therefore,
    if $F_j$ is a facet of $P$,
    both when there are infinitely many good $V_k$
    and when there are infinitely many not-so-good $V_k$
    we can compute $\rvol F_j$ and $b_j$.

    Now apply induction on $j$ to finish the proof.
\end{proof}

\begin{corollary}
    \label{thm:semirational-complete-invariant}
    Let $P$ and $Q$ be two full-dimensional semi-rational polytopes
    such that $L_{P + w}(s) = L_{Q + w}(s)$
    for all integer $w$
    and all real $s > 0$.
    Then $P = Q$.
\end{corollary}

In other words,
the functions $L_{P + w}(s)$ for all integer $w$
form a complete set of invariants
in the class of full-dimensional semi-rational polytopes.

\begin{proof}
    If $a$ is a primitive integer vector such that
    \begin{equation*}
        P \cap \{x \in \mathbb R^d \mid \langle a, x \rangle = b \}
    \end{equation*}
    is a facet of $P$,
    then,
    as $Q$ is a bounded set,
    for some appropriate $b'$ we have
    \begin{equation*}
        Q \subseteq \{x \in \mathbb R^d \mid \langle a, x \rangle \leq b' \}.
    \end{equation*}

    This means we can write
    \begin{align*}
        P &= \bigcap_{i=1}^n \{
                x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i
            \} \\
        Q &= \bigcap_{i=1}^n \{
                x \in \mathbb R^d \mid \langle a_i, x \rangle \leq c_i
            \},
    \end{align*}
    where $a_1, \dots, a_n$ are primitive integer vectors,
    and $b_1, \dots, b_n, c_1, \dots, c_n$ are real numbers.

    That is,
    by possibly adding some redundant vectors,
    we can write $P$ and $Q$ as intersection of hyperplanes
    using the same set of normal vectors.
    Now just apply Theorem~\ref{thm:recovering-right-hand-sides}
    (assuming knowledge of the vectors $a_1, \dots, a_n$)
    to conclude that $P = Q$.
\end{proof}
