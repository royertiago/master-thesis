\section{Codimension one polytopes}
\label{sec:codimension-one}

Corollary~\ref{thm:semirational-complete-invariant}
says that the functions $L_{P + w}(s)$ form a complete set of invariants
in the class of full-dimensional semi-rational polytopes.
There exists a simple example which shows that
full-dimensionality is needed:
consider again the affine space $M$
defined in Section~\ref{sec:translation-variant} by
\begin{equation*}
    M = \{(\ln 2, \ln 3)\} \times \mathbb R^{d-2}.
\end{equation*}

For any real $s > 0$ and any integer $w$,
the affine space $s(M + w)$ has no integer points,
and thus if $P$ and $Q$ are any polytopes which are contained in $M$
then $L_{P + w}(s) = L_{Q + w}(s) = 0$
for all $w$ and all $s > 0$.

$P$ and $Q$ may be chosen to be semi-rational in the example above,
so we know that
the analogue of Corollary~\ref{thm:semirational-complete-invariant}
for codimension $2$ semi-rational polytopes
is false.
This leaves open the possibility that
the analogue for semi-rational codimension $1$ polytopes,
or for rational polytopes of any dimension,
is true.
In this section
we will show that both these analogues are indeed true.

The general idea is to reduce to the full-dimensional case,
but we will need to do some adjustments.
For example,
if $P$ is a $(d-1)$-dimensional polytope
contained in $\mathbb R^{d-1} \times \{\frac 12\}$,
let $P' = P - (0, \dots, 0, \frac 12)$,
and $P''$ be the projection of $P'$ to $\mathbb R^{d-1}$.
The polytope $P''$ is,
indeed,
a full-dimensional polytope in $\mathbb R^{d-1}$,
and if $w = (w_1, \dots, w_{d-1})$ is an integer vector
we know that
\begin{equation*}
    L_{P' + w}(s) = L_{P'' + (w_1, \dots, w_{d-1}, 0)}(s)
\end{equation*}
for all $s$.
Therefore,
if we can compute $L_{P' + w}(s)$ for all $w$ whose last coordinate is zero,
we may use Corollary~\ref{thm:semirational-complete-invariant}
for $P''$,
and conclude $P''$ is uniquely identified.
(We will see later how to distinguish between two translates of the same polytope.)

The problem is that,
just by using $L_{P + w}(s)$,
we cannot compute $L_{P' + w}(s)$ for all $s$.
Let $w = (w_1, \dots, w_d)$ be an integer vector.
Then
\begin{equation*}
    P + w \subseteq \mathbb R^{d-1} \times \{w_d + \tfrac 12\},
\end{equation*}
so $L_{P + w}(s)$ will be nonzero only for $s$ of the form $\frac{m}{w_d + \frac12}$
for some integer $m$.
In this case,
we have
\begin{equation*}
    L_{P + w}(s) = L_{P' + w'}(s),
\end{equation*}
for $w' = (w_1, \dots, w_{d-1}, 0)$.

Thus,
we may compute $L_{P'' + w}(s)$
only for $s$ of the form $\frac{2m}{2 w_d + 1}$;
that is,
instead of knowing the value of $L_{P'' + w}(s)$ for all $s$,
we know it just for a dense subset of $\mathbb R$.

Since each $L_P(s)$ is piecewise constant,
this is still enough information to compute the one-sided limits
$L_P(s^+)$ and $L_P(s^-)$
for all $s > 0$,
and as each $L_P(s)$ is lower semicontinuous,
we can fully reconstruct most of its discontinuities.
In particular,
if $s_0$ is either a right-discontinuity or a left-discontinuity,
but no both,
of $L_P(s)$,
we know that $L_P(s_0)$ is the largest of $L_P(s_0^-)$ and $L_P(s_0^+)$.
However,
this is not enough to recover $L_P(s_0)$
if $s_0$ is both a left- and right-discontinuity;
this happens,
for example,
at $s_0 = 3$ with the square of Figure~\ref{fig:ehrhart-function-small-square}.

In order to use Corollary~\ref{thm:semirational-complete-invariant},
we will strengthen its proof
to work with knowledge of $L_{P + w}(s)$ only for densely many $s$.
More specifically,
we will modify Lemma~\ref{thm:isolating-largest-vector}
so that the choice of $w_0$ avoids these overlapping discontinuities,
at least in the window $(\alpha_k, \alpha_k + \epsilon_k)$
in which we analyze $L_{P + kw_0}(s)$.
