\subsection{Semi-rational polytopes with codimension $0$ and $1$}

Now we may show Corollary~\ref{thm:semirational-complete-invariant}
for semi-rational polytopes
which have codimension $0$ and $1$.

\begin{restatetheorem}{thm:codimension-zero-and-one}
    Let $P$ and $Q$ be two semi-rational polytopes in $\mathbb R^d$,
    both having codimension $0$ or $1$.
    Suppose moreover that $L_{P + w}(s) = L_{Q + w}(s)$
    for all integer $w$ and all real $s > 0$.
    Then $P = Q$.
\end{restatetheorem}

\begin{proof}
    If their codimensions do not match,
    then $L_P(s)$ and $L_Q(s)$ will be different,
    so we may assume either both have codimension $0$
    or both have codimension $1$.
    In the first case,
    $P$ and $Q$ are full-dimensional,
    so we may use Corollary~\ref{thm:semirational-dense-information-complete-invariant}
    directly.
    Thus,
    assume both $P$ and $Q$ have codimension $1$.

    Let $H$, $H'$ satisfy
    \begin{align*}
        P \subseteq H &= \{x \in \mathbb R^d \mid \langle a, x \rangle = b\} \\
        Q \subseteq H' &= \{x \in \mathbb R^d \mid \langle a', x \rangle = b'\}.
    \end{align*}

    If we had $a \neq a'$,
    we could choose some vector $w$ which is orthogonal to $a$ but not to $a'$,
    and then $\vol \ppyr(Q + kw)$ would increase for large $k$
    (because the height of the pseudopyramid would increase,
    whereas the area of the base do not change)
    but $\vol \ppyr(P + kw)$ would stay the same
    (because neither the height nor the area of the base would change).
    Since $\vol \ppyr(P + kw)$ is determined by $L_{P + kw}(s)$
    (by Lemma~\ref{thm:different-pseudopyramid-volumes}),
    we know this cannot happen.

    This shows $a = a'$,
    and using Lemma~\ref{thm:limit-is-relative-volume}
    both $L_P(s)$ and of $L_Q(s)$ must exhibit discontinuities
    for all large enough $s$,
    which shows $b = b'$.
    Thus, $H = H'$.

    Now using unimodular transforms we may assume that
    \begin{equation*}
        H = \{x \in \mathbb R^d \mid x_d = b \}.
    \end{equation*}

    We have $P - (0, \dots, 0, b) \in \mathbb R^{d-1} \times \{0\}$,
    and analogously for $Q$,
    so we may define $P' \subseteq \mathbb R^{d-1}$
    to be the projection of $P - (0, \dots, 0, b)$ to $\mathbb R^{d-1}$,
    and analogously for $Q$.
    We will show that $P' = Q'$,
    which implies $P = Q$.

    Let $w' = (w_1, \dots, w_{d-1})$ be given.
    If $s$ is of the form
    \begin{equation*}
        s = \frac{m}{b + w_d}
    \end{equation*}
    for some integers $m$ and $w_d$,
    let $w = (w_1, \dots, w_d)$ and then
    \begin{align*}
        L_{P + w}(s) &= \#( s(P + w) \cap \mathbb Z^d ) \\
            &= \#\big( (s(P + w) - (0, \dots, 0, m)) \cap \mathbb Z^d \big) \\
            &= \#(s(P' + w') \cap \mathbb Z^{d-1}) \\
            &= L_{P' + w'}(s).
    \end{align*}

    Analogously, we have $L_{Q + w}(s) = L_{Q' + w'}(s)$.

    This shows that $L_{P' + w'}(s) = L_{Q' + w'}(s)$
    for all integer $w' \in \mathbb R^{d-1}$
    and all $s > 0$ of the form $\frac{m}{b + w_d}$,
    which form a dense subset of $\mathbb R$.
    Therefore,
    $P'$ and $Q'$ satisfy the hypothesis
    of Corollary~\ref{thm:semirational-dense-information-complete-invariant},
    and thus $P' = Q'$,
    which shows $P = Q$.
\end{proof}
