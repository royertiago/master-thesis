\subsection{Avoiding overlapping discontinuities}

Again,
write $P$ as
\begin{equation*}
    P = \bigcap_{i = 1}^n \{ x \in \mathbb R^d \mid \langle a_i, x \rangle \leq b_i \},
    \tag{\ref{eq:intersection-representation} revisited}
\end{equation*}
and let $F_i$ be the corresponding facets of $P$.

The facet $F_i + w_k$ may only trigger a discontinuity at $s$ if
\begin{equation*}
    s = \frac{m}{b_i + \langle a_i, w_k \rangle}
\end{equation*}
for some integer $m$.
Therefore,
if $F_i + w_k$ and $F_j + w_k$ both trigger a discontinuity at $s$,
then we must have,
for integers $m_i$, $m_j$,
\begin{equation*}
    s = \frac{m_i}{b_i + \langle a_i, w_k \rangle}
      = \frac{m_j}{b_j + \langle a_j, w_k \rangle}.
\end{equation*}

If we divide both $m_i$ and $m_j$ by their greatest common divisor,
we obtain a ``primitive'' number $s$
such that all other simultaneous discontinuity points of $F_i + w_k$ and $F_j + w_k$
are integer multiples of $s$.
So,
assume $m_i$ and $m_j$ are relatively prime.

From this equation,
it is clear that if both $F_i + w_k$ and $F_j + w_k$ trigger a discontinuity,
we must either have both $b_i$ and $b_j$ rational,
or both irrational.
We will first deal with the irrational case,
which is easier.

Assume for now that $b_i$ and $b_j$ are both irrational.
We may rewrite the above equation as
\begin{equation*}
    b_j = \frac{m_i}{m_j} b_i +
        \frac{m_i}{m_j} \langle a_j, w_k \rangle - \langle a_i, w_k \rangle.
\end{equation*}

If there is a $k' \neq k$
for which $F_i + w_{k'}$ and $F_j + w_{k'}$ also have overlapping discontinuities,
then there is also two coprime integers $m'_i, m'_j$ such that
\begin{equation*}
    b_j = \frac{m'_i}{m'_j} b_i +
        \frac{m'_i}{m'_j} \langle a_j, w_{k'} \rangle - \langle a_i, w_{k'} \rangle.
\end{equation*}

In both cases,
we wrote $b_j$ as a rational combination of $b_i$ and $1$.
Thinking of $\mathbb R$ as a vector space over $\mathbb Q$,
we know $b_i$ and $1$ are linearly independent,
and thus this representation of $b_j$ is unique.
Therefore,
\begin{equation*}
    \frac{m'_i}{m'_j} = \frac{m_i}{m_j}
    \qquad \text{and} \qquad
    \frac{m_i}{m_j} \langle a_j, w_k \rangle - \langle a_i, w_k \rangle =
    \frac{m'_i}{m'_j} \langle a_j, w_{k'} \rangle - \langle a_i, w_{k'} \rangle.
\end{equation*}

Since the pairs $(m_i, m_j)$ and $(m'_i, m'_j)$ are of coprime numbers,
the first of these two equations give
$m_i = \pm m'_i$ and $m_j = \pm m'_j$;
we may assume $m_i = m'_i$ and $m_j = m'_j$.
Using these identities
and expanding $w_k = k w_0$,
the second equation may be rearranged to
\begin{equation*}
    k \langle m_i a_j - m_j a_i, w_0 \rangle = k' \langle m_i a_j - m_j a_i, w_0 \rangle.
\end{equation*}

Since we assumed that $k' \neq k$,
we must have
\begin{equation*}
    \langle m_i a_j - m_j a_i, w_0 \rangle = 0,
\end{equation*}
which means that $w_0$ is orthogonal to $m_i a_j - m_j a_i$.

Therefore,
if we guarantee that $w_0$ is not orthogonal to $m_i a_j - m_j a_i$,
we are sure that $F_i + w_k$ and $F_j + w_k$ will have overlapping discontinuities
for at most one $k$.

Ideally,
we would like to make $w_0$ non-orthogonal to $m_i a_j - m_j a_i$
for all possible choices of $m_i, m_j$ and all $a_i, a_j$.
That would add an infinite number of restrictions on $w_0$,
which might make the choice of $w_0$ impossible
(for example, if $a_1 = (1, 0)$ and $a_2 = (0, 1)$,
then the only possible choice would be $w_0 = 0$).

Fortunately,
there is at most one problematic $m_i$ and $m_j$ which must be avoided
for each pair of irrationals $b_i$ and $b_j$.
So,
for convenience,
call the ``dependence index''
between any two irrational numbers $b_i$ and $b_j$
to be $\max\{|m_i|, |m_j|\}$,
where $m_i$ and $m_j$ are coprime numbers such that $b_i = \frac{m_i}{m_j} b_j + r$
for some rational number $r$.
(This is well-defined because if $m_i$ and $m_j$ exist,
then they are unique,
up to signs.)
If no such integers $m_i$ and $m_j$ exist,
let the dependence index be zero.

If $N$ is larger than the dependence index
between any two irrational numbers in $\{b_1, \dots, b_n\}$,
making $w_0$ not orthogonal to any vector of the form
$m_i a_j - m_j a_i$ with $|m_i|, |m_j| \leq N$ guarantees that
the discontinuities of $F_i + w_k$ and $F_j + w_k$
will overlap for at most one $k$.

This solves the irrational clashes,
so now assume $b_i$ and $b_j$ are rational.

Here,
clashes are unavoidable.
The goal is to make them happen outside $(\alpha_k, \alpha_k + \epsilon_k)$.
This will be accomplished by showing that,
if $s$ is a simultaneous discontinuity point for any $k$,
then the denominator of $s$ is bounded.
The interval $(\alpha_k, \alpha_k + \epsilon_k)$
contains points of the form $k + \delta$
for small, positive $\delta$,
and as $k$ gets large,
$\delta$ gets small.
This will guarantee no discontinuity clashes happen inside this interval.

Write $b_i = \frac{p_i}{q_i}$.
The ``primitive clash equation'' reads
\begin{equation*}
    s = \frac{q_i m_i}{p_i + q_i \langle a_i, w_k \rangle}
      = \frac{q_j m_j}{p_j + q_j \langle a_j, w_k \rangle},
\end{equation*}
which may be rewritten as
\begin{equation*}
    m_i q_i (p_j + q_j \langle a_j, w_0 \rangle k) =
    m_j q_j (p_i + q_i \langle a_i, w_0 \rangle k).
\end{equation*}

This is an equation of the form $\zeta m_i = \xi m_j$,
where $\zeta = q_i p_j + q_i q_j \langle a_j, w_0 \rangle k$
and $\xi = q_j p_i + q_j q_i \langle a_i, w_0 \rangle k$.
All solutions are of the form
\begin{equation*}
    (m_i, m_j) = \left( \frac{\xi}{\gcd(\zeta, \xi)} l,
                        \frac{\zeta}{\gcd(\zeta, \xi)} l \right),
\end{equation*}
for some integer $l$.
Since we are looking for the ``primitive'' solution,
we will take $l = 1$.

The following lemma guarantees that
the denominator of the primitive solution is bounded,
as a function of $k$.

\begin{lemma}
    \label{thm:gcd-arithmetic-sequence}
    Let $\zeta, \xi, \gamma, \eta$ be integers
    such that the vectors $(\zeta, \gamma)$ and $(\xi, \eta)$
    are linearly independent.
    Then the sequence $\{\gcd(\zeta k + \gamma, \xi k + \eta)\}_{k = 1}^{\infty}$
    is periodic.
\end{lemma}

\begin{proof}
    By swapping $(\zeta, \gamma)$ and $(\xi, \eta)$
    and multiplying them by $-1$,
    if needed,
    we may assume that $\zeta \geq \xi \geq 0$.

    If $\xi = 0$,
    then the sequence has only terms of the form
    \begin{equation*}
        \gcd( \zeta k + \gamma, \eta ).
    \end{equation*}

    Since $\eta \neq 0$
    (due to the linear independence restriction),
    we have
    \begin{equation*}
        \gcd( \zeta k + \gamma, \eta ) =
        \gcd(\eta, (\zeta k + \gamma) \bmod \eta),
    \end{equation*}
    from which periodicity is clear.

    If $\zeta, \xi > 0$,
    we have
    \begin{equation*}
        \gcd(\zeta k + \gamma, \xi k + \eta) =
        \gcd(\xi k + \eta, (\zeta - \xi)k + \gamma - \eta),
    \end{equation*}
    so if we let $\zeta' = \xi$,
    $\xi' = \zeta - \xi$,
    $\gamma' = \eta$,
    and $\eta' = \gamma - \eta$,
    then $\zeta', \xi' \geq 0$ and $\zeta' + \xi' < \zeta + \xi$,
    so we may apply induction in $\zeta + \xi$
    to conclude that the sequence is periodic.
\end{proof}

In our case,
we have $\zeta = q_i q_j \langle a_i, w_0 \rangle$,
$\xi = q_i q_j \langle a_i, w_0 \rangle$,
$\gamma = q_j p_i$ and $\eta = q_i p_j$.
For the vectors $(\zeta, \gamma)$ and $(\xi, \eta)$ to be linearly dependent,
there must exist some rational number $r$ such that
$r\zeta = \xi$ and $r \gamma = \eta$.
That is,
\begin{equation*}
    \frac{q_i q_j \langle a_i, w_0 \rangle} {q_i q_j \langle a_j, w_0 \rangle}
    = \frac \xi \zeta = r = \frac \eta \gamma =
    \frac{q_i p_j}{q_j p_i}
\end{equation*}

Rearranging the equation gives
\begin{equation*}
    \langle w_0, p_j q_i a_i - p_i q_j a_j \rangle = 0.
\end{equation*}
Therefore,
if we can guarantee that $w_0$ is not orthogonal to
$p_j q_i a_j - p_i q_j a_j$,
for all $i$ and $j$
(with $i \neq j$),
we will make sure that
\begin{equation*}
    \frac{m_i}{p_i + q_i \langle a_i, w_k \rangle}
\end{equation*}
will have only finitely many distinct values.
As $s$ is a multiple of this value,
this gives a bound on the denominator of $s$;
so,
for large enough $k$,
no such $s$ will appear in $(\alpha_k, \alpha_k + \epsilon_k)$.

We may use the same trick that deals with the irrational case:
we will add a parameter $N$ to the lemma,
and make $w_0$ orthogonal to all $\tau_j a_j - \tau_i a_i$
for any pair $\tau_i, \tau_j$ with $|\tau_i|, |\tau_j| \leq N^2$.

The modified lemma is the following.

\begin{lemma}
    \label{thm:densely-isolating-largest-vector}
    Let $N \geq 0$ be some integer number,
    and let $a_1, \dots, a_n$ be primitive integer vectors in $\mathbb R^d$,
    with $\norm{a_1} \geq \norm{a_i}$ for all $i$.
    Then there is an integer vector $w_0$
    and a sequence $(\alpha_k, \alpha_k + \epsilon_k)$ of intervals
    such that,
    for all possible choice of real numbers $b_1, \dots, b_n$,
    all the properties enumerated in Lemma~\ref{thm:isolating-largest-vector}
    are true,
    and also the following one:
    \begin{enumerate}[technical-lemma]
        \item \label{item:no-discontinuity-clash}
            If the numerator and the denominator of all rational $b_i$
            is smaller than or equal to $N$,
            and the ``dependence index'' between any two irrational $b_i$
            is smaller than or equal to $N$,
            then for all sufficiently large $k$
            there will be no $s \in (\alpha_k, \alpha_k + \epsilon_k)$
    \end{enumerate}
\end{lemma}

\begin{proof}
    In the choice of the vector $w_0$,
    in the beginning of the lemma,
    there was already a (finite) list of integer vectors
    such that $w_0$ was made non-orthogonal
    (namely, all vectors $a_i$ for $i = 1, \dots, n$).
    Now,
    add to that list all vectors of the form
    \begin{equation*}
        \tau_i a_j - \tau_j a_i,
    \end{equation*}
    for all pairs of integers $\tau_i, \tau_j$ such that $|\tau_i|, |\tau_j| \leq N^2$.

    Primitiveness of the vectors $a_i$ guarantee that
    none of these vectors is the zero vector,
    and as we are limiting the coefficients by $N^2$
    we still have a finite list.

    Now choose $w_0$, $\alpha_k$ and $\epsilon_k$
    in the same way as in the proof of Lemma~\ref{thm:isolating-largest-vector}
    except that, now,
    the list of vectors not orthogonal to $w_0$ is larger.
    This will guarantee all the properties of that lemma.

    Finally,
    property~\ref{item:no-discontinuity-clash}
    follows from the discussion above.
\end{proof}
